const electron = require('electron');

const { app, BrowserWindow, Menu, ipcMain, ipcRenderer, protocol, net } = electron;
const moment = require('moment');
const fs = require('fs');
const path = require('path');
const isDev = require('electron-is-dev');

const { Services, FileServices } = require(`${path.join(__dirname, './services')}`);
const Yeezysupply = require('./yeezysupply')

var captchaBank = [];

let mainWindow;
let activationWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1074,
    height: 665,
    minWidth: 1074,
    minHeight: 665,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: `${__dirname}/preload.js`,
      partition: 'persist:main',
      // DEV_MODE
      devTools: true
    },
    resizable: true,
    useContentSize: true,
    backgroundColor: '#666666',
    icon: 'file://' + path.join(__dirname, 'logo192.png'),
  });
  // DEV_MODE
  if(isDev) { mainWindow.webContents.openDevTools(); }
  mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
  mainWindow.on('closed', () => (mainWindow = null));
}
app.commandLine.appendSwitch('disable-site-isolation-trials');

let isauth = false;
app.on('ready', () => {
  authWindow();
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

ipcMain.on('close', async (event, _) => {
  let window = BrowserWindow.getFocusedWindow();
  window.close();
});

ipcMain.on('hide', async (event, _) => {
  let window = BrowserWindow.getFocusedWindow();
  window.minimize();
});

// Listen for windowSizeMaximise message from renderer process
ipcMain.on('windowSizeToggle', (event, arg) => {
  let win = BrowserWindow.getFocusedWindow();
  if (!win.isMaximized()) {
    win.maximize();
  } else {
    win.unmaximize();
  }
});

ipcMain.on('get:accounts', async (event, _) => {
  const accounts = await FileServices.AccountService.getAccount();
  mainWindow.webContents.send('accounts:data', accounts);
});

ipcMain.on('account:add', async (event, account) => {
  try {
    const _account = await FileServices.AccountService.saveAccount(account);
    mainWindow.webContents.send('account:added', _account);
  } catch (error) {}
});

ipcMain.on('account:delete', async (event, account) => {
  try {
    const _account = await FileServices.AccountService.deleteAccount(account);
    mainWindow.webContents.send('account:deleted', _account);
  } catch (error) {}
});

ipcMain.on('get:profiles', async (event, _) => {
  const profiles = await FileServices.ProfileService.getProfile();
  mainWindow.webContents.send('profiles:data', profiles);
});

ipcMain.on('profile:add', async (event, profile) => {
  try {
    const _profile = await FileServices.ProfileService.saveProfile(profile);
    mainWindow.webContents.send('profile:added', _profile);
  } catch (error) {}
});

ipcMain.on('profile:update', async (event, profile) => {
  try {
    const _profile = await FileServices.ProfileService.updateProfile(profile);
    mainWindow.webContents.send('profile:updated', _profile);
  } catch (error) {}
});

ipcMain.on('profile:delete', async (event, profile) => {
  try {
    const _profile = await FileServices.ProfileService.deleteProfile(profile);
    mainWindow.webContents.send('profile:deleted', _profile);
  } catch (error) {}
});

ipcMain.on('get:proxies', async (event, _) => {
  const proxies = await FileServices.ProxyService.getProxy();
  mainWindow.webContents.send('proxies:data', proxies);
});

ipcMain.on('proxies:add', async (event, proxy) => {
  try {
    const _proxy = await FileServices.ProxyService.saveProxy(proxy);
    mainWindow.webContents.send('proxies:added', _proxy);
  } catch (error) {}
});

ipcMain.on('proxies:update', async (event, proxy) => {
  try {
    const _proxy = await FileServices.ProxyService.updateProxy(proxy);
    mainWindow.webContents.send('proxies:updated', _proxy);
  } catch (error) {}
});

ipcMain.on('proxies:delete', async (event, proxy) => {
  try {
    const _proxy = await FileServices.ProxyService.deleteProxy(proxy);
    mainWindow.webContents.send('proxies:deleted', _proxy);
  } catch (error) {}
});

ipcMain.on('get:settings', async (event, _) => {
  const settings = await FileServices.SettingService.getSettings();
  mainWindow.webContents.send('settings:data', settings);
});

ipcMain.on('settings:add', async (event, setting) => {
  try {
    const _setting = await FileServices.SettingService.saveSettings(setting);
    mainWindow.webContents.send('settings:added', _setting);
  } catch (error) {}
});

ipcMain.on('get:tasks', async (event, _) => {
  const tasks = await FileServices.TaskService.getTask();
  mainWindow.webContents.send('tasks:data', tasks);
});

ipcMain.on('task:add', async (event, task) => {
  try {
    const _task = await FileServices.TaskService.saveTask(task);
    mainWindow.webContents.send('task:added', _task);
  } catch (error) {}
});

ipcMain.on('task:start', async (event, task) => {
  try {
    const _task =  new Yeezysupply(task).generateCookies()
    mainWindow.webContents.send('update:task:status', _task);
  } catch (error) {console.log(error)}
});

ipcMain.on('task:delete', async (event, task) => {
  try {
    const _task = await FileServices.TaskService.deleteTask(task);
    mainWindow.webContents.send('task:deleted', _task);
  } catch (error) {}
});

ipcMain.on('task:update', async (event, task) => {
  try {
    const _task = await FileServices.TaskService.updateTask(task);
    mainWindow.webContents.send('task:updated', _task);
  } catch (error) {}
});

ipcMain.on('loading', async (event, key) => {
  isauth = await Services.isActivated();
  if (isauth) {
    activationWindow.close();
    return createWindow();
  } else {
    activationWindow.webContents.send('activationstatusinput', 'input');
  }
})

ipcMain.on('tryActivationKey', async (event, key) => {
  activationWindow.webContents.send('activationstatus', 'authenticating....');
  const isActivated = await Services.activate(key);
  if (isActivated) {
    activationWindow.close();
    createWindow();
  } else {
    activationWindow.webContents.send('activationstatus', 'failed');
  }
});

ipcMain.on('open:captcha', (event, details) => {
  initCaptchaWindow();
});

//captcha

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

let captchaWindow;

async function initCaptchaWindow() {
  captchaWindow = new BrowserWindow({
    width: 400,
    height: 600,
    frame: false,
    webPreferences: {
      enableRemoteModule: true,
      preload: `${__dirname}/preload.js`,
      allowRunningInsecureContent: true,
      nodeIntegration: true,
    },
  });

  SetupIntercept();

  captchaWindow.loadURL('http://supremenewyork.com/');

  await sleep(1000);

  captchaWindow.on('close', function (e) {
    captchaWindow = null;
  });

  captchaWindow.webContents.session.webRequest.onBeforeRequest(
    { urls: ['https://myaccount.google.com/*'] },
    (details, callback) => {
      callback({ redirectURL: 'http://supremenewyork.com/' });
    },
  );
}

function SetupIntercept() {
  protocol.interceptBufferProtocol('http', (req, callback) => {
    if (req.url == 'http://supremenewyork.com/') {
      fs.readFile(__dirname + '/captcha.html', 'utf8', function (err, html) {
        callback({ mimeType: 'text/html', data: Buffer.from(html) });
      });
    } else {
      const request = net.request(req);
      request.on('response', (res) => {
        const chunks = [];

        res.on('data', (chunk) => {
          chunks.push(Buffer.from(chunk));
        });

        res.on('end', async () => {
          const file = Buffer.concat(chunks);
          callback(file);
        });
      });

      if (req.uploadData) {
        req.uploadData.forEach((part) => {
          if (part.bytes) {
            request.write(part.bytes);
          } else if (part.file) {
            request.write(fs.readFileSync(part.file));
          }
        });
      }

      request.end();
    }
  });
}

electron.ipcMain.on('openCapWindow', function (event, args) {
  initCaptchaWindow();
});

electron.ipcMain.on('sendCaptcha', function (event, token) {
  captchaBank.push({
    token: token,
    timestamp: moment(),
    host: 'http://supremenewyork.com/',
    sitekey: '6LeWwRkUAAAAAOBsau7KpuC9AV-6J8mhw4AjC3Xz',
  });

  // console.log(token,event)
});

setInterval(function () {
  for (var i = 0; i < captchaBank.length; i++) {
    if (moment().diff(moment(captchaBank[i].timestamp), 'seconds') > 110) {
      console.log('Removing Expired Captcha Token');
      captchaBank.splice(0, 1);
    }
  }
}, 1000);

function authWindow() {
  activationWindow = new BrowserWindow({
    width: 650,
    height: 175,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    },
  });
  activationWindow.loadURL(`file://${path.join(__dirname, './activation.html')}`);
  activationWindow.on('closed', () => (activationWindow = null));
}
