const fs = require('fs');
const path = require('path');

exports.AccountService = (function () {
  const p = path.join(__dirname, 'data', 'accounts.json');

  const getAccount = async () => {
    const data = await fs.readFileSync(p);
    const parsedJson = JSON.parse(data);
    return parsedJson;
  };


  const saveAccount = async (account) => {
    const data = await getAccount();
    const updated = [...data];
    updated.push(account);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return account;
  };

  const updateAccount = async (account) => {
    const data = await getAccount();
    const updated = [...data];
    const index = data.findIndex(item => item.id === account.id);
    if (index > -1) {
      updated[index] = account;
    } else {
      updated.push(account);
    }
    await fs.writeFileSync(p, JSON.stringify(updated));
    return account;
  };
  
  const deleteAccount = async (account) => {
    const data = await getAccount();
    const updated = data.filter(item => item.id !== account.id);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return account;
  };

  return {
    getAccount,
    saveAccount,
    updateAccount,
    deleteAccount,
  };
})();
