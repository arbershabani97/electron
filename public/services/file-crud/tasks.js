const fs = require('fs');
const path = require('path');

exports.TaskService = (function () {
  const p = path.join(__dirname, 'data', 'tasks.json');

  const getTask = async () => {
    const data = await fs.readFileSync(p);
    const parsedJson = JSON.parse(data);
    return parsedJson;
  };

  const saveTask = async (task) => {
    const data = await getTask();
    const updated = [...data];
    updated.push(task);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return task;
  };



  const updateTask = async (task) => {
    const data = await getTask();
    const updated = [...data];
    let index = updated.findIndex((x) => x.id == task.id);
    if (index > -1) updated[index] = task;
    await fs.writeFileSync(p, JSON.stringify(updated));
    return task;
  };

  const deleteTask = async (task) => {
    const data = await getTask();
    const updated = data.filter(item => item.id !== task.id);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return task;
  };

  return {
    getTask,
    saveTask,
    updateTask,
    deleteTask
  };
})();
