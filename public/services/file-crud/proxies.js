const fs = require('fs');
const path = require('path');

exports.ProxyService = (function () {
  const p = path.join(__dirname, 'data', 'proxies.json');

  const getProxy = async () => {
    const data = await fs.readFileSync(p);
    const parsedJson = JSON.parse(data);
    return parsedJson;
  };

  const saveProxy = async (proxy) => {
    const data = await getProxy();
    const updated = [...data];
    await updated.push(proxy);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return proxy;
  };

  const updateProxy = async (proxy) => {
    const data = await getProxy();
    const updated = [...data];
    let index = updated.findIndex(item => item.groupId == proxy.groupId);
    if(index>-1)updated[index] = proxy;
    await fs.writeFileSync(p, JSON.stringify(updated));
    return proxy;
  }

  const deleteProxy = async (proxy) => {
    const data = await getProxy();
    const updated = data.filter(item => item.groupId !== proxy.groupId);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return proxy;
  };

  return {
    getProxy,
    saveProxy,
    deleteProxy,
    updateProxy
  };
})();
