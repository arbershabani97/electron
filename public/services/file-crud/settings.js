const fs = require('fs');
const path = require('path');

exports.SettingService = (function () {
  const p = path.join(__dirname, 'data', 'settings.json');

  const getSettings = async () => {
    const data = await fs.readFileSync(p);
    const parsedJson = JSON.parse(data);
    return parsedJson;
  };

  const saveSettings = async (settings) => {
    const data = await getSettings();
    const updated = data;
    Object.keys(settings).forEach(key => updated[key] = settings[key]);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return settings;
  };

  const saveLicense = async (licenseInfo) => {
    const data = await getSettings();
    const updated = data;
    updated.authenticated = licenseInfo.authenticated;
    updated.loggedIn = licenseInfo.loggedIn;
    updated.deviceId = licenseInfo.deviceId;
    updated.username = licenseInfo.username;
    updated.userKey = licenseInfo.userKey;
    updated.discordUserID = licenseInfo.discordUserID;
    updated.email = licenseInfo.email;
    updated.expiredDate = licenseInfo.expiredDate;
    await fs.writeFileSync(p, JSON.stringify(updated));
    return updated;
  }

  const getActivationCode = async () => {
    const data = await getSettings();
    return data.activationKey;
  }

  return {
    getSettings,
    saveLicense,
    saveSettings,
    getActivationCode
  };
})();
