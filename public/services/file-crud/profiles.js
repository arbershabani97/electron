const fs = require('fs');
const path = require('path');

exports.ProfileService = (function () {
  const p = path.join(__dirname, 'data', 'profiles.json');

  const getProfile = async () => {
    const data = await fs.readFileSync(p);
    const parsedJson = JSON.parse(data);
    return parsedJson;
  };

  const saveProfile = async (profile) => {
    const data = await getProfile();
    const updated = [...data];
    updated.push(profile);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return profile;
  };



  const updateProfile = async (profile) => {
    let updatedProfile = profile.profile
    const data = await getProfile();
    const updated = [...data];
    let index = updated.findIndex(item => item.id == updatedProfile.id);
    if(index>-1)updated[index] = updatedProfile;
    await fs.writeFileSync(p, JSON.stringify(updated));
    return updatedProfile;
  }

  const deleteProfile = async (profile) => {
    const data = await getProfile();
    const updated = data.filter(item => item.id !== profile.id);
    await fs.writeFileSync(p, JSON.stringify(updated));
    return profile;
  };

  return {
    getProfile,
    saveProfile,
    updateProfile,
    deleteProfile,
  };
})();
