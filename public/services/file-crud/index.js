const { AccountService } = require('./accounts');
const { ProfileService } = require('./profiles');
const { ProxyService } = require('./proxies');
const { SettingService } = require('./settings');
const { TaskService } = require('./tasks');

exports.FileServices = (() => {
  return {
    AccountService,
    ProfileService,
    ProxyService,
    SettingService,
    TaskService
  };
})();
