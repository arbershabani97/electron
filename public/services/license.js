const os = require('os');
const util = require('util');
const { default: Axios } = require('axios');
const serialNumber = require('serial-number');
serialNumber.preferUUID = true;
const {
  LICENSE_API_HOST,
  BOT_NAME,
  BOT_VERSION
} = require("../../public/constants");

let userAgent = '';
try { userAgent = `${BOT_NAME} / ${BOT_VERSION} (${os.type()} ${os.arch()} ${os.release()})`; }
catch (err) { }

const configGenerator = function (url, body, method = 'post') {
  const licHeaders = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': userAgent,
    },
  };
  return {
    method: method,
    url: `${LICENSE_API_HOST}${url}`,
    data: body,
    headers: licHeaders
  };
}

const serialGenerator = async function () {
  const seriaNumberPromise = util.promisify(serialNumber);
  const deviceId =  await seriaNumberPromise();
  if (deviceId && !(deviceId instanceof Error)) {
    return deviceId;
  }
  return null;
}

module.exports.activateBot = async function (licenseKey) {
  const val = await serialGenerator();
  if (val) {
    const deviceId = val;
    const reqBody = {
      licenseKey: licenseKey,
      deviceId: deviceId,
    };
    return await Axios(configGenerator('/licensecheck', reqBody)).then((res) => {
      console.log(JSON.stringify(res?.data));
      const data = res?.data || {};
      data['activated'] = !!data.licenseinfo?.loggedIn;
      return (data);
    }).catch((e) => {
      console.error(e);
      return null;
    });
  } else {
    return null;
  }
};

module.exports.deactivateBot = async function (licenseKey) {
  const val = await serialGenerator();
  if (val) {
    const deviceId = val;
    const reqBody = {
      licenseKey: licenseKey,
      deviceId: deviceId,
    };
    return await Axios(configGenerator('/logoutlicense', reqBody)).then((res) => {
      console.log(JSON.stringify(res?.data));
      const data = res?.data || {};
      return data?.value || false;
    }).catch((e) => {
      console.error(e);
      return false;
    });
  } else {
    return null;
  }
};

module.exports.fetchUserProfile = async function (licenseKey) {
  const val = await serialGenerator();
  if (val) {
    const deviceId = val;
    const reqBody = {
      licenseKey: licenseKey,
      deviceId: deviceId,
    };
    return await Axios(configGenerator('/licensecheck', reqBody)).then((res) => {
      console.log(JSON.stringify(res?.data));
      const data = res?.data || {};
      data['activated'] = !!data.licenseinfo?.loggedIn;
      return (data);
    }).catch((e) => {
      console.error(e);
      return null;
    });
  } else {
    return null;
  }
};
