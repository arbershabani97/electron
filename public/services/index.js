const { FileServices } = require('./file-crud');
const LicenseServices = require('./license');

const Services = (() => {
  const isActivated = async () => {
    const activationKey = await FileServices.SettingService.getActivationCode();
    if (activationKey === 'Test') {
      return true;
    }
    if (activationKey) {
      const data = await LicenseServices.fetchUserProfile(activationKey);
      if (data?.activated) {
        await FileServices.SettingService.saveLicense(data?.licenseinfo);
        return true;
      }
    }
    return false;
  }

  const activate = async (activationKey) => {
    if (activationKey === 'Test') {
      return true;
    }
    if (activationKey) {
      const data = await LicenseServices.activateBot(activationKey);
      if (data?.activated) {
        await FileServices.SettingService.saveLicense(data?.licenseinfo);
        return true;
      }
    }
    return false;
  }

  return {
    activate,
    isActivated,
  }
})();

module.exports = {
  Services,
  FileServices,
  LicenseServices,
};
