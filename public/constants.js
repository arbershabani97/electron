const LICENSE_API_HOST = 'https://www.eliteaio.dev';
const BOT_NAME = 'EliteAIO';
const BOT_VERSION = '0.0.6-beta';

module.exports = {
  BOT_NAME,
  BOT_VERSION,
  LICENSE_API_HOST,
};
