const jsob = require('./jsob'); 
const fs = require('fs-extra'); 
const glob = require('glob');

const options = {
    'compact': true,
    'controlFlowFlattening': false,
    'deadCodeInjection': true,
    'deadCodeInjectionThreshold': 0.5,
    'debugProtection': false,
    'debugProtectionInterval': false,
    'disableConsoleOutput': true,
    'identifierNamesGenerator': 'hexadecimal',
    'log': false,
    'numbersToExpressions': true,
    'renameGlobals': false,
    'rotateStringArray': true,
    'selfDefending': true,
    'shuffleStringArray': true,
    'simplify': false,
    'splitStrings': false,
    'splitStringsChunkLength': 2,
    'stringArray': true,
    'stringArrayEncoding': 'base64',
    'target': 'node',
    'transformObjectKeys': true,
    'stringArrayThreshold': 0.75,
    'unicodeEscapeSequence': false
} 

function obfuscateFile(fileName) {
	const fileToRead = fs.readFileSync(fileName);
	const obfuscated = jsob.obfuscate(fileToRead.toString(), options);

	
	const splitFiles = fileName.split('/');
	for (let i = 0; i < splitFiles.length - 1; i++) {

	}

	const fileNameArr = fileName.split('/')
	let newArr = fileNameArr.slice();
	newArr.pop();
	fileDir = newArr.join('/');
	if (!fs.existsSync('./'+fileDir)) fs.mkdirSync('./'+fileDir, {recursive: true});
	
	try {
		const writeStream = fs.createWriteStream('./'  + fileName);
		writeStream.write(obfuscated.getObfuscatedCode());
		writeStream.end();
	} catch (e) {
		console.error(e);
	}
}

function obfuscateDir(dirName) {
	console.log(dirName);
	glob(dirName + '/**/*.js', (err, files) => {
		for (file of files) {
			obfuscateFile(file);
		}
	});
}

obfuscateDir(process.argv[2]);
