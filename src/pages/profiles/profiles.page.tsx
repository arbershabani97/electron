import React, { useState, useEffect } from 'react';
import {
  ProfilesContainer,
  ProfileCardContainer,
  ProfilesMainBox,
  ProfilesTopRow,
  ProfilesGrid,
  Button, ProfileIconButtons
} from './profiles.styles';
import { Bottom, Top } from '../../global.styles';
import pencilIcon from '../../assets/profiles/pencil-icon.svg';
import paintIcon from '../../assets/profiles/paint-icon.svg';
import deleteIcon from '../../assets/profiles/delete-icon.svg';
import addIcon from '../../assets/profiles/add-icon.svg';
import ccIcon from '../../assets/profiles/cc-icon.svg';
import VersionNumber from '../../components/version-number/version-number.component';
import UserName from '../../components/username/username.component';
import PopupContainer from '../../components/common/popup.container';
import CreateTask from '../../components/create-task/create-task.component';
import AddProfile from '../../components/add-profile/addProfile.component';
import { IProfile as Profile } from '../../redux/profile/types';

import { fetchRequest, deleteRequest, addRequest, exportRequest, importRequest } from '../../redux/profile/actions';
import { ApplicationState } from '../../redux';
import { connect } from 'react-redux';
import edit from "../../assets/images/edit.svg";
import del from "../../assets/images/delete.svg";
import { TaskIconBtnCtn, TaskIconButtons } from '../../components/taskItem/taskItem.styles';


export interface IProfilePageState {
  isShowingProfileForm: boolean;
  export: boolean;
  isShowingImportProfileForm: boolean;
  isEditingProfile: boolean;
  profile?: any;
}

interface PropsFromState {
  loading: boolean;
  data: Profile[];
  errors?: string;
}

interface PropsFromDispatch {
  fetchProfile: typeof fetchRequest;
  deleteProfile: typeof deleteRequest;
  copyProfile: typeof addRequest;
  exportProfile: typeof exportRequest;
}

type AllProps = PropsFromDispatch & PropsFromState;

class ProfilesPage extends React.Component<AllProps, IProfilePageState> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShowingProfileForm: false,
      isShowingImportProfileForm: false,
      export: false,
      isEditingProfile: false,
    };
    this.createNewProfile = this.createNewProfile.bind(this);
    this.exportProfile = this.exportProfile.bind(this);
    this.importProfile = this.importProfile.bind(this);
    this.closeWindows = this.closeWindows.bind(this);
  }

  public componentDidMount() {
    const { data, fetchProfile } = this.props;

    if (!data?.length) {
      fetchProfile();
    }
  }

  createNewProfile(event: React.MouseEvent) {
    event.preventDefault();
    this.setState({
      isEditingProfile: false,
      isShowingProfileForm: !this.state.isShowingProfileForm,
      isShowingImportProfileForm: this.state.isShowingImportProfileForm,
      profile: ''
    });
  }

  editProfile(profile: Profile) {
    this.setState({
      isShowingProfileForm: !this.state.isShowingImportProfileForm,
      isEditingProfile: true,
      profile: profile,
    });
  }

  deleteProfile(profile: Profile) {
    this.props.deleteProfile(profile)
  }

  async exportProfile(event: React.MouseEvent) {
    //event.preventDefault();
    //alert(JSON.stringify(convertToAycd(this.props.data)));
  }

  importProfile(event: React.MouseEvent) {
    event.preventDefault();
    this.setState({
      isShowingProfileForm: this.state.isShowingProfileForm,
      isShowingImportProfileForm: !this.state.isShowingImportProfileForm,
    });
    //navigator.clipboard.writeText(JSON.stringify(convertToAycd(this.props.data)));
  }

  closeWindows() {
    this.setState({
      isShowingProfileForm: false,
      isShowingImportProfileForm: false,
    });
  }

  readonly maskCardNumber = (cardNumber: string): string => {
    console.log(cardNumber.replace(/\d{4}-(?=\d{4})/g, '*'));
    return cardNumber.replace(/\d{4}-(?=\d{4})/g, '*');
  };

  render() {
    return (
      <ProfilesContainer>
        <UserName />
        <ProfilesMainBox>
          <ProfilesTopRow>
            <span>Profiles</span>
            <img className="icon" src={addIcon} alt="Add Profile" onClick={this.createNewProfile} />
            {this.state.isShowingProfileForm && (
              <Top>
                <AddProfile
                  dismiss={() => { this.setState({ isShowingProfileForm: !this.state.isShowingProfileForm }) }}
                  passedFunction={this.state.isShowingImportProfileForm}
                  profile={this.state.profile}
                />
              </Top>
            )}
            <Button onClick={() => this.state.isShowingProfileForm}></Button>
          </ProfilesTopRow>
          <ProfilesGrid>
            {this.props.data.map((props: Profile) => {
              return (
                <ProfileCardContainer key={props.id} className={props?.color}>
                  <ProfileIconButtons >
                    <TaskIconBtnCtn onClick={() => { this.editProfile(props) }}>
                      <img
                        src={edit}
                        className="App-logo"
                        alt="icon"
                        width="15"
                        height="15"
                      />
                    </TaskIconBtnCtn>
                    <TaskIconBtnCtn onClick={() => { this.deleteProfile(props) }}>
                      <img
                        src={del}
                        className="App-logo"
                        alt="icon"
                        width="15"
                        height="15"
                      />
                    </TaskIconBtnCtn>
                  </ProfileIconButtons>
                  <span className="name">{props?.name}</span>
                  <img src={ccIcon} className="cc-icon" />
                  <span className="numbers">{this.maskCardNumber(props?.paymentInfo?.cardNumber)}</span>
                  <span className="cardholder">{props?.paymentInfo?.cardHolderName}</span>
                </ProfileCardContainer>
              );
            })}
          </ProfilesGrid>
        </ProfilesMainBox>
        <VersionNumber />
      </ProfilesContainer>
    );
  }
}

const mapDispatchToProps = {
  fetchProfile: fetchRequest,
  deleteProfile: deleteRequest,
  copyProfile: addRequest,
  exportProfile: exportRequest,
  importProfile: importRequest,
};

const mapStateToProps = ({ profiles }: ApplicationState) => ({
  loading: profiles.loading,
  errors: profiles.errors,
  data: profiles.data,
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilesPage);
