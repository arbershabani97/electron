import styled from 'styled-components';
import cardBackground from '../../assets/images/card-backgrond-logo.png';
import darkBlue from '../../assets/profiles/Sunburstdark-blue.svg'
import cardOrange from '../../assets/profiles/orange.svg'
import pink from '../../assets/profiles/pink.svg'
import blue from '../../assets/profiles/blue.svg'
import grey from '../../assets/profiles/grey.svg'
import red from '../../assets/profiles/red.svg'




export const ProfilesContainer = styled.div`
  color: white;
  width: 100%;
  min-height: calc(100vh - 165px);
  display: flex;
  flex-direction: row;

  .card-main {
    background-image: url(${blue}), linear-gradient(13.04deg, #cd7cff 41.44%, #8be3ff 80.21%);
  }

  .card-grey {
    background-image: url(${grey}), linear-gradient(13.04deg, #bdc2e8 1.29%, #ebecf2 98.18%);
  }

  .card-green {
    background-image: url(${blue}), linear-gradient(13.04deg, #43e97b 1.29%, #38f9d7 98.18%);
  }

  .card-orange {
    background-image: url(${cardOrange}), linear-gradient(13.04deg, #fe0f80 1.29%, #fbf871 98.18%);
  }

  .card-darkblue {
    background-image: url(${darkBlue}), linear-gradient(13.04deg, #13547a 1.29%, #80d0c7 98.18%);
  }

  .card-bluegreen {
    background-image: url(${cardBackground}), linear-gradient(13.04deg, #2af598 1.29%, #009efd 98.18%);
  }

  .card-blue {
    background-image: url(${blue}), linear-gradient(13.04deg, #57fbfe 1.29%, #565efe 98.18%);
  }

  .card-purple {
    background-image: url(${cardBackground}), linear-gradient(13.04deg, #9a00fb 1.29%, #f000c7 98.18%);
  }

  .card-red {
    background-image: url(${red}), linear-gradient(13.04deg, #ff0844 1.29%, #ffb199 98.18%);
  }

  .card-pink {
    background-image: url(${pink}), linear-gradient(13.04deg, #ff5670 1.29%, #ff57f4 98.18%);
  }
`;

export const ProfileCardContainer = styled.div`
  border-radius: 18px;
  padding: 15px;
  height: max-content;

  background-repeat: no-repeat;
  background-position: right 25% top 61%,center;
  background-size: auto 60%,cover;

  display: flex;
  flex-direction: column;
  align-items: flex-start;

  .cc-icon {
    margin-top: 15px;
    height: 29px;
    margin-bottom: 15px;
  }

  .numbers {
    margin-bottom: 5px;
    letter-spacing: 4px;
  }

  .cardholder {
    text-transform: uppercase;
    font-size: 15px;
    letter-spacing: -0.5px;
  }
`;

export const ProfilesGrid = styled.div`
  display: grid;
  grid-template-columns: 31% 31% 31%;
  column-gap: 3%;
  row-gap: 25px;
  overflow-y: auto;

  @media only screen and (min-width: 1400px) {
    grid-template-columns: 20% 20% 20% 20%;
    column-gap: 5%;
  }
`;

export const ProfilesMainBox = styled.div`
  border: 2px solid #acecfd;
  // box-sizing: border-box;
  height: 82vh;
  width: 90vw;
  // flex-grow:2;
  padding: 25px;
  padding-bottom: 0;
  display: flex;
  flex-direction: column;
  box-shadow: 0px 2px 12px rgba(172, 236, 253, 0.5), 0px -2px 12px rgba(172, 236, 253, 0.5);
  overflow-y: auto;
`;

export const ProfilesTopRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 23px;

  span {
    color: #acecfd;
    font-weight: regular;
    font-size: 22px;
    margin-right: 13px;
  }

  .icon {
    box-shadow: 0px -1px 15px rgba(174, 253, 172, 0.5), 0px 10px 50px rgba(174, 253, 172, 0.5);
    border-radius: 50%;
    width: 15px;
    cursor: pointer;
  }
`;

export const Button = styled.span`
`;

export const ProfileIconButtons = styled.div`
    display:flex;
    justify-content:flex-end;
    width:100%;
`;