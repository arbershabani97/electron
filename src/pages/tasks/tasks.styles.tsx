import styled from 'styled-components';

export const TaskPageContainer = styled.div`
  display: flex;
  min-height: calc(100vh - 165px);

`;

export const TaskItemsContainer = styled.div`
  height: 82vh;
  width: 90vw;
  // flex-grow:5;
  border: 1px solid #acecfd;

  display: flex;
  box-shadow: 0px 2px 12px rgba(172, 236, 253, 0.5), 0px -2px 12px rgba(172, 236, 253, 0.5);
`;

export const TaskActionContainer = styled.div`
  margin-top:12px;  
  height: 20px;
  width: 100%;
  display: flex;
  padding: 0 15px;
  z-index: 100;
  justify-content: space-between;
  color:blue;
`;

export const TaskActionButtonsContainer = styled.div`
  // background-color:red;
  margin-bottom: 3vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const TaskActionButtonText = styled.div`
  font-style: normal;
  font-size: 22px;
  line-height: 85px;
  display: flex;
  align-items: center;
  letter-spacing: 0.075em;
  color: #eefbff;
`;
export const TaskActionButtonContainer = styled.div`
  box-shadow: 0px -10px 50px rgba(172, 236, 253, 0.5), 0px 10px 50px rgba(172, 236, 253, 0.5);
  margin-bottom: 15px;
`;

export const TaskText = styled.div`
  height: 75vh;
  width: 5vw;
  color: #bdbdbd;
  display: flex;
  align-items: center;
  padding-top: 50px;
  // flex-direction:column;
  // justify-content:flex-end;
  transform: rotate(-90deg);

  // background-color:blue;
`;

export const TaskUserName = styled.div`
  color: #eefbff;
  font-size: 15px;
  line-height: 61px;
  display: flex;
  align-items: center;

  color: #eefbff;

  transform: rotate(-90deg);
  margin-right: 40px;
  margin-bottom: 20px;
`;

export const TaskRightContainer = styled.div`
  width: 39%;
  height: 100;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding-top: 2%;
  padding-bottom: 1%;
  padding-right: 1%;
  padding-left: 1%;
`;
export const TaskLeftContainer = styled.div`
    width:60%;
    height:100%:
    background-color:blue;
    color:orange;
`;
export const TaskStatText = styled.div`
  font-style: normal;
  font-size: 25px;
  line-height: 70px;
  display: flex;
  padding-left: 15px;
  align-items: center;
  letter-spacing: 0.075em;
  color: #ACECFD;
`;

export const TaskStatContainer = styled.div`
  width: 101%;
  height: 32%;
  margin-left: -20px;
  blackground-color: blue;
  display: flex;
  justify-content: space-between;
`;

export const TaskStatItem = styled.div`
  background: #0c0c0c;
  border-radius: 8px;
  marigin: 150px;
  width: 31%;
  height: 43%;
  display: flex;
  justify-content: space-between;
  padding: 0 15px;
  align-items: center;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
`;

export const TaskStatNumber = styled.div`
  font-size: 20px;
  line-height: 88px;
  display: flex;
  align-items: center;

  text-shadow:1px 0px 20px rgba(172,236,253,0.5), 4px 0px 20px rgba(172,236,253,0.5);
`;
export const TaskStatContainerText = styled.div`
  padding-left: 20px;
  // color: #eefbff;
  font-size: 23;
  text-transform: uppercase;
`;

export const TaskListContainer = styled.div`
  height: 62%;
  width: 101.2%;
  margin-top: -75px;
  overflow: scroll;
  padding-right: 45px;
  padding-left: 15px;
`;

export const TaskUpcomingContainer = styled.div`
  height: 87%;
  width: 60%;
  // background-color:red;
  overflow: scroll;
  padding-top: 10px;
`;

export const UpComongText = styled.div`
  color: #c5c5c5;
  font-size: 20px;
`;

export const UpcominItemContainer = styled.div`
  width: 17vw;
  height: 30vh;
  box-shadow: -3px -1px 4px rgba(66, 66, 66, 0.41), 4px 4px 9px #0d0d0d;
  border-radius: 10px;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  font-size: 13px;
`;

export const ItemName = styled.div`
  text-align: center;
  color: #cbcbcb;
`;

export const ItemFooter = styled.div`
  color: #bdbdbd;
  display: flex;
  justify-content: space-evenly;
  width: 80%;
`;

export const TaskDataContainer=styled.div`
  width: 93%;
  height: 75%;
  margin:auto;

  // background: black;
  /* pkiouy7 */

  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 4px black;
  border-radius: 10px;
  display:flex;
  flex-wrap: wrap;
  justify-content:space-evenly;
  padding-top:5px;
  padding-bottom: 5px;
  overflow:scroll;
`;


export const TaskDataBox=styled.div`
  display:flex;
  flex-direction:column;
  justify-content:space-evenly;
  align-items:center;
  padding:20px;
  width: 40%;
  height: 35%;
  margin:5px;
  color: #767676;
  font-size:12px;
  // background: #0d0d0d;
  box-shadow: inset 0px 4px 4px rgba(0, 0, 0, 0.25), inset 4px 1px 3px #0D0D0D, inset -4px -2px 0px #121212;;
  border-radius: 8px;
`;

export const TaskBtn=styled.div`
width:48%;
height:35px;
text-align:center;
padding-top:10px;
background: #0c0c0c;
/* pkiouy7 */

box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
border-radius: 5px;
`;

export const TextGlow = styled.div`
text-shadow: 5px 4px 10px #C9C9C9, -1px -1px 9px rgba(255, 255, 255, 0.66);
`