import React from 'react';
import {
  TaskPageContainer,
  TaskItemsContainer,
  TaskActionContainer,
  TaskText,
  TaskActionButtonsContainer,
  TaskActionButtonText,
  TaskActionButtonContainer,
  TaskUserName,
  TaskLeftContainer,
  TaskRightContainer,
  TaskStatText,
  TaskStatContainer,
  TaskStatItem,
  TaskStatNumber,
  TaskStatContainerText,
  TaskListContainer,
  UpComongText,
  TaskUpcomingContainer,
  UpcominItemContainer,
  ItemName,
  ItemFooter,
  TaskBtn,
  TaskDataBox,
  TaskDataContainer, TextGlow
} from './tasks.styles';

import start from '../../assets/images/start.svg';
import stop from '../../assets/images/stop.svg';
import del from '../../assets/images/delete.svg';
import addTask from '../../assets/images/addTask.svg';
import checks from '../../assets/images/checks.svg';
import TaskItem from '../../components/taskItem/taskItem.component';

import { connect } from 'react-redux';
import { Bottom, Top } from '../../global.styles';
import NewTask from '../../components/new-task/newTask.component';

import { Upcoming } from '../../redux/upcoming/types';
import { fetchRequest as fetchTasksRequest, createGroup, getTasks, updateGroupName, clearTasks, deleteGroup, runTask } from '../../redux/task/actions';
import { fetchRequest } from '../../redux/upcoming/actions';
import { ApplicationState } from '../../redux';
import VersionNumber from '../../components/version-number/version-number.component';
import UserName from '../../components/username/username.component';
import { ITask as Task } from '../../redux/task/types';
import { runAllTask, stopAllTask, addRequest, deleteRequest, updateRequest, multiUpdateRequest } from '../../redux/task/actions';
import { uid } from '../proxies/proxyHelper';
import MultiEditTask from '../../components/multi-edit-task/multiEditTask.component';
import Input from '../../components/input/input.component';
// const electron = window.require('electron');
// const ipcRenderer = electron.ipcRenderer;

export interface ITaskPageState {
  isShowingTaskForm: boolean;
  isShowingMultiEditTaskForm: boolean;
  taskId?: string;
  groupName: any;
  groups: any;
  groupId: any;
}

interface PropsFromState {
  loading: boolean;
  data: [];
  group: [];
  errors?: string;
  proxies:any;
}

// We can use `typeof` here to map our dispatch types to the props, like so.
interface PropsFromDispatch {
  addRequest: any;
  updateRequest: any;
  multiUpdateRequest: any;
  deleteRequest: any;
  fetchReleaseDates: typeof fetchRequest;
  fetchTasks: typeof fetchTasksRequest;
  startAll: any;
  stopAll: any;
  createGroup: any;
  getTasks: any;
  updateGroupName: any;
  clearTasks: any;
  deleteGroup: any;
  runTask : typeof runTask;
}

interface RouteParams {
  name: string;
}

interface State {
  selected?: Upcoming;
}

// Combine both state + dispatch props - as well as any props we want to pass - in a union type.
type AllProps = PropsFromState & PropsFromDispatch;

const enterKeyCode = 13;

class TaskPage extends React.Component<AllProps, ITaskPageState> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShowingTaskForm: false,
      isShowingMultiEditTaskForm: false,
      groupName: [],
      groups: [],
      groupId: ''

    };
    this.createNewTask = this.createNewTask.bind(this);
    this.closeNewTask = this.closeNewTask.bind(this);
  }

  public componentDidMount() {
    const { data, fetchReleaseDates, fetchTasks } = this.props;

    if (!data?.length) {
      fetchReleaseDates();
      fetchTasks();
    }
  }

  createNewTask(event: React.MouseEvent) {
    // event.preventDefault();
    this.setState({ isShowingTaskForm: true });
  }

  saveTask = (task: any, id?: any) => {
    if (!id) {
      for (let i = 0; i < task?.computed?.taskCount; i++) {
        let updatedTask = { ...task, id: uid() }
        this.props.addRequest(updatedTask, this.state.groupId)
      }
    } else {
      this.props.updateRequest(task, this.state.groupId)
      this.setState({ taskId: '' })
    }

    this.setState({ isShowingTaskForm: false });
  }

  deleteTask = (task: any) => {
    this.props.deleteRequest(task, this.state.groupId)
  }

  deleteAllTask = () => {
    this.props.deleteRequest([], this.state.groupId)
    this.setState({
      taskId: ''
    });
  }

  duplicateTask = (task: any) => {
    let updatedTask = { ...task, id: uid() }
    this.props.addRequest(updatedTask, this.state.groupId)
  }

  editTask = (task: any) => {
    this.setState({ isShowingTaskForm: true, taskId: task.id });
  }

  closeNewTask(event: React.MouseEvent) {
    event.preventDefault();
    this.setState({ isShowingTaskForm: false, taskId: '' });
  }

  massEdit = () => {
    this.setState({
      isShowingMultiEditTaskForm: true,
    });
  }

  closeMultiEdit = () => {
    this.setState({
      isShowingMultiEditTaskForm: false,
    });
  }

  multiUpdate = (task: any) => {
    this.props.multiUpdateRequest(task, this.state.groupId);
    this.setState({
      isShowingMultiEditTaskForm: false,
    });
  }

  startAllTasks() {
    this.props.startAll(this.props.data);
  }

  stopAllTasks() {
    this.props.stopAll(this.props.data);
  }

  handleChange(i: any, event: any) {
    this.setState(
      {
        groupName: { ...this.state.groupName, [i]: event.target.value }
      }
    );
  }

  handleGroup = (taskId: string, index: number, name: string) => {
    this.setState({
      groups: { ...this.state.groups, [taskId]: true },
      groupName: { ...this.state.groupName, [index]: name }
    })
  }

  getGroupName = (groupId: string) => {
    let groups = this.props.group
    let groupName = groups.find((item: any) => item.id == groupId)
    if (groupName) {
      return groupName['name']
    }
  }

  handleSubmit = () => {
    let groupName = `Group_${(this.props.group.length === 0) ? 1 : this.props.group.length + 1}`;
    let id = uid(32)
    if (!this.state.groupId && (this.props.data.length > 0)) this.props.createGroup(id, this.props.data, groupName)
    this.props.clearTasks()
    this.setState({ groupName: [], groupId: '' })
  }

  openTasks = (id: any) => {
    this.props.getTasks(id)
    this.setState({ groupId: id })
  }

  handleKeyPress = (event: any, id: any) => {
    if (event.keyCode === enterKeyCode) {
      this.setState({
        groups: { ...this.state.groups, [id]: false }
      })
      this.props.updateGroupName(event.target.value, id)

    }
  }

  deleteGroup = () => {
    this.props.deleteGroup(this.state.groupId);
    this.setState({
      groupId: '',
    })
  }
   
  getProxyCount = (data: []) => {
    let set = new Set();
    data.forEach((item: any) => {
      if (typeof item.proxy === 'string') {
        set.add(item.proxy)
      }
    })
    let count = 0
    set.forEach(item => {
      let proxy = this.props.proxies.find((proxy: any) => proxy.groupId == item)
      if (proxy) count += proxy.groupList ? proxy.groupList.length : 0;
    })
    return count;
  }

  startTask = (task:any) =>{
    this.props.runTask(task)
  }

  render() {
    return (
      <TaskPageContainer>
        <UserName />
        <Bottom>
          <TaskItemsContainer>
            <TaskLeftContainer>
              <TaskStatText>{
                this.state.groupId ? <div>{this.getGroupName(this.state.groupId)}</div> : <div className=''>Overview</div>}

                <img
                  onClick={this.createNewTask}
                  src={addTask}
                  className='App-logo'
                  alt='icon'
                  width='50'
                  height='50'
                />
                {this.state.groupId ?
                  <img
                    onClick={this.deleteGroup}
                    src={del}
                    className='App-logo'
                    alt='icon'
                    width='15'
                    height='15'
                  /> : <></>
                }

              </TaskStatText>

              <TaskStatContainer>
                <TaskStatItem
                  style={{
                    color: '#ACECFD',
                  }}>
                  <TaskStatNumber>261</TaskStatNumber>
                  <TaskStatContainerText>carted</TaskStatContainerText>
                </TaskStatItem>
                <TaskStatItem
                  style={{
                    color: '#ACFDB4',
                  }}
                >
                  <TaskStatNumber>143</TaskStatNumber>
                  <TaskStatContainerText>checkouts</TaskStatContainerText>
                </TaskStatItem>
                <TaskStatItem
                  style={{
                    color: '#FDACAC',
                  }}
                >
                  <TaskStatNumber>118</TaskStatNumber>
                  <TaskStatContainerText>failed</TaskStatContainerText>
                </TaskStatItem>
              </TaskStatContainer>
              <TaskListContainer>
                {this.props.data.map((data, i) => {
                  return <TaskItem key={i} task={data} id={i} duplicateTask={this.duplicateTask} deleteRequest={this.deleteTask} editRequest={this.editTask} startTask={this.startTask} />;
                })}
              </TaskListContainer>
              <TaskActionContainer>
                <div
                  style={{
                    width: '43%',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <TaskBtn
                    style={{
                      color: '#AEFDAC',
                      fontSize: '13px'
                    }}
                    onClick={() => this.startAllTasks()}
                  >
                    Start All
                  </TaskBtn>
                  <TaskBtn
                    style={{
                      color: '#ACECFD',
                      fontSize: '13px'
                    }}
                    onClick={() => this.stopAllTasks()}
                  >
                    Stop All
                  </TaskBtn>
                </div>
                <div
                  style={{
                    width: '43%',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <TaskBtn
                    style={{
                      color: '#FFF495',
                      fontSize: '13px'
                    }}
                    onClick={() => this.massEdit()}
                  >
                    Mass Edit
                  </TaskBtn>
                  <TaskBtn
                    style={{
                      color: '#FDACAC',
                      fontSize: '13px'
                    }}
                    onClick={() => this.deleteAllTask()}
                  >
                    Delete All
                  </TaskBtn>
                </div>
              </TaskActionContainer>
            </TaskLeftContainer>
            <TaskRightContainer>
              <TaskDataContainer>
                <TaskDataBox
                  style={{
                    transform: 'scale(0.5)'
                  }}
                >
                  <div
                    style={{
                      color: 'white',
                      fontSize: '80px'
                    }}
                    onClick={() => this.handleSubmit()}
                  >
                    +
                 </div>
                </TaskDataBox>
                {this.props.group.map((task: any, i) => {
                  return (
                    <TaskDataBox
                      style={{
                        ...(task.id === this.state.groupId) ? {
                          boxShadow: '0px 2px 12px rgba(172,236,253,0.5), 0px -2px 12px rgba(172,236,253,0.5)',
                          border: '1px solid #acecfd'
                        } : {}
                      }}
                      onClick={() => this.openTasks(task.id)} key={i}>
                      <div
                        style={{
                          fontWeight: 'bold',
                          color: '#FFFF',
                          fontSize: '14px',
                        }}
                      >
                        {!this.state.groups[task.id] && <TextGlow className='textGlow' onClick={() => this.handleGroup(task.id, i, task.name)}>{task.name}</TextGlow>}
                        {this.state.groups[task.id] ? <div>
                          <Input
                            grow={true}
                            placeholder=""
                            id=""
                            value={this.state.groupName[i]}
                            name={this.state.groupName[i]}
                            handleChange={this.handleChange.bind(this, i)}
                            type="text"
                            keyPress={(e: any) => { this.handleKeyPress(e, task.id) }}
                          />
                        </div> : null}
                      </div>
                      <div style={{
                        paddingTop: '8px',
                        fontSize: '11px'
                      }}>Tasks: {task.data ? task.data.length : ""}</div>
                      <div style={{
                        paddingTop: '3px',
                        fontSize: '11px'
                      }}>Proxies:{this.getProxyCount(task.data)}</div>
                      <div style={{
                        paddingTop: '14px',
                        fontSize: '12px'
                      }}>Checkouts</div>
                      <div style={{
                        paddingTop: '2px',
                        fontSize: '13px'
                      }}>123</div>
                    </TaskDataBox>
                  );
                })}
              </TaskDataContainer>
            </TaskRightContainer>
          </TaskItemsContainer>
        </Bottom>
        {this.state.isShowingMultiEditTaskForm ? (
          <Top>
            <MultiEditTask dismiss={this.closeMultiEdit} passedFunction={this.multiUpdate} />
          </Top>
        ) :
          (
            <div></div>
          )}
        {this.state.isShowingTaskForm ? (
          <Top>
            <NewTask dismiss={this.closeNewTask} passedFunction={this.saveTask} taskId={this.state.taskId} />
          </Top>
        ) : (
            <div></div>
          )}
        <VersionNumber />
      </TaskPageContainer>
    );
  }
}

const mapStateToProps = ({ tasks ,proxies }: ApplicationState) => ({
  loading: tasks.loading,
  errors: tasks.errors,
  data: tasks.data,
  group: tasks.group,
  proxies: proxies.proxies
});

// mapDispatchToProps is especially useful for constraining our actions to the connected component.
// You can access these via `this.props`.
const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchTasks: () => dispatch(fetchTasksRequest()),
    addRequest: (task: any, id: any) => dispatch(addRequest(task, id)),
    updateRequest: (task: any, groupId?: any) => dispatch(updateRequest(task, groupId)),
    multiUpdateRequest: (task: any, groupId: any) => dispatch(multiUpdateRequest(task, groupId)),
    deleteRequest: (task: any, groupId: any) => dispatch(deleteRequest(task, groupId)),
    fetchReleaseDates: fetchRequest,
    startAll: (tasks: any) => dispatch(runAllTask(tasks)),
    stopAll: (tasks: any) => dispatch(stopAllTask(tasks)),
    createGroup: (id: string, tasks: any, name: any) => dispatch(createGroup(tasks, name, id)),
    getTasks: (task: any) => dispatch(getTasks(task)),
    updateGroupName: (name: any, id: any) => dispatch(updateGroupName(name, id)),
    clearTasks: () => dispatch(clearTasks()),
    deleteGroup: (id: string) => dispatch(deleteGroup(id)),
    runTask : (task:any) => dispatch(runTask(task))
  }
};

// Now let's connect our component!
// With redux v4's improved typings, we can finally omit generics here.
export default connect(mapStateToProps, mapDispatchToProps)(TaskPage);
