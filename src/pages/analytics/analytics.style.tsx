import styled from 'styled-components';

export const AnalyticsPageContainer = styled.div`
    color: white;
    width: 100%;
    min-height: calc(100vh - 165px);
    display: flex;
    flex-direction: row;
`;

export const AnalyticsMainContainer = styled.div`
    border: 1px solid #acecfd;
    box-sizing: border-box;
     
    flex-grow:2;
    height: calc(103vh - 149px);
    padding: 25px;
    display: flex;
    flex-direction: inherit;
    box-shadow: 0px 2px 12px rgba(172, 236, 253, 0.5), 0px -2px 12px rgba(172, 236, 253, 0.5);
    
`;

export const AnalyticsSectionFirst = styled.div`
    width:55%;
`;

export const AnalyticsSectionSecond = styled.div`
  width:45%;
  padding: 10px 10px 25px 35px;
`;



export const AnalyticsFilterContainer = styled.div`
    width:100%;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    padding-bottom: 0px;
    margin-left: -30px;
`;

export const TextContainer = styled.p`
    text-transform: capitalize;
    text-align: center;
    margin: 10px;
    font-size:13px;
`;

export const AnalyticsIndFilter = styled.div`
    width: 100px;
    height: 34px;
    background: #0c0c0c;
    color:#ffffff;
    box-shadow: -1px -1px 2px rgba(60,60,60,0.25), 2px 2px 4px black;
    border-radius: 10px;
    margin-left: -5px;
    margin-bottom: 15px;
    margin-top: -10px;
`;

export const AnalyticsCountViewContainer = styled.div`
    width: 102%;
    height: 29vh;
    margin-top:-30px;
    padding:20px;
    margin-left:-33px;
    background: #0c0c0c;
    box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
    border-radius: 10px;
`;

export const RetailItemCount = styled.div`
    color: #ACECFD;
    font-size: 18px;
    filter: drop-shadow(rgb(172, 236, 253) 0px 0px 0.75rem);
`;

export const RetailItemText = styled.div`
    margin: 5;
    color: #CBCBCB; 
    font-size: 12px;
`;
export const RetailImageContainer = styled.div`
    display: flex;
    flex: 1 1 0%;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    height: 20vh;
`;

export const RetailCostText = styled.div`
    justify-content: center;
    align-items: center;
    margin: 0px;
    display: flex;
    width: 100%;
    color: rgb(203, 203, 203);
    font-size: 12px;
`;

export const AnalyticsItemContainer = styled.div`
    
    width: 102%;
    height: 45.8vh;
    margin-top:15px;
    margin-left:-33px;
    padding:15px;
    background: #0c0c0c;
    box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
    border-radius: 10px;
`;

export const AnalyticsCountHeaderContainer = styled.div`
    width:100%;
    padding-top: 5px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;
export const AnalyticsItemsContainer = styled.div`
    width:100%;
    margin-top: 30px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;
export const AnalyticsReleasesText = styled.div`
    width: 100%;
    font-size: 20px;
    margin-bottom:10px;
    align-items: center;
    color: #ACECFD;
`;

export const AnalyticsReleasesContainer = styled.div`
    height: 55%;
    width:100%;
    display: flex;
    flex-wrap: wrap;
`;

export const AnalyticsCalanderContainer = styled.div`
    width:50%;
    height:80%;
`;

export const AnalyticsProductContainer = styled.div`
    width:50%;
`;

export const AnalyticsSitelistContainer = styled.div`
    width:105%;
    height: 40vh !important;
    overflow: scroll;
    background: #0c0c0c;
    box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
    border-radius: 10px;
    padding:5px;
`;

export const SitelistHeader = styled.div`
    font-size: 21px;
    align-items: center;
    text-align: center;
    padding: 13px 0px 22px 0px;
    filter: drop-shadow(rgb(172, 236, 253) 0px 0px 0.75rem);
    letter-spacing: 0.075em;
    color: #ACECFD;
    text-shadow: 0px 10px 50px rgba(172, 236, 253, 0.55), 0px -10px 50px rgba(172, 236, 253, 0.5);
`;

export const Sitelist = styled.div`
    height: 21%;
    width:90%
    align-items: center;
    text-align: center;
    letter-spacing: 0.025em;
    color: #FFFFFF;
    margin-bottom: 8px;
    padding: 1px;
    background: #0c0c0c;
    box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
    border-radius: 10px;
`;

export const ContentContainer = styled.div`
    align-items: center;
    text-align: center;
    letter-spacing: 0.075em;
    color: #FFFFFF;
    width: 96%;
    margin-left: 10px;
    margin-bottom: 10px;
    padding: 10px;
    background: #0C0C0C;
    box-shadow: 4px 4px 9px #0D0D0D, -3px -1px 4px rgba(66, 66, 66, 0.41);
    border-radius: 10px;
`;

export const SiteListTitle = styled.p`
    margin: 0px;
    margin-bottom: 0px;
    font-size: 12px;
`;

export const SiteListText = styled.p`
    margin: 3px;
    font-size: 10px;
`;

export const CalendarContainer = styled.div`
  height: 100%;
  display: space-evenly;
  flex-direction: column;
  align-items: center;
`;