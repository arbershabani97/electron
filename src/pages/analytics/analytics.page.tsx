import React, { useState, useEffect } from 'react';
import UserName from "../../components/username/username.component";
import {
    AnalyticsPageContainer,
    AnalyticsMainContainer,
    AnalyticsSectionFirst,
    AnalyticsSectionSecond,
    AnalyticsFilterContainer,
    AnalyticsIndFilter,
    TextContainer,
    AnalyticsCountViewContainer,
    AnalyticsItemsContainer,
    AnalyticsCountHeaderContainer,
    AnalyticsItemContainer,
    AnalyticsReleasesText,
    AnalyticsReleasesContainer,
    AnalyticsCalanderContainer,
    ContentContainer,
    AnalyticsProductContainer,
    AnalyticsSitelistContainer,
    SitelistHeader,
    CalendarContainer,
    RetailCostText,
    RetailImageContainer,
    RetailItemCount,
    RetailItemText,
    Sitelist,
    SiteListTitle,
    SiteListText
} from './analytics.style';
import VersionNumber from '../../components/version-number/version-number.component';
import DropDown from '../../components/dropdown/dropdown.component';
import CountHeader from '../../components/analyticItem/countHeader.component';
import AnalyticItem from '../../components/analyticItem/analyticItem.component';
import SelectComponent from '../../components/select/select.component';
import ShopifyIcon from "../../assets/images/shopping.svg";
import "react-modern-calendar-datepicker/lib/DatePicker.css";
import Calendar from '../../components/calander/calander';
import Info from '../../assets/images/info.svg';
import Left from '../../assets/images/left.svg';


const filterData = ['Orders', "Declines", "Total"];
const countViewData = [
    // { type: 'icon', url: ShopifyIcon },
    // { type: 'label', value: 'orders', count: '139', color: '#ACECFD' },
    // { type: 'label', value: 'declines', count: "23", color: '#FDACAC' },
    // { type: 'list', value: 'Shopify', options: ["Shopify", "Test-1"], count: '32424' }]
    // { type: 'icon', url: ShopifyIcon },
    { type: 'list', value: 'orders', count: '139', color: '#ACECFD' },
    { type: 'list', value: 'declines', count: "23", color: '#FDACAC' },
    { type: 'list', value: 'Total', count: '32424', color:'#C190FF' }]
const select1 = [
    { key: "Shopify", value: "Shopify" }, { key: "Supreme", value: "Supreme" },
    { key: "Footsites", value: "Footsites" }, { key: "Demandware", value: "Demandware" },
    { key: "Mesh", value: "Mesh" }, { key: "Show All", value: "Show All" },

]

function AnalyticsPage(props: any) {

    const [selectedDay, setSelectedDay] = useState(null);

    const setSelectedDays = (event: any) => {
        console.log(event)
    }
    return (
        <AnalyticsPageContainer>
            <UserName />
            <AnalyticsMainContainer>
                <AnalyticsSectionFirst>

                    <AnalyticsFilterContainer>
                        {filterData.map((indFilter, i) =>
                            <AnalyticsIndFilter>
                                <TextContainer key={i}>{indFilter}</TextContainer>
                            </AnalyticsIndFilter>
                        )}
                    </AnalyticsFilterContainer>

                    <AnalyticsCountViewContainer>
                        <AnalyticsCountHeaderContainer>
                            {countViewData.map((indCHeader, i) =>
                                <CountHeader indHeader={indCHeader}>
                                </CountHeader>
                            )}
                        </AnalyticsCountHeaderContainer>
                        <AnalyticsItemsContainer>
                            <div style={{ width: "10%" }}>
                                <img src={ShopifyIcon} style={{ width: 25 }}></img>
                            </div>
                            <div style={{ fontSize: "30px"}}>
                                Elite AIO
                            </div>
                            <div 
                            style={{
                                display: "flex",
                                height: "40px",
                                justifyContent: "center",
                                alignItems: "center",
                            }}
                            >
                                <SelectComponent placeholder={"select option"} options={select1}></SelectComponent>
                            </div>
                            
                        </AnalyticsItemsContainer>
                    </AnalyticsCountViewContainer>
                    
                    <AnalyticsItemContainer>
                        <AnalyticItem></AnalyticItem>

                    </AnalyticsItemContainer>

                </AnalyticsSectionFirst>

                <AnalyticsSectionSecond>
                    <AnalyticsReleasesText>Releases</AnalyticsReleasesText>
                    <AnalyticsReleasesContainer>

                        <AnalyticsCalanderContainer>
                            <Calendar />
                        </AnalyticsCalanderContainer>

                        <AnalyticsProductContainer>
                            <ContentContainer>
                                <RetailItemCount>6/11</RetailItemCount>
                                <RetailItemText>Space Hippie 01 'This Is Trash'</RetailItemText>
                                <RetailImageContainer>
                                    <img src={Left} className="App-logo" alt="icon" width="15" height="15" style={{ marginRight: 10 }} />
                                    <img src={"https://toppng.com/uploads/preview/nike-zoom-winflo-3-831561-001-mens-running-shoes-11550187236tiyyje6l87.png"} className="App-logo" alt="icon" width="100" height="100" style={{ marginRight: 10 }} />
                                    <img src={Left} className="App-logo" alt="icon" width="15" height="15" style={{ marginRight: 10,transform: "rotate(180deg)" }} />
                                </RetailImageContainer>
                                <RetailCostText>
                                    <img src={Info} className="App-logo" alt="icon" width="15" height="15" style={{ marginRight: 10 }} />
                                    Retail $130 / £115
                                </RetailCostText>

                            </ContentContainer>
                        </AnalyticsProductContainer>

                        <AnalyticsSitelistContainer>
                            <SitelistHeader>Sitelist</SitelistHeader>
                            <Sitelist>
                                <SiteListTitle>Adidas</SiteListTitle>
                                <SiteListText>https://www.adidas.ca/en/nmd_r1-shoes/FV8727.html</SiteListText>
                            </Sitelist>
                            <Sitelist>
                                <SiteListTitle>DSML</SiteListTitle>
                                <SiteListText>https://shop.doverstreetmarket.com/us/what-s-new/stussy-stussy-check</SiteListText>
                            </Sitelist>
                            <Sitelist>
                                <SiteListTitle>Nike</SiteListTitle>
                                <SiteListText>https://www.nike.com/t/air-force-1-07-mens-shoe-kpQKZt/CJ1379-100</SiteListText>
                            </Sitelist>
                            <Sitelist>
                                <SiteListTitle>Kith</SiteListTitle>
                                <SiteListText>https://kith.com/collections/kith-kids/products/khk3030-100</SiteListText>
                            </Sitelist>
                        </AnalyticsSitelistContainer>
                    </AnalyticsReleasesContainer>
                </AnalyticsSectionSecond>
            </AnalyticsMainContainer>
            <VersionNumber />
        </AnalyticsPageContainer>
    );
};

export default AnalyticsPage;
