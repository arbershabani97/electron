// import styled from 'styled-components';

// export const SettingsPageContainer = styled.div`
//   display: flex;
//   min-height: calc(100vh - 165px);

// `;

// export const SettingsItemsContainer = styled.div`
//   height: 82vh;
//   width: 90vw;
//   // flex-grow:5;
//   border: 1px solid #acecfd;

//   display: flex;
//   box-shadow: 0px 2px 12px rgba(172, 236, 253, 0.5), 0px -2px 12px rgba(172, 236, 253, 0.5);
// `;

// export const SettingHalfCtn=styled.div`
//   width:50%;
//   height:100%;
// `;

// export const SettingListCtn=styled.div`
//   // background:blue;
//   width:90%;
//   height:87%;
//   margin-top:-10px;
//   margin-left:-10px;
//   color:white;
// `;

// export const SettingDataCtn=styled.div`
//   width:100%;
//   height:80px;
//   display:flex;
//   justify-content:space-between;
// `;

// export const SettingDataItem=styled.div`
//   width:48%;
//   height: 95%;
//   background: #0c0c0c;
//   box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
//   border-radius: 15px;
//   display:flex;
//   justify-content:space-evenly;
//   align-items:center;
// `;

// export const SettingDataListctn=styled.div`
//   width:100%;
//   height:80%;
//   border-radius: 5px;
//   background: #0c0c0c;
//   box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
// `

// export const SettingItemLabel=styled.div`
//   width:22%;
//   text-align:center;
//   font-size:13px;
//   white-space: nowrap;
//   overflow: hidden;
//   text-overflow: ellipsis;
// `;

// export const SettingItemCtn=styled.div`
//   width: 98%;
//   height:24px;
//   display:flex;
//   justify-content:space-evenly;
//   align-items:center;
//   margin:auto;
//   margin-top:10px;
// /* pkiouy7 */

// border-radius: 5px;
// background: #0c0c0c;
// box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
// `;


// export const SettingProfileCtn=styled.div`
// height:100%;
// width:90%;
// margin:auto;
// padding-top:40px;
// padding-left:40px;
//   color:white;
// `;

// export const SettingProfInfoCtn=styled.div`
//   width:100%;
//   height:30%;
//   box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
//   border-radius: 10px;
//   display:flex;
//   flex-direction:column;
// `;
// export const SettingInputsCtn=styled.div`
//   width:100%;
//   margin-top:15px;
//   height:35%;
//   color:white;
//   box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
//   border-radius: 10px;
//   display:flex;
//   flex-direction:column;
// `;

// export const SettingApiCtn=styled.div`
//   width:100%;
//   margin-top:15px;
//   padding-left:10px;
//   padding-right:10px;
//   height:10%;
//   color:white;
//   box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
//   border-radius: 10px;
//   display:flex;
//   flex-direction:column;
// `;
// export const HorizontalHalf=styled.div`
//   height:50%;
//   width:100%;
//   display:flex;
// `;

// export const SettingUserImage=styled.div`
// width: 90%;
// height: 80%;
// display: flex;
// justify-content: space-evenly;
// align-items: center;
// background: #0c0c0c;
// border-radius: 15px;
// box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
// `;

// export const SettingBtnCtn=styled.div`
// background: #0c0c0c;
// color:white;
// width:90%;
// height:40%;
// border-radius: 15px;
// box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
// display: flex;
// justify-content: space-evenly;
// align-items: center;

// a{
//   color:white;
//   text-align:center;
//   font-size:15px;
// }
// `;
import styled from 'styled-components';

export const SettingsPageContainer = styled.div`
  display: flex;
  min-height: calc(100vh - 165px);

`;

export const SettingsItemsContainer = styled.div`
  height: 82vh;
  width: 90vw;
  // flex-grow:5;
  border: 1px solid #acecfd;

  display: flex;
  box-shadow: 0px 2px 12px rgba(172, 236, 253, 0.5), 0px -2px 12px rgba(172, 236, 253, 0.5);
`;

export const SettingHalfCtn=styled.div`
  width:50%;
  height:100%;
`;

export const SettingListCtn=styled.div`
  // background:blue;
  width:90%;
  height:87%;
  margin-top:-10px;
  margin-left:16px;
  color:white;
`;

export const SettingDataCtn=styled.div`
  width:98%;
  height:75px;
  display:flex;
  justify-content:space-between;
`;

export const SettingDataItem=styled.div`
  width:48%;
  height: 99%;
  background: #0c0c0c;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 15px;
  display:flex;
  justify-content:space-evenly;
  align-items:center;
  font-size: 17px;
`;

export const SettingDataListctn=styled.div`
  width:98%;
  height:80%;
  margin-top: -10px;
  border-radius: 5px;
  background: #0c0c0c;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
`

export const SettingItemLabel=styled.div`
  width:22%;
  text-align:center;
  font-size:12px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const SettingItemCtn=styled.div`
  width: 99%;
  height:37px;
  display:flex;
  justify-content:space-evenly;
  align-items:center;
  margin:auto;
  margin-top:6px;
/* pkiouy7 */

border-radius: 5px;
background: #0c0c0c;
box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
`;


export const SettingProfileCtn=styled.div`
height:100%;
width:85%;
margin:auto;
padding-top:40px;
padding-left:0px;
  color:white;
`;

export const SettingProfInfoCtn=styled.div`
  width:105%;
  height:31%;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 10px;
  display:flex;
  flex-direction:column;
`;
export const SettingInputsCtn=styled.div`
  width:105%;
  margin-top:15px;
  height:35%;
  color:white;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 10px;
  display:flex;
  flex-direction:column;
`;

export const SettingApiCtn=styled.div`
  width:105%;
  margin-top:15px;
  padding-left:10px;
  padding-right:10px;
  height:10%;
  color:white;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 10px;
  display:flex;
  flex-direction:column;
`;
export const HorizontalHalf=styled.div`
  height:50%;
  width:100%;
  display:flex;
`;

export const SettingUserImage=styled.div`
width: 90%;
height: 80%;
display: flex;
justify-content: space-evenly;
align-items: center;
background: #0c0c0c;
border-radius: 15px;
box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
`;

export const SettingBtnCtn=styled.div`
background: #0c0c0c;
color:white;
width:88%;
height:40%;
border-radius: 12px;
margin-top: 10px;
box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
display: flex;
justify-content: space-evenly;
align-items: center;

a{
  color:white;
  text-align:center;
  font-size:14px;
}
`;

export const InputContainer=styled.div`
    display: flex;
    justify-content: center;
    width: 45%;
    text-align:center;
    color: #BCBCBC;
    font-size:14px;
    // padding: 7px 0px 10px 0px;
`;

export const InputColumnContainer=styled.div`
    display:flex;
    color: #BCBCBC;
    // justify-content:center;
    // align-items:center;
    flex-direction:column;
    // background:red;
    width:95%;
    // padding: 0px 0px 0 10px;
    font-size: 14px;
    
`;

export const SettingLabel = styled.label`
margin-bottom :0px;
`;

export const PolicyContainer = styled.div`
    color: #999999;
    display: flex;
    justify-content: unset;
    transform: rotate(-90deg);
    position: absolute;
    right: -6.2em;
    bottom: 13em;
`
export const PolicyLink = styled.a`
  color: #999999;
  margin-right: 6px;
  font-size: 12px;
  font-family: Montserrat;
  text-shadow: 4px 4px 9px #0D0D0D, -3px -1px 4px rgba(66, 66, 66, 0.41);
`