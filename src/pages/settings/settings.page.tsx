import React from 'react';
import {
  SettingsPageContainer,
  SettingsItemsContainer,
  SettingHalfCtn,
  SettingListCtn,
  SettingDataCtn,
  SettingDataItem,
  SettingDataListctn,
  SettingItemLabel,
  SettingItemCtn,
  SettingProfileCtn,
  SettingProfInfoCtn,
  HorizontalHalf,
  SettingUserImage,
  SettingBtnCtn,
  SettingInputsCtn,
  SettingApiCtn,
  InputContainer,
  InputColumnContainer, SettingLabel, PolicyContainer, PolicyLink
} from './settings.styles';
import VersionNumber from '../../components/version-number/version-number.component';
import UserName from '../../components/username/username.component';
import { TaskStatText } from '../tasks/tasks.styles';
import addTask from '../../assets/images/addTask.svg';
import {
  NewTaskFormInputRowContainer,
  // InputContainer,
  // InputColumnContainer,
  NewTaskBtn,
} from '../../components/new-task/newTask.styles';
import Input from '../../components/input/input.component';
import saveProxy from '../../assets/images/save-proxy.svg';
import van from '../../assets/images/van.svg';
import check from '../../assets/images/check.svg';
import cookie from '../../assets/images/cookie.svg';
import PopupContainer from '../../components/common/popup.container';
import ShippingRateList from '../../components/shippingRate/shippingRate.component';
import GenerateCookies from '../../components/generate-cookies/generate-cookies.component';
import QuickTask from '../../components/quick-task/quick-task.component';
import CreateTask from '../../components/create-task/create-task.component';
import { connect } from 'react-redux';
import { ApplicationState } from '../../redux';
import { IAccount as Account } from '../../redux/accounts/types';
import { Top } from '../../global.styles';
import AddEditAccount from '../../components/add-edit-account-modal/addEditAccount.component';
import { addAccount, fetchAccount } from '../../redux/accounts/actions';
import { updateError, updateMonitor, updateWebhook } from '../../redux/settings/actions';
import * as uuid from "uuid";

const SettingItem = ({ account }: any) => {
  return (
    <SettingItemCtn>
      <SettingItemLabel>{account?.store?.longCode}</SettingItemLabel>
      <SettingItemLabel>{account?.email}</SettingItemLabel>
      <SettingItemLabel>{account?.password}</SettingItemLabel>
      <SettingItemLabel>{account?.name}</SettingItemLabel>
    </SettingItemCtn>
  );
};

class SettingsPage extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      isShippingModal: false,
      isQuickTask: false,
      isGenCookies: false,
      isCreateTask: false,
      isAddEditModalOpen: false,
      error: this.props.error,
      monitor: this.props.monitor,
      webhook: this.props.webhook,
    };
    this.openShippingModal = this.openShippingModal.bind(this);
    this.openCookiesModal = this.openCookiesModal.bind(this);
    this.openCheckModal = this.openCheckModal.bind(this);
    this.openQuickTaskModal = this.openQuickTaskModal.bind(this);
    this.dissmissable = this.dissmissable.bind(this);
    this.toggleAddEditModal = this.toggleAddEditModal.bind(this);
    this.createNewAccount = this.createNewAccount.bind(this);
    this.updateSettings = this.updateSettings.bind(this);
  }

  public componentDidMount() {
    const { accounts, fetchAccount } = this.props;

    if (!accounts?.length) {
      fetchAccount();
    }
  }

  openQuickTaskModal = (event: any) => {
    let { isQuickTask }: any = this.state;
    isQuickTask = !isQuickTask;
    this.setState({ isQuickTask });
  };

  closeQuickTask = (event: any) => {
    let { isQuickTask }: any = this.state;
    isQuickTask = false;
    this.setState({ isQuickTask });
  };

  openCookiesModal = (event: any) => {
    let { isGenCookies }: any = this.state;
    isGenCookies = !isGenCookies;
    this.setState({ isGenCookies });
  };

  closeCookies = (event: any) => {
    let { isGenCookies }: any = this.state;
    isGenCookies = false;
    this.setState({ isGenCookies });
  };

  openCheckModal = (event: any) => {
    let { isCreateTask }: any = this.state;
    isCreateTask = !isCreateTask;
    this.setState({ isCreateTask });
  };

  closeCheck = (event: any) => {
    let { isCreateTask }: any = this.state;
    isCreateTask = false;
    this.setState({ isCreateTask });
  };

  openShippingModal = (event: any) => {
    let { isShippingModal }: any = this.state;
    isShippingModal = !isShippingModal;
    this.setState({ isShippingModal });
  };

  dissmissable = (event: any) => {
    let { isShippingModal }: any = this.state;
    isShippingModal = false;
    this.setState({ isShippingModal });
  };

  toggleAddEditModal = (event: any) => {
    event.preventDefault();
    this.setState((prevState: any) => ({
      isAddEditModalOpen: !prevState.isAddEditModalOpen,
    }));
  };

  createNewAccount = (account: any) => {
    account.id = uuid.v4();
    this.props.addAccount(account);
    this.setState({ isAddEditModalOpen: false });
  };

  updateSettings = () => {
    this.props.updateError(this.state.error);
    this.props.updateMonitor(this.state.monitor);
    this.props.updateWebhook(this.state.webhook);
  };

  handleChange = (event: any) => {
    this.setState({ [event.target.id]: event.target.value });
  };

  verifyWebhook = () => {
    const request = new XMLHttpRequest();
    request.open('POST', this.state.webhook);
    request.setRequestHeader('Content-type', 'application/json');
    const params = {
      username: 'EliteAIO',
      avatar_url: '',
      content: 'Notify: Validation of Hook',
    };
    request.send(JSON.stringify(params));
  };

  // openDashboardLink = () => {
  //   const url = 'https://github.com'
  //   require('electron').shell.openExternal(url)
  // }

  render() {
    const { isShippingModal, isQuickTask, isGenCookies, isCreateTask }: any = this.state;
    let children;
    let isAction;
    if (isShippingModal) {
      children = <ShippingRateList />;
    }
    if (isGenCookies) {
      children = <GenerateCookies />;
    }
    if (isCreateTask) {
      children = <CreateTask />;
    }
    if (isQuickTask) {
      children = <QuickTask />;
    }
    return (
      <SettingsPageContainer>
        <UserName />
        {isShippingModal && (
          <PopupContainer visible={isShippingModal} dismiss={this.dissmissable} children={children} action={null} />
        )}
        {isGenCookies && (
          <PopupContainer visible={isGenCookies} dismiss={this.closeCookies} children={children} action={true} />
        )}
        {isCreateTask && (
          <PopupContainer visible={isCreateTask} dismiss={this.closeCheck} children={children} action={true} />
        )}
        {isQuickTask && <PopupContainer visible={isQuickTask} dismiss={this.closeQuickTask} children={children} />}

        <SettingsItemsContainer>
          <SettingHalfCtn>
            <TaskStatText>
              <div className="">Create</div>
              <img
                onClick={this.toggleAddEditModal}
                src={addTask}
                className="App-logo"
                alt="icon"
                width="50"
                height="50"
              />
            </TaskStatText>
            <SettingListCtn>
              <SettingDataCtn>
                <SettingDataItem>
                  <div className="">Total</div>
                  <div
                    style={{
                      color: '#ACECFD',
                      textShadow: '4px 4px 9px #0D0D0D, -3px -1px 4px rgba(66, 66, 66, 0.41)',
                      fontSize: '20px',
                    }}
                    className=""
                  >
                    {this.props.total}
                  </div>
                </SettingDataItem>
                {/* <SettingDataItem>
                  <div className="">Generated</div>
                  <div
                    style={{
                      color: '#ACECFD',
                      textShadow: '4px 4px 9px #0D0D0D, -3px -1px 4px rgba(66, 66, 66, 0.41)',
                      fontSize: '20px',
                    }}
                    className=""
                  >
                    37
                  </div>
                </SettingDataItem> */}
              </SettingDataCtn>
              <SettingDataListctn>
                <div
                  style={{
                    position: 'sticky',
                    width: '100%',
                    background: '#0c0c0c',
                    marginTop: '25px',
                    padding: '10px 0px',
                    borderTopLeftRadius: 5,
                  }}
                  className=""
                >
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-evenly',
                      paddingBottom: '0px',
                    }}
                    className=""
                  >
                    <SettingItemLabel>Store</SettingItemLabel>
                    <SettingItemLabel>Email</SettingItemLabel>
                    <SettingItemLabel>Password</SettingItemLabel>
                    <SettingItemLabel>Account Name</SettingItemLabel>
                  </div>
                </div>
                <div
                  style={{
                    paddingTop: '0px',
                    overflow: 'scroll',
                    height: '92%',
                  }}
                  className=""
                >
                  {this.props.accounts.map((data: Account) => {
                    return (
                      <SettingItem
                        key={data?.id}
                        account={data}
                      />
                    );
                  })}
                </div>
              </SettingDataListctn>
            </SettingListCtn>
          </SettingHalfCtn>
          <SettingHalfCtn>
            <SettingProfileCtn>
              <div style={{ fontSize: '20px', paddingBottom: '10px' }} className="">
                Settings
              </div>
              <div
                style={{
                  width: '95%',
                  height: '100%',
                }}
                className=""
              >
                <SettingProfInfoCtn>
                  <HorizontalHalf>
                    <div
                      style={{
                        margin: 'auto',
                        height: '100%',
                        width: '60%',
                        display: 'flex',
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                      }}
                      className=""
                    >
                      <SettingUserImage>
                        <img
                          style={{
                            borderRadius: '50%',
                            objectFit: 'cover',
                          }}
                          src="https://images.complex.com/complex/images/c_fill,dpr_auto,f_auto,q_90,w_1400/fl_lossy,pg_1/wiblzfaqozaor1rlmsrg/gucci-glo"
                          width="40px"
                          height="40px"
                          alt="profile"
                        />
                        <div className="">Orion#1997</div>
                      </SettingUserImage>
                    </div>
                    <div
                      style={{
                        width: '40%',
                        height: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      className=""
                    >
                      <div
                        style={{
                          color: '#ACECFD',
                          fontSize: '15px',
                        }}
                        className=""
                      >
                        Beta License
                      </div>
                      <SettingBtnCtn>
                        <a
                        // onClick={this.openDashboardLink}
                        >
                          Dashboard
                        </a>
                      </SettingBtnCtn>
                    </div>
                  </HorizontalHalf>
                  <HorizontalHalf>
                    <SettingHalfCtn>
                      <div
                        style={{
                          width: '100%',
                          height: '100%',
                          textAlign: 'center',
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        className=""
                      >
                        <div
                          style={{
                            fontSize: '13px',
                            padding: '5px',
                          }}
                          className=""
                        >
                          Expiration
                        </div>
                        <div className="">July 24th, 2020</div>
                      </div>
                    </SettingHalfCtn>
                    <SettingHalfCtn>
                      <div
                        style={{
                          width: '100%',
                          height: '100%',
                          textAlign: 'center',
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                          color: '#FDACAC',
                        }}
                        className=""
                      >
                        3 Days Remaining
                      </div>
                    </SettingHalfCtn>
                  </HorizontalHalf>
                </SettingProfInfoCtn>
                <SettingInputsCtn>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width: '95%',
                      margin: '0 auto',
                    }}
                  >
                    <InputContainer>
                      <SettingLabel htmlFor="monitorMin">Monitor</SettingLabel>
                    </InputContainer>
                    <InputContainer>
                      <SettingLabel htmlFor="Error">Error</SettingLabel>
                    </InputContainer>
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      width: '95%',
                      margin: '0 auto',
                    }}
                  >
                    <InputContainer>
                      {/* <label htmlFor="monitorMin">Monitor</label> */}
                      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                        <Input
                          grow={false}
                          handleChange={this.handleChange}
                          type="text"
                          id="monitor"
                          value={this.state.monitor}
                          placeholder=""
                        />
                      </div>
                    </InputContainer>
                    <InputContainer>
                      {/* <label htmlFor="Error">Error</label> */}
                      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                        <Input
                          grow={false}
                          handleChange={this.handleChange}
                          type="text"
                          id="error"
                          value={this.state.error}
                          placeholder=""
                        />
                      </div>
                    </InputContainer>
                  </div>
                  <NewTaskFormInputRowContainer>
                    <InputColumnContainer>
                      <SettingLabel style={{ textAlign: 'center' }} htmlFor="">
                        Webhook
                      </SettingLabel>
                      <Input
                        grow={true}
                        handleChange={this.handleChange}
                        type="keywords"
                        id="webhook"
                        value={this.state.webhook}
                        placeholder=""
                      />
                    </InputColumnContainer>
                  </NewTaskFormInputRowContainer>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-end',
                      // marginRight: '50px',
                      width: '90%',
                      margin: '0 auto',
                    }}
                    className=""
                  >
                    <div
                      style={{
                        fontSize: '12px',
                        color: '#C190FF',
                        textAlign: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                        borderRadius: '20px',
                        alignItems: 'center',
                        background: '#0c0c0c',
                        boxShadow: '-1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black',
                        width: '50px',
                        height: '20px',
                      }}
                      className=""
                      onClick={this.verifyWebhook}
                    >
                      Test
                    </div>
                  </div>
                </SettingInputsCtn>
                <SettingApiCtn>
                  <Input
                    grow={true}
                    handleChange={() => { }}
                    type="keywords"
                    id="proxy"
                    value=""
                    placeholder="Access Token Api Key"
                  />
                </SettingApiCtn>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    marginTop: '10px',
                  }}
                  className=""
                >
                  <div
                    style={{
                      display: 'flex',
                    }}
                    className=""
                  >
                    <NewTaskBtn
                      style={{ height: '30px', width: '30px', margin: '5px' }}
                    >
                      <img
                        src={saveProxy}
                        className="App-logo"
                        alt="icon"
                        width="15"
                        height="15"
                        onClick={this.updateSettings}
                      />
                    </NewTaskBtn>
                    <NewTaskBtn
                      style={{ height: '30px', width: '30px', margin: '5px' }}>
                      <img
                        src={van}
                        className="App-logo"
                        alt="icon"
                        width="20"
                        height="20"
                        onClick={this.openShippingModal}
                      />
                    </NewTaskBtn>
                    <NewTaskBtn
                      style={{ height: '30px', width: '30px', margin: '5px' }}>
                      <img
                        src={check}
                        className="App-logo"
                        alt="icon"
                        width="20"
                        height="20"
                        onClick={this.openCheckModal}
                      />
                    </NewTaskBtn>
                    <NewTaskBtn
                      style={{ height: '30px', width: '30px', margin: '5px' }}>
                      <img
                        src={cookie}
                        className="App-logo"
                        alt="icon"
                        width="20"
                        height="20"
                        onClick={this.openCookiesModal}
                      />
                    </NewTaskBtn>
                  </div>
                </div>
              </div>
            </SettingProfileCtn>
            <PolicyContainer>
              <PolicyLink >Terms of Service</PolicyLink>
              <PolicyLink >Privacy Policy</PolicyLink>
              <PolicyLink >Check for Updates</PolicyLink>
            </PolicyContainer>
          </SettingHalfCtn>
          {/* <div style={{
              color:'white',
              background:'red',
              height:'20px',
              transform:'rotate(-90deg)'
            }} className="">
              <div className="">Terms of Service</div>
      
            </div> */}
        </SettingsItemsContainer>
        {this.state.isAddEditModalOpen && (
          <Top>
            <AddEditAccount dismiss={this.toggleAddEditModal} onAction={this.createNewAccount} />
          </Top>
        )}
        <VersionNumber />
      </SettingsPageContainer>
    );
  }
}

const mapStateToProps = ({ accounts, settings }: ApplicationState) => ({
  accounts: accounts.accounts,
  total: accounts.total,
  error: settings.error,
  monitor: settings.monitor,
  webhook: settings.webhook,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    addAccount: (account: any) => dispatch(addAccount(account)),
    fetchAccount: () => dispatch(fetchAccount()),
    updateError: (error: any) => dispatch(updateError(error)),
    updateMonitor: (monitor: any) => dispatch(updateMonitor(monitor)),
    updateWebhook: (webhook: any) => dispatch(updateWebhook(webhook)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
