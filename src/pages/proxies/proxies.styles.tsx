import styled from 'styled-components';

export const ProxiesContainer = styled.div`
  color: white;
  width: 100%;
  min-height: calc(100vh - 165px);
  display: flex;
  flex-direction: row;
`;

export const ProxiesMainBox = styled.div`
  border: 1px solid #acecfd;
  height: 82vh;
  width: 90vw;
  padding: 25px;
  padding-bottom: 0;
  display: flex;
  flex-direction: column;
  box-shadow: 0px 2px 12px rgba(172, 236, 253, 0.5), 0px -2px 12px rgba(172, 236, 253, 0.5);
  overflow-y: auto;
`;

export const ProxiesMainRow = styled.div`
  display: flex;
`;

export const ProxiesStateRow = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  position: absolute;
  justify-content: space-between;
  top: -60px;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
`;

export const NumberCardContainer = styled.div`
  width: 45%;
  display: flex;
  justify-content: space-between;
  padding: 15px;
  background: #0c0c0c;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  align-self: flex-start;
`;

export const NumberText = styled.span`
  font-size: 20px;
  color: white;
`;

export const NumberContent = styled.span`
  font-size: 20px;
`;

export const ProxiesTextArea = styled.textarea`
  resize: none;
  border: none;
  outline: none;
  background: #0c0c0c;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  color: white;
  margin-top: 10px;
  width: 27vw;
  height: 59vh;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
`;

export const ActionButton = styled.div`
  background: #0c0c0c;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  cursor: pointer;
  padding: 10px;
  margin-right: 10px;
  height: 33px;

  img {
    height: 12px;
    width: 15px;
  }
`;

export const ProxiesInput = styled.input`
  outline: none;
  border: none;
  background: #0c0c0c;
  color: white;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  text-align: center;
  font-size: 12px;
  margin-right: 15px;
  width: 130px;
`;

export const ProxiesList = styled.div`
box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  overflow: auto;
  height: 100%;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
`;

export const ProxyItem = styled.span`
  color: #848383;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  background: #0c0c0c;
  text-align: center;
  padding: 11px 0;
  margin-bottom: 10px;
  font-size: 12.5px;
`;

export const ProxyURLInput = styled.input`
  outline: none;
  border: none;
  background: none;
  font-size: 14px;
  color: white;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  padding: 10px 5px;
  box-sizing: border-box;
  width: 33vw;
  margin-top: 10px;
  margin-bottom: 15px;
`;

export const ProxyTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 3px black;
  border-radius: 5px;
  padding: 10px 5px;
  overflow: auto;
  //background: white;
`;

export const ProxyTable = styled.table`
  width: 100%;

  th {
    color: #5d5c5c;
    font-size: 12px;
    text-align: start;
    padding-bottom: 10px;
    position: sticky;
    top: 0;
    background: #0c0c0c;
    
  }

  td {
    font-size: 12px;
    color: white;
    padding-bottom: 7px;
  }
`;
