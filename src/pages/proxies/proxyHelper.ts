//TO_DO need to move separate file  helper methods and constants

export const Separater = { Newline: '\n', };

export const errorStrings = {
    groupList: "GroupList is required",
    groupName: "GroupName is required"
}

export const getStringifiedList = (list: Array<[]>, key: any, separator: any) => {
    let stringFormatValue = '';
    if (list) {
        list.forEach((item: any) => {
            if (item[key]) {
                stringFormatValue += item[key] + separator;
            }
        });
    }
    return stringFormatValue;
}

export const uid = (number?: any) => Date.now().toString(number ? number : 36) + Math.random().toString(number ? number : 36).substr(2);

