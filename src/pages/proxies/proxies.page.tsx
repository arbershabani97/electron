import React from 'react';
import {
  Column,
  NumberCardContainer,
  NumberContent,
  NumberText,
  ProxiesContainer,
  ProxiesMainBox,
  ProxiesMainRow,
  ProxiesStateRow,
  ProxiesTextArea,
  ActionButton,
  ProxiesInput,
  ProxiesList,
  ProxyItem,
  ProxyURLInput,
  ProxyTable,
  ProxyTableWrapper,
} from './proxies.styles';
import UserName from '../../components/username/username.component';
import VersionNumber from '../../components/version-number/version-number.component';
import { ProfilesTopRow } from '../profiles/profiles.styles';
import playIcon from '../../assets/images/play-icon.svg';
import saveIcon from '../../assets/images/save-proxy.svg';
import deleteIcon from '../../assets/images/delete-proxy.svg';
import { connect } from 'react-redux';
import { addProxy, updateProxy, deleteProxy } from '../../redux/proxies/actions';
import { ApplicationState } from '../../redux';
import { uid, getStringifiedList, Separater, errorStrings } from './proxyHelper';
import { fetchRequest } from '../../redux/proxies/actions';

const NumberCard = (props: { name: string; color: string; amount: number }) => {
  return (
    <NumberCardContainer>
      <NumberText>{props.name}</NumberText>
      <NumberContent style={{ color: props.color }}>{props.amount}</NumberContent>
    </NumberCardContainer>
  );
};



class ProxiesPage extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      groupId: '',
      groupList: '',
      groupName: '',
      total: 0,
    }
  }

  componentDidMount() {
    this.props.fetchRequest()
  }

  handleChange = (event: any) => {
    this.setState({ [event.target.id]: event.target.value });
    if (event.target.id === "groupList") {
      let str = event.target.value.split(/\r\n|\r|\n/).length - 1
      this.setState({
        total: str
      })
    }
  };

  addProxy = (groupLists: any, groupName: any) => {
    let _groupList = groupLists.split(/\r\n|\n/g);
    const _groupName = groupName;
    let groupList = new Array();
    for (let item of _groupList) {
      item = item.replace(/^\s+|\s+$/g, '');
      if (item !== '') {
        groupList.push({
          'proxyUrl': item
        });
      }
    }
    if (groupList && groupList.length) {
      if (!this.state.groupId) {
        this.setState({ groupId: uid(), groupList: groupList, groupName: _groupName }, () => {
          this.props.addProxy(this.state)
          this.resetForm();
        });
      } else {
        this.setState({ groupId: this.state.groupId, groupList: groupList, groupName: _groupName }, () => {
          this.props.updateProxy(this.state)
          this.resetForm();
        });
      }
    }

  }

  resetForm = () => {
    this.setState({
      groupId: '',
      groupList: '',
      groupName: '',
      total: 0
    });
  }


  updateProxyForm = (proxy: any) => {
    this.setState({
      groupId: proxy.groupId,
      groupName: proxy.groupName
    });
    let list = getStringifiedList(proxy.groupList, 'proxyUrl', Separater.Newline);
    this.setState({ groupList: list, total: proxy.groupList.length })
  }

  deleteProxy = () => {
    this.props.deleteProxy(this.state);
    this.resetForm();
  }

  handleSubmit = () => {
    let errors = { groupList: '', groupName: '' }
    if (!this.state.groupList) {
      errors.groupList = errorStrings.groupList;
    }
    if (!this.state.groupName) {
      errors.groupName = errorStrings.groupName;
    }
    if (errors.groupList || errors.groupName) {
      return;
    } else {
      this.addProxy(this.state.groupList, this.state.groupName)
    }
  }



  render() {
    const activeProxiesExample = [
      {
        ip: '192.168.0.1',
        port: '28',
        user: 'delmo',
        password: 'delmo1234',
        status: 'Loading site',
        speed: '24ms',
      },
      {
        ip: '192.168.0.1',
        port: '761',
        user: 'delmo',
        password: 'delmo1234',
        status: 'Error',
        speed: '0ms',
      },
      {
        ip: '192.168.0.1',
        port: '3000',
        user: 'delmo',
        password: 'delmo1234',
        status: 'Loading site',
        speed: '123ms',
      },
    ];

    return (
      <ProxiesContainer>
        <UserName />
        <ProxiesMainBox>
          <ProfilesTopRow>
            <span>Proxies</span>
            <img src={playIcon} className={'icon'} style={{ boxShadow: 'none', borderRadius: '0' }} alt="Test" />
          </ProfilesTopRow>
          <ProxiesMainRow>
            <Column style={{ marginRight: '2vw' }}>
              <NumberCard name={'Total'} color={'#ACECFD'} amount={this.state.total} />
              <ProxiesTextArea placeholder={'Enter proxies here'} id="groupList" onChange={this.handleChange} value={this.state.groupList} />
            </Column>
            <Column style={{ paddingRight: '2vw' }}>
              <ProxiesMainRow style={{ marginTop: '10px', marginBottom: '20px' }}>
                <ActionButton onClick={this.handleSubmit}>
                  <img src={saveIcon} alt="Save"  />
                </ActionButton>
                <ProxiesInput type={'text'} placeholder={'Profile Name'} id="groupName" onChange={this.handleChange} value={this.state.groupName} />
                <ActionButton onClick={() => this.deleteProxy()} style={{ marginRight: '0' }}>
                  <img src={deleteIcon} alt="Delete" />
                </ActionButton>
              </ProxiesMainRow>
              <ProxiesList>
                {this.props.proxies.map((proxy: any) => {
                  return <ProxyItem onClick={() => this.updateProxyForm(proxy)} >{proxy.groupName}</ProxyItem>;
                })}
              </ProxiesList>
            </Column>
            <Column>
              <ProxiesStateRow style={{ justifyContent: 'space-between' }}>
                <NumberCard name={'Success'} color={'#ACFDB4'} amount={232} />
                <NumberCard name={'Failed'} color={'#FF6262'} amount={5} />
              </ProxiesStateRow>
              <ProxyURLInput type={'text'} placeholder={'Enter a site URL to test'} />
              <ProxyTableWrapper>
                <ProxyTable cellSpacing={'0'}>
                  <thead>
                    <tr>
                      <th>ip</th>
                      <th>port</th>
                      <th>user</th>
                      <th>pass</th>
                      <th>status</th>
                      <th>speed</th>
                    </tr>
                  </thead>
                  <tbody>
                    {activeProxiesExample.map((proxy) => {
                      return (
                        <tr>
                          <td>{proxy.ip}</td>
                          <td>{proxy.port}</td>
                          <td>{proxy.user}</td>
                          <td>{proxy.password}</td>
                          <td>{proxy.status}</td>
                          <td>{proxy.speed}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </ProxyTable>
              </ProxyTableWrapper>
            </Column>
          </ProxiesMainRow>
        </ProxiesMainBox>
        <VersionNumber />
      </ProxiesContainer>
    )
  };
};


const mapStateToProps = ({ proxies }: ApplicationState) => ({
  proxies: proxies.proxies
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    addProxy: (proxy: any) => dispatch(addProxy(proxy)),
    updateProxy: (proxy: any) => dispatch(updateProxy(proxy)),
    deleteProxy: (proxy: any) => dispatch(deleteProxy(proxy)),
    fetchRequest :() =>dispatch(fetchRequest())

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProxiesPage);