import styled from 'styled-components';

export const HomePageContainer = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 10px 50px rgba(172, 236, 253, 0.55), 0px -10px 50px rgba(172, 236, 253, 0.5);
`;
