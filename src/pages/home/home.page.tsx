import React, { Component } from 'react';
import { HomePageContainer } from './home.style';
import Header from '../../components/header/header.component';
import TaskPage from '../tasks/tasks.page';
import ProfilesPage from '../profiles/profiles.page';
import AnalyticsPage from '../analytics/analytics.page';

import '../../assets/normalize.css';

import { Route, Switch } from 'react-router-dom';
import SettingsPage from '../settings/settings.page';
import ProxiesPage from '../proxies/proxies.page';

const HomePage: React.SFC = () => {
  return (
    <HomePageContainer>
      <Header />
      <Switch>
        <Route exact path="/" component={TaskPage} />
        <Route path="/profiles" component={ProfilesPage} />
        <Route path="/proxies" component={ProxiesPage} />
        <Route path="/analytics" component={AnalyticsPage} />
        <Route path="/settings" component={SettingsPage} />
      </Switch>
    </HomePageContainer>
  );
};

export default HomePage;
