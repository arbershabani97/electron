import { Proxies } from './proxies/types';
import { combineReducers } from 'redux';
import { all, fork } from 'redux-saga/effects';
import { connectRouter, RouterState } from 'connected-react-router';
import { History } from 'history';

import { upcomingReducer } from './upcoming/reducer';
import { UpcomingState } from './upcoming/types';
import { Accounts } from './accounts/types';
import { Settings } from './settings/types';
import { IProfile as Profile } from './profile/types';

import { tasksReducer } from './task/reducer';
import { TasksState } from './task/types';
import { accountReducer } from './accounts/reducers';
import { settingsReducer } from './settings/reducers';
import { proxyReducer } from './proxies/reducers';
import { profilessReducer } from './profile/reducer';
import { ProfilesState } from './profile/types';

// The top-level state object
export interface ApplicationState {
  releaseDates: UpcomingState;
  tasks: TasksState;
  accounts: Accounts;
  settings: Settings;
  proxies: Proxies;
  router: RouterState;
  profiles:ProfilesState
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
export const createRootReducer = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    releaseDates: upcomingReducer,
    tasks: tasksReducer,
    accounts: accountReducer,
    proxies: proxyReducer,
    profiles:profilessReducer,
    settings: settingsReducer,
  });

// Here we use `redux-saga` to trigger actions asynchronously. `redux-saga` uses something called a
// "generator function", which you can read about here:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*
