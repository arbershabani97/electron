import { AccountsActionTypes, IAccount } from './types';
const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

export const fetchAccount = () => (dispatch: any) => {
  dispatch({ type: AccountsActionTypes.FETCH_REQUEST });
  try {
    ipcRenderer.send('get:accounts', 'get account');
    ipcRenderer.on('accounts:data', (event: any, accounts: any) => {
      if (accounts == undefined) {
        dispatch({ type: AccountsActionTypes.FETCH_ERROR, payload: 'Error fetching accounts' });
        return;
      }
      dispatch({ type: AccountsActionTypes.FETCH_SUCCESS, payload: accounts });
    });
  } catch (error) {
    dispatch({ type: AccountsActionTypes.FETCH_ERROR });
  }
}

export const addAccount = (account: IAccount) => (dispatch: any) => {
  dispatch({ type: AccountsActionTypes.ADD_REQUEST });
  ipcRenderer.send('account:add', account);
  ipcRenderer.on('account:added', (event: any, account: any) => {
    dispatch({ type: AccountsActionTypes.ADD_SUCCESS, payload: account });
  })
};
