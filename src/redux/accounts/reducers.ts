import { Reducer } from 'redux';
import { IAccount, Accounts, AccountsActionTypes } from './types';

const initialState: Accounts = {
  accounts: [],
  total: 0,
  generated: 0,
  loading: false,
};

const reducer: Reducer<Accounts> = (state = initialState, action) => {
  switch (action.type) {
    case AccountsActionTypes.FETCH_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case AccountsActionTypes.FETCH_SUCCESS:
      return {
        ...state,
        accounts: action.payload,
        loading: false,
        total: action?.payload?.length
      };
    case AccountsActionTypes.FETCH_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload,
      };
    case AccountsActionTypes.ADD_REQUEST: {
      return {
        ...state,
        accounts: state.accounts.concat(action.payload),
        total: state.total + 1,
      };
    }
    case AccountsActionTypes.ADD_SUCCESS:
      // let index = state.accounts.findIndex((el: any) => el.id === action.payload.id);
      // if (index === -1) {
      //   return {
      //     ...state,
      //     loading: false,
      //     accounts: [...state.accounts, action.payload],
      //   };
      // }
      return {
        ...state,
        loading: false
      };

    case AccountsActionTypes.UPDATE_ACCOUNT: {
      return {
        ...state,
        accounts: state.accounts.map(
          (account: IAccount): IAccount => {
            if (account.id === action.payload.id) {
              return action.payload;
            } else {
              return account;
            }
          },
        ),
        total: state.total - 1,
      };
    }
    default: {
      return state;
    }
  }
};

export { reducer as accountReducer };
