export interface Accounts {
  accounts: IAccount[];
  total: number;
  generated: number;
  loading?: boolean;  
}

// User accounts on specific sites
export interface IAccount {
  id: string;
  store: string;
  email: string;
  password: string;
  name: string;
}

export enum AccountsActionTypes {
  FETCH_REQUEST = '@@accounts/FETCH_REQUEST',
  FETCH_SUCCESS = '@@accounts/FETCH_SUCCESS',
  FETCH_ERROR = '@@accounts/FETCH_ERROR',
  ADD_REQUEST = '@@account/ADD_REQUEST',
  ADD_SUCCESS = '@@account/ADD_SUCCESS',
  UPDATE_ACCOUNT = '@@account/UPDATE_ACCOUNT',
}
