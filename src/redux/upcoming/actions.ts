import { action } from "typesafe-actions";
import { UpcomingActionTypes, Upcoming } from "./types";
import { callApi } from "../../utils/api";

// Here we use the `action` helper function provided by `typesafe-actions`.
// This library provides really useful helpers for writing Redux actions in a type-safe manner.
// For more info: https://github.com/piotrwitek/typesafe-actions
export const fetchRequest = () => (dispatch: any) => {
  dispatch({
    type: UpcomingActionTypes.FETCH_REQUEST,
  });
  callApi(
    "get",
    "https://resell-tracker-api.herokuapp.com/releases/upcoming",
    "/"
  )
    .then((data) => {
      if (data.error) {
        dispatch({
          type: UpcomingActionTypes.FETCH_ERROR,
          payload: "Error fetching data",
        });
        return;
      }
      dispatch({
        type: UpcomingActionTypes.FETCH_SUCCESS,
        payload: data,
      });
    })
    .catch((error) => {
      dispatch({
        type: UpcomingActionTypes.FETCH_ERROR,
        payload: "Error fetching data",
      });
    });
};
