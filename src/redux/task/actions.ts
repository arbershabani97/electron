import { action } from 'typesafe-actions';
import { TaskActionTypes, ITask as Task } from './types';
const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;
// Here we use the `action` helper function provided by `typesafe-actions`.
// This library provides really useful helpers for writing Redux actions in a type-safe manner.
// For more info: https://github.com/piotrwitek/typesafe-actions

export const fetchRequest = () => (dispatch: any) => {
    dispatch({ type: TaskActionTypes.FETCH_REQUEST });
    try {
        ipcRenderer.send('get:tasks', 'get task');
        ipcRenderer.on('tasks:data', (event: any, tasks: any) => {
            if (tasks == undefined) {
                dispatch({ type: TaskActionTypes.FETCH_ERROR, payload: 'Error fetching task' });
                return;
            }
            dispatch({ type: TaskActionTypes.FETCH_SUCCESS, payload: tasks });
        });
    } catch (error) {
        dispatch({ type: TaskActionTypes.FETCH_ERROR });
    }
}
export const addRequest = (task: Task, groupId?: any) => (dispatch: any) => {
    dispatch({ type: TaskActionTypes.ADD_REQUEST, payload: { task, groupId } });
    ipcRenderer.send('task:add', task);
    ipcRenderer.on('task:added', (event: any, addedTask: any) => {
        dispatch({ type: TaskActionTypes.ADD_SUCCESS, payload: addedTask });
    })
}
export const updateRequest = (task: Task, groupId?: any) => (dispatch: any) => {
  dispatch({ type: TaskActionTypes.UPDATE_REQUEST, payload: { task, groupId } })
  ipcRenderer.send('task:update', task);
  ipcRenderer.on('task:updated', (event: any, updatedTask: any) => {
    dispatch({ type: TaskActionTypes.UPDATE_SUCCESS, payload: updatedTask });
  })
}

export const multiUpdateRequest = (task: Task, groupId?: any) => {
    return {
        type: TaskActionTypes.MULTI_UPDATE_REQUEST,
        payload: { task, groupId },
    };
}

export const deleteRequest = (task: Task, groupId?: any) => (dispatch: any) => {
    dispatch({ type: TaskActionTypes.DELETE_REQUEST, payload: { task, groupId } });
    ipcRenderer.send('task:delete', task);
    ipcRenderer.on('task:deleted', (event: any, myT: any) => {
        dispatch({ type: TaskActionTypes.DELETE_SUCCESS, payload: myT.id });
    })

}

export const runTask = (task: Task) => (dispatch: any) => {
  dispatch({
    type: TaskActionTypes.UPDATE_STATUS,
    payload: {
      id: task.id,
      status: 'Starting',
    },
  });
  ipcRenderer.send('task:start', task);
  ipcRenderer.on('update:task:status', (event: any, status: any) => {
    dispatch({
      type: TaskActionTypes.UPDATE_STATUS,
      payload: {
        id: status.id,
        status: status.status,
      },
    });
  });
};

export const stopTask = (task: Task) => (dispatch: any) => {
    dispatch({
      type: TaskActionTypes.UPDATE_STATUS,
      payload: {
        id: task.id,
        status: 'Stopping Task....',
      },
    });
    ipcRenderer.send('task:stop', task);
    ipcRenderer.on('update:task:status', (event: any, status: any) => {
      dispatch({
        type: TaskActionTypes.UPDATE_STATUS,
        payload: {
          id: status.id,
          status: status.status,
        },
      });
    });
  }

export const runAllTask = (tasks: Task[]) => (dispatch: any) => {
  tasks.forEach((task) => {
    dispatch({
      type: TaskActionTypes.UPDATE_STATUS,
      payload: {
        id: task.id,
        status: 'Starting',
      },
    });
  });
  ipcRenderer.send('task:start:all', '');
  ipcRenderer.on('update:task:status', (event: any, status: any) => {
    dispatch({
      type: TaskActionTypes.UPDATE_STATUS,
      payload: {
        id: status.id,
        status: status.status,
      },
    });
  });
};

export const stopAllTask = (tasks: Task[]) => (dispatch: any) => {
  tasks.forEach((task) => {
    dispatch({
      type: TaskActionTypes.UPDATE_STATUS,
      payload: {
        id: task.id,
        status: 'Stopping',
      },
    });
  });
  ipcRenderer.send('task:stop:all', '');
  ipcRenderer.on('update:task:status', (event: any, status: any) => {
    dispatch({
      type: TaskActionTypes.UPDATE_STATUS,
      payload: {
        id: status.id,
        status: status.status
      }
    })
})
}

export const createGroup = (tasks: Task[], name: any, id: string) => (dispatch: any) => {
    dispatch({ type: TaskActionTypes.CREATE_GROUP, payload: { tasks, name, id } });
}

export const getTasks = (id: any) => (dispatch: any) => {
    dispatch({ type: TaskActionTypes.GET_TASKS, payload: id });
}

export const updateGroupName = (name: any, id: string) => (dispatch: any) => {
    dispatch({ type: TaskActionTypes.UPDATE_GROUP_NAME, payload: { name, id } })
}

export const clearTasks = () => (dispatch: any) => {
  dispatch({ type: TaskActionTypes.CLEAR_TASKS })
}

export const deleteGroup = (groupId: string) => (dispatch: any) => {
  dispatch({ type: TaskActionTypes.DELETE_GROUP, payload: { groupId } })
}




// Remember, you can also pass parameters into an action creator. Make sure to
// type them properly as well.
// export const fetchSuccess = () => action(TaskActionTypes.FETCH_SUCCESS,)
// export const fetchError = (message: string) => action(TaskActionTypes.FETCH_ERROR, message)
// export const addSuccess = () => action(TaskActionTypes.ADD_SUCCESS)
// export const addError = (message: string) => action(TaskActionTypes.ADD_ERROR, message)
