import { Task } from 'redux-saga';
import { Reducer, combineReducers } from "redux";
import { TasksState, TaskActionTypes } from "./types";
const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

export const INITIAL_STATE: TasksState = {
  loading: false,
  data: [],
  group: [],
  errors: undefined,
};

const reducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case TaskActionTypes.FETCH_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case TaskActionTypes.FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case TaskActionTypes.FETCH_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload,
      };
    case TaskActionTypes.ADD_SUCCESS:
      let index = state.data.findIndex((el: any) => el.id == action.payload.id);
      if (index == -1) {
        return {
          ...state,
          loading: false,
          data: [...state.data, action.payload],
        };
      }
      return {
        ...state,
        loading: false
      }
    case TaskActionTypes.ADD_REQUEST:
      if (action.payload.groupId) {
        state.group.map((item: any) => {
          if (item.id === action.payload.groupId) {
            return item.data.push(action.payload.task)
          }
        })
        return {
          ...state,
          loading: true,
        };
      } else {
        return {
          ...state,
          data: state.data.concat(action.payload.task),
          loading: true,
        };
      }
    case TaskActionTypes.GET_TASKS:
      return {
        ...state,
        data: state.group.find((item: any) => {
          if (item.id === action.payload) {
            return item
          }
        }).data
      }
    case TaskActionTypes.UPDATE_REQUEST:
      if (action.payload.groupId) {
        state.group.map((item: any, i: any) => {
          if (item.id === action.payload.groupId) {
            return item.data[item.data.findIndex((el: any) => el.id === action.payload.task.id)] = action.payload.task
          }
        })
        return {
          ...state,
        };
      } else {
        return {
          ...state,
          data: state.data.map((item: any) => {
            if (item.id === action.payload.task.id) {
              return action.payload.task
            } else {
              return item;
            }
          }),
        };
      }
    case TaskActionTypes.MULTI_UPDATE_REQUEST:
      if (action.payload.groupId) {
        state.data.map((item: any) => {
          if (action.payload.task.keywords) item["keywords"] = action.payload.task.keywords;
          if (action.payload.task.size) item["size"] = action.payload.task.size;
          return item;
        })
        state.group.map((item: any, i: any) => {
          if (item.id === action.payload.groupId) {
            return item.data = state.data
          }
        })

        return {
          ...state
        }
      } else
        return {
          ...state,
          data: state.data.map((item: any) => {
            if (action.payload.task.keywords) item["keywords"] = action.payload.task.keywords;
            if (action.payload.task.size) item["size"] = action.payload.task.size;
            return item;
          })
        }
    case TaskActionTypes.ADD_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload,
      };
    case TaskActionTypes.DELETE_REQUEST:
      if (action.payload.groupId) {
        state.group.map((item: any) => {
          if (item.id === action.payload.groupId) {
            state.data = action.payload.task.id ? state.data.filter((item: any) => item.id !== action.payload.task.id) : []
            return item.data = state.data
          }
        })
        return {
          ...state,
        }
      } else {
        return {
          ...state,
          data: action.payload.task.id ? state.data.filter((item: any) => item.id !== action.payload.task.id) : [],
        };
      }
    case TaskActionTypes.DELETE_SUCCESS:
      return {
        ...state,
        data: state.data.filter((task: any) => task.id !== action.payload),
      };
    case TaskActionTypes.UPDATE_STATUS:
      const newList = state.data.map((item: any) =>
        item.id === action.payload.id
          ? { ...item, status: action.payload.status }
          : item
      );
      return {
        ...state,
        data: newList,
      };
    case TaskActionTypes.CREATE_GROUP:

      return {
        ...state,
        group: state.group.concat({ id: action.payload.id, name: action.payload.name, data: action.payload.tasks }),
        data: [],
      }
    case TaskActionTypes.UPDATE_GROUP_NAME:
      const updatedGroup = state.group.map((item: any) =>
        item.id === action.payload.id ? { ...item, name: action.payload.name } : item
      );
      return {
        ...state,
        group: updatedGroup
      }
    case TaskActionTypes.CLEAR_TASKS:
      return {
        ...state,
        data: []
      }
    case TaskActionTypes.DELETE_GROUP:
      state.data.map(item => {
        ipcRenderer.send('task:delete', item)
      })
      return {
        ...state,
        group: state.group.filter((item: any) => item.id !== action.payload.groupId),
        data: []
      }
    default:
      return state;
  }
};

export { reducer as tasksReducer };
