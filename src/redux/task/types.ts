import { IProfile } from '../profile/types';

export interface ITask {
  id: string;
  store: any;
  proxy: any;
  monitorInput: string;
  profile: IProfile;
  account: any;
  size: string;
  rate: string;
  status: string;
  advanced: IAdvanced;
  computed?: IComputed;
}

interface IAdvanced {
  delays: {
    error: IError;
    monitor: IMonitor;
  };
  rates: IRates;
}

interface IError {
  min: any;
  max: any;
}

interface IMonitor {
  min: any;
  max: any;
}

interface IRates {
  min: any;
  max: any;
  shipping: string;
  profile: any;
}

interface IComputed {
  productName?: string;
  status?: string;
  taskCount?: number;
}

export enum TaskActionTypes {
    FETCH_REQUEST = '@@tasks/FETCH_REQUEST',
    FETCH_SUCCESS = '@@tasks/FETCH_SUCCESS',
    FETCH_ERROR = '@@tasks/FETCH_ERROR',
    ADD_REQUEST = '@@tasks/ADD_REQUEST',
    UPDATE_REQUEST = '@@tasks/UPDATE_REQUEST',
    MULTI_UPDATE_REQUEST = '@@tasks/MULTI_UPDATE_REQUEST',
    ADD_SUCCESS = '@@tasks/ADD_SUCCESS',
    ADD_ERROR = '@@tasks/ADD_ERROR',
    DELETE_REQUEST = '@@tasks/DELETE_REQUEST',
    DELETE_SUCCESS = '@@tasks/DELETE_SUCCESS',
    DELETE_ERROR = '@@tasks/DELETE_ERROR',
    UPDATE_STATUS = '@@tasks/UPDATE_STATUS',
    CREATE_GROUP = '@@taks/CREATE_GROUP',
    UPDATE_GROUP_NAME = '@@tasks/UPDATE_GROUP_NAME',
    GET_TASKS = '@@tasks/GET_TASKS',
    CLEAR_TASKS = '@@tasks/CLEAR_TASKS' ,
    DELETE_GROUP = '@@tasks/DELETE_GROUP' ,
  UPDATE_SUCCESS = '@@tasks/UPDATE_SUCCESS'
}

export interface TasksState {
    loading: boolean
    data: ITask[]
    errors?: string,
    group?: any
}
