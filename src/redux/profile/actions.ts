import { action } from 'typesafe-actions';
import { ProfileActionTypes, IProfile } from './types';
const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;
// Here we use the `action` helper function provided by `typesafe-actions`.
// This library provides really useful helpers for writing Redux actions in a type-safe manner.
// For more info: https://github.com/piotrwitek/typesafe-actions

export const fetchRequest = () => (dispatch: any) => {
  dispatch({ type: ProfileActionTypes.FETCH_REQUEST });
  try {
    ipcRenderer.send('get:profiles', 'get profile');
    ipcRenderer.on('profiles:data', (event: any, profiles: any) => {
      if (profiles == undefined) {
        dispatch({ type: ProfileActionTypes.FETCH_ERROR, payload: 'Error fetching profiles' });
        return;
      }
      dispatch({ type: ProfileActionTypes.FETCH_SUCCESS, payload: profiles });
    });
  } catch (error) {
    dispatch({ type: ProfileActionTypes.FETCH_ERROR });
  }
}

export const addRequest = (profile: IProfile) => (dispatch: any) => {
  dispatch({ type: ProfileActionTypes.ADD_REQUEST });
  ipcRenderer.send('profile:add', profile);
  ipcRenderer.on('profile:added', (event: any, profile: any) => {
    dispatch({ type: ProfileActionTypes.ADD_SUCCESS, payload: profile });
  })
}

export const updateRequest = (profile: IProfile) => (dispatch: any) => {
  dispatch({ type: ProfileActionTypes.UPDATE_REQUEST, payload: profile });
  ipcRenderer.send('profile:update', profile);
  ipcRenderer.on('profile:updated', (event: any, profile: any) => {
    dispatch({ type: '', payload: profile });
  })
}

export const exportRequest = (profiles: object) => (dispatch: any) => {
  ipcRenderer.send('profile:export', profiles);
  //alert(JSON.stringify(profiles))
}

export const importRequest = (profiles: IProfile[]) => (dispatch: any) => {
}

export const deleteRequest = (profile: IProfile) => (dispatch: any) => {
  dispatch({ type: ProfileActionTypes.DELETE_REQUEST, payload: profile });
  ipcRenderer.send('profile:delete', profile);
  ipcRenderer.on('profile:deleted', (event: any, myT: any) => {
    dispatch({ type: ProfileActionTypes.DELETE_SUCCESS, payload: myT.id });
  })
}

// Remember, you can also pass parameters into an action creator. Make sure to
// type them properly as well.
export const fetchSuccess = () => action(ProfileActionTypes.FETCH_SUCCESS,)
export const fetchError = (message: string) => action(ProfileActionTypes.FETCH_ERROR, message)
export const addSuccess = () => action(ProfileActionTypes.ADD_SUCCESS)
export const addError = (message: string) => action(ProfileActionTypes.ADD_ERROR, message)
