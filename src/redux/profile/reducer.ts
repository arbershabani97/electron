import { Reducer } from "redux";
import { ProfilesState, ProfileActionTypes } from "./types";

export const INITIAL_STATE: ProfilesState = {
  loading: false,
  data: [],
  errors: undefined,
};

const reducer: Reducer<ProfilesState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ProfileActionTypes.FETCH_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ProfileActionTypes.FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case ProfileActionTypes.FETCH_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload,
      };

    case ProfileActionTypes.ADD_SUCCESS:
      let index = state.data.findIndex((el: any) => el.id === action.payload.id);
      if (index === -1) {
        return {
          ...state,
          loading: false,
          data: [...state.data, action.payload],
        };
      }
      return {
        ...state,
        loading: false
      };
    case ProfileActionTypes.ADD_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ProfileActionTypes.ADD_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload,
      };

    case ProfileActionTypes.DELETE_REQUEST:
      return {
        ...state,
        data: state.data.filter(prof => prof.id !== action.payload.id)
      }
    case ProfileActionTypes.UPDATE_REQUEST:
      return {
        ...state,
        data: state.data.map((item: any) => {
          if (item.id === action.payload.profile.id) {
            return action.payload.profile
          } else {
            return item;
          }
        })
      }
    case ProfileActionTypes.DELETE_SUCCESS:
      return {
        ...state,
        data: state.data.filter(prof => prof.id !== action.payload)
      }

    default:
      return state;
  }
};

export { reducer as profilessReducer };
