export interface IProfile {
  id: string;
  color: string;
  name: string;
  billingInfo: IBillingAddress;
  shippingInfo: IShippingAddress;
  paymentInfo: IPaymentDetails;
}

interface IBillingAddress {
  firstName: string;
  lastName: string;
  address: string;
  aptNumber: string;
  state: string;
  country: string;
  zipcode: string;
  email: string;
  phoneNumber: string;
  isSameAsShipping: boolean;
}

interface IShippingAddress {
  firstName: string;
  lastName: string;
  address: string;
  aptNumber: string;
  state: string;
  country: string;
  zipcode: string;
  email: string;
  phoneNumber: string;
}

interface IPaymentDetails {
  cardHolderName: string;
  cardNumber: string;
  cardType: string;
  cvv: string;
  expiryMonth: string;
  expiryYear: string;
  email: string;
}

export enum ProfileActionTypes {
  FETCH_REQUEST = '@@profiles/FETCH_REQUEST',
  FETCH_SUCCESS = '@@profiles/FETCH_SUCCESS',
  FETCH_ERROR = '@@profiles/FETCH_ERROR',
  ADD_REQUEST = '@@profiles/ADD_REQUEST',
  UPDATE_REQUEST = '@@profiles/UPDATE_REQUEST',
  ADD_SUCCESS = '@@profiles/ADD_SUCCESS',
  ADD_ERROR = '@@profiles/ADD_ERROR',
  DELETE_REQUEST = '@@profiles/DELETE_REQUEST',
  DELETE_SUCCESS = '@@profiles/DELETE_SUCCESS',
  DELETE_ERROR = '@@profiles/DELETE_ERROR',
}

export interface ProfilesState {
  loading: boolean;
  data: IProfile[];
  errors?: string;
}
