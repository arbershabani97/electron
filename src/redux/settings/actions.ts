import { SettingsActionTypes } from './types';

export const updateError = (error: number) => {
  return {
    type: SettingsActionTypes.UPDATE_ERROR,
    payload: error,
  };
};

export const updateMonitor = (monitor: number) => {
  return {
    type: SettingsActionTypes.UPDATE_MONITOR,
    payload: monitor,
  };
};

export const updateWebhook = (webhook: string) => {
  return {
    type: SettingsActionTypes.UPDATE_WEBHOOK,
    payload: webhook,
  };
};
