export interface Settings {
  error: number;
  monitor: number;
  webhook?: string;
}

export enum SettingsActionTypes {
  UPDATE_ERROR = '@@settings/UPDATE_ERROR',
  UPDATE_MONITOR = '@@settings/UPDATE_MONITOR',
  UPDATE_WEBHOOK = '@@settings/UPDATE_WEBHOOK',
}
