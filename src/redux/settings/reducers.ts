import { Reducer } from 'redux';
import { Settings, SettingsActionTypes } from './types';

const initialState: Settings = {
  error: 0,
  monitor: 0,
  webhook: 'test',
};

const reducer: Reducer<Settings> = (state = initialState, action) => {
  switch (action.type) {
    case SettingsActionTypes.UPDATE_ERROR: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case SettingsActionTypes.UPDATE_MONITOR: {
      return {
        ...state,
        monitor: action.payload,
      };
    }
    case SettingsActionTypes.UPDATE_WEBHOOK: {
      return {
        ...state,
        webhook: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export { reducer as settingsReducer };
