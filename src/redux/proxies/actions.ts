import { ProxiesActionTypes } from './types';
const electron = window.require('electron');
const ipcRenderer = electron.ipcRenderer;

export const fetchRequest = () => (dispatch: any) => {
  dispatch({ type: ProxiesActionTypes.FETCH_REQUEST });
  try {
    ipcRenderer.send('get:proxies', 'get proxies');
    ipcRenderer.on('proxies:data', (event: any, proxies: any) => {
      console.log(proxies)
      if (proxies == undefined) {
        dispatch({ type: ProxiesActionTypes.FETCH_ERROR, payload: 'Error fetching task' });
        return;
      }
      dispatch({ type: ProxiesActionTypes.FETCH_SUCCESS, payload: proxies });
    });
  } catch (error) {
    console.log(error)
  }
}

export const addProxy = (proxy: any) => (dispatch: any) => {
  dispatch({ type: ProxiesActionTypes.ADD_PROXY, payload: proxy });
  ipcRenderer.send('proxies:add', proxy);
  ipcRenderer.on('proxies:added', (event: any, addedProxy: any) => {
    dispatch({ type: ProxiesActionTypes.ADD_SUCCESS, payload: addedProxy });
  })
};

export const updateProxy = (proxy: any) => (dispatch: any) => {
  dispatch({ type: ProxiesActionTypes.UPDATE_PROXY, payload: proxy });
  ipcRenderer.send('proxies:update', proxy);
  ipcRenderer.on('proxies:updated', (event: any, addedProxy: any) => {
    dispatch({ type: ProxiesActionTypes.ADD_SUCCESS, payload: addedProxy });
  })
};

export const deleteProxy = (proxy: any) => (dispatch: any) => {
  dispatch({ type: ProxiesActionTypes.DELETE_PROXY, payload: proxy });
  ipcRenderer.send('proxies:delete', proxy);
  ipcRenderer.on('proxies:deleted', (event: any, myT: any) => {
    dispatch({ type: ProxiesActionTypes.DELETE_SUCCESS, payload: myT.id });
  })
  return {
    type: ProxiesActionTypes.DELETE_PROXY,
    payload: proxy,
  };
};
