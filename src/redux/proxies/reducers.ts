import { Reducer } from 'redux';
import { ProxiesActionTypes, Proxies } from './types';

const initialState: Proxies = {
  proxies: []
};

const reducer: Reducer<any> = (state = initialState, action) => {
  switch (action.type) {
    case ProxiesActionTypes.FETCH_REQUEST:
      return {
        ...state,
      };
    case ProxiesActionTypes.FETCH_SUCCESS:
      return {
        ...state,
        proxies: action.payload,
      };
    case ProxiesActionTypes.FETCH_ERROR:
      return {
        ...state,
        errors: action.payload,
      };
    case ProxiesActionTypes.ADD_PROXY: {
      return {
        ...state,
        proxies: state.proxies.concat(action.payload),
      };
    }
    case ProxiesActionTypes.UPDATE_PROXY: {
      return {
        ...state,
        proxies: state.proxies.map((item: any) => {
          if (item.groupId === action.payload.groupId) {
            return action.payload
          } else {
            return item;
          }
        }),
      };
    }
    case ProxiesActionTypes.DELETE_PROXY: {
      return {
        ...state,
        proxies: state.proxies.filter((item: any) => item.groupId !== action.payload.groupId),
      };
    }

    default: {
      return state;
    }
  }
};

export { reducer as proxyReducer };
