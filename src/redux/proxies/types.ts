export interface Proxies {
  proxies: Proxy[]
}

export interface Proxy {
  groupId: string;
  groupList: any;
  groupName: any;
}

export enum ProxiesActionTypes {
  ADD_PROXY = '@@proxy/ADD_PROXY',
  ADD_SUCCESS = '@@proxy/ADD_SUCCESS',
  UPDATE_PROXY = '@@proxy/UPDATE_PROXY',
  DELETE_PROXY = '@@proxy/DELETE_PROXY',
  FETCH_REQUEST = '@@proxy/FETCH_REQUEST',
  FETCH_ERROR = '@@proxy/FETCH_ERROR',
  FETCH_SUCCESS = '@@proxy/FETCH_SUCCESS',
  DELETE_SUCCESS = `@@proxy/DELETE_SUCCESS`
}
