import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { ApplicationState } from './redux';
import { Store } from 'redux';
import configureStore from './configureStore';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router'; // WHEN I ADDED /IMMUTABLE IT SOLVED MY PROBLEM

import 'bootstrap/dist/css/bootstrap.min.css';

// We use hash history because this example is going to be hosted statically.
// Normally you would use browser history.
const history = createBrowserHistory();
declare global {
  interface Window {
    INITIAL_REDUX_STATE?: any;
  }
}

const initialState = window.INITIAL_REDUX_STATE;
const store = configureStore(history, initialState);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
