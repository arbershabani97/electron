import React from "react";
import { InputContainer } from "./pupup-input.style";

interface IInputProps {
  grow: boolean;
  handleChange: any;
  type: string;
  id: string;
  value: string;
  placeholder: string;
}
const PupupInput: React.SFC<IInputProps> = ({
  grow,
  handleChange,
  type,
  id,
  value,
  placeholder,
}) => (
  <InputContainer
    grow={grow}
    type={type}
    onChange={handleChange}
    placeholder={placeholder}
    // id={id}
    // placeholder={placeholder}
  />
);

export default PupupInput;
