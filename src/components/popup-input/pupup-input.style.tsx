import styled from "styled-components";


export const InputContainer=styled.input<{grow:boolean}>`
    flex-grow:${props=>props.grow?"1":"1"};
    color:white;
    margin:0px;
    border:none;
    height:30px;
    width: 100%;
    padding:10px;
    font-size:12px;
    text-align:center;
    background: #0C0C0C;
    box-shadow: -1px -1px 2px rgba(60,60,60,0.25), 2px 2px 4px black;
    border-radius: 5px;`;