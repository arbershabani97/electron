import React, { useState, useEffect } from 'react';
import CircleContainer from '../../components/analyticItem/circle.component';
function CountHeader(props: any) {
    // const [activeFilter, setCurrentFilter] = useState([]);


    return (
        <>
            {props.indHeader.type === "icon" &&
                <div style={{ width: "10%" }}>
                    <img src={props.indHeader.url} style={{ width: 25 }}></img>
                </div>
            }
            {props.indHeader.type === "label" &&
                <div style={{
                    width: "35%", textAlign: "center", display: "flex", flexDirection: "column"
                }}>
                    <p style={{ margin: 0, textTransform: 'capitalize' }}>{props.indHeader.value}</p>
                    <div style={{ display: "flex", flex: 1 }}>
                        <CircleContainer count={props.indHeader.count} color={props.indHeader.color}></CircleContainer>
                    </div>
                </div>
            }
            {props.indHeader.type === "list" &&
                <div style={{
                    width: "30%",
                    textAlign: "center",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between"
                }}>
                    {/* <p style={{
                        background: "#0C0C0C",
                        boxShadow: "4px 4px 9px #0D0D0D, -3px -1px 4px rgba(66, 66, 66, 0.41)",
                        borderRadius: 15, textTransform: 'capitalize', padding: "10px 15px", margin: 0
                    }}>{props.indHeader.value}</p> */}
                    <div style={{
                        background: "#0C0C0C",
                        marginTop: "15px",
                        height: "70px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        boxShadow: "-1px -1px 2px rgba(60,60,60,0.25), 2px 2px 4px black",
                        borderRadius: 5
                    }}>
                        <p style={{ margin: 0, 'color': props.indHeader.color, padding: '5px 0px 10px',filter: "drop-shadow("+props.indHeader.color+" 0px 0px 0.75rem)" }}>
                            ${props.indHeader.count}
                        </p>
                    </div>
                </div>
            }

        </>
    );
};

export default CountHeader;
