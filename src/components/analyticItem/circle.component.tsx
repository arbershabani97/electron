import React from "react";
import './circle.css';

function CircleContainer(props: any) {

    return (
        <>
            {/* <div id="Artboard__1"> */}
            <div className="pure-css-clock" style={{ filter: `drop-shadow(0 0 0.75rem ${props.color ? props.color : '#FDACAC'}` }}>
                <div className="quater-arc-outer upper-left">
                    <div className="quater-arc-inner">
                        <div className="quater-arc-circle"></div>
                    </div>
                </div>
                <div className="quater-arc-outer upper-right">
                    <div className="quater-arc-inner">
                        <div className="quater-arc-circle"></div>
                    </div>
                </div>
                <div className="quater-arc-outer lower-left">
                    <div className="quater-arc-inner">
                        <div className="quater-arc-circle"></div>
                    </div>
                </div>
                <div className="quater-arc-outer lower-right">
                    <div className="quater-arc-inner">
                        <div className="quater-arc-circle"></div>
                    </div>
                </div>
                <div className="disc" style={{ color: props.color ? props.color : '#FDACAC', fontSize: 20 }}>{props.count}</div>
            </div>
            {/* </div> */}

        </>
    );
};

export default CircleContainer;