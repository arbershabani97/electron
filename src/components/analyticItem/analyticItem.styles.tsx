import styled from "styled-components";

export const AnalyticsItemContainer=styled.div`
border-radius:10px;
height:40px;
width:100%;
margin-bottom:7px;
padding:0px 8px;
display:flex;
justify-content:space-between;
align-items:center;
// background: #0c0c0c;
/* pkiouy7 */

box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
`;

export const AnalyticsNumber=styled.div`
color: #CDF4FE;
font-size:12px;
`;

export const AnalyticsInfo=styled.div`
// background-color:red;
width:90%;
color: #CDF4FE;
display:flex;
flex-direction:column;
// margin-right:35px;
`;

export const AnalyticsItemName=styled.div`
color:#CDF4FE;
font-size:12px;   
display:flex;
justify-content:space-between;
`;

export const AnalyticsItemStore=styled.div`
color:#818181;
font-size:10.5px;   
display:flex;
justify-content:space-between;
`;

export const AnalyticsStatus=styled.div`
color:#CDF4FE;
font-size:12px;
// background:red;
width:19%;
text-align:center;
text-shadow: 0px -10px 50px rgba(253, 172, 172, 0.5), 0px 10px 50px rgba(253, 172, 172, 0.5);
`;

export const AnalyticsIconButtons=styled.div`
    display:flex;
    justify-content:space-between;
    align-items:center;
`;

export const AnalyticsIconBtnCtn=styled.div`
padding-right:5px; 
display:flex;
justify-content:space-between;
align-items:center;                                                                                                                                                                                                           px;
`;
