import React from "react";
import {
    AnalyticsItemContainer,
    AnalyticsNumber, AnalyticsInfo,
    AnalyticsStatus, AnalyticsIconButtons,
    AnalyticsIconBtnCtn, AnalyticsItemName,
    AnalyticsItemStore
} from "./analyticItem.styles";
import van from "../../assets/images/van.svg";

const AnalyticItem = () => (
    <>
        <AnalyticsItemContainer>
            <AnalyticsInfo>
                <AnalyticsItemName>
                    <div className="">
                        Adidas Yeezy 350 - Black
                </div>
                    <div className="">
                        8 US
                </div>
                    <div className="" style={{
                        textShadow: "0px 10px 25px rgba(172, 253, 180, 0.5), 0px -10px 25px rgba(172, 253, 180, 0.5)"
                    }}>
                        $242
                </div>
                </AnalyticsItemName>
                <AnalyticsItemStore>
                    <div className="">YeezySupply</div>
                    <div className="">Notifees 1</div>
                    <div className="">June 19th, 2020</div>
                </AnalyticsItemStore>

            </AnalyticsInfo>
            <AnalyticsIconButtons>
                <AnalyticsIconBtnCtn>
                    <img
                        src={van}
                        className="App-logo"
                        alt="icon"
                        width="18"
                        height="18"
                    />
                </AnalyticsIconBtnCtn>

            </AnalyticsIconButtons>
        </AnalyticsItemContainer>
        {/*  */}
        <AnalyticsItemContainer>
            <AnalyticsInfo>
                <AnalyticsItemName>
                    <div className="">
                        Adidas Yeezy 350 - Black
                </div>
                    <div className="">
                        8 US
                </div>
                    <div className="" style={{
                        textShadow: "0px 10px 25px rgba(172, 253, 180, 0.5), 0px -10px 25px rgba(172, 253, 180, 0.5)"
                    }}>
                        $242
                </div>
                </AnalyticsItemName>
                <AnalyticsItemStore>
                    <div className="">YeezySupply</div>
                    <div className="">Notifees 1</div>
                    <div className="">June 19th, 2020</div>
                </AnalyticsItemStore>

            </AnalyticsInfo>
            <AnalyticsIconButtons>
                <AnalyticsIconBtnCtn>
                    <img
                        src={van}
                        className="App-logo"
                        alt="icon"
                        width="18"
                        height="18"
                    />
                </AnalyticsIconBtnCtn>

            </AnalyticsIconButtons>
        </AnalyticsItemContainer>
        {/*  */}
        <AnalyticsItemContainer>
            <AnalyticsInfo>
                <AnalyticsItemName>
                    <div className="">
                        Adidas Yeezy 350 - Black
                </div>
                    <div className="">
                        8 US
                </div>
                    <div className="" style={{
                        textShadow: "0px 10px 25px rgba(172, 253, 180, 0.5), 0px -10px 25px rgba(172, 253, 180, 0.5)"
                    }}>
                        $242
                </div>
                </AnalyticsItemName>
                <AnalyticsItemStore>
                    <div className="">YeezySupply</div>
                    <div className="">Notifees 1</div>
                    <div className="">June 19th, 2020</div>
                </AnalyticsItemStore>

            </AnalyticsInfo>
            <AnalyticsIconButtons>
                <AnalyticsIconBtnCtn>
                    <img
                        src={van}
                        className="App-logo"
                        alt="icon"
                        width="18"
                        height="18"
                    />
                </AnalyticsIconBtnCtn>

            </AnalyticsIconButtons>
        </AnalyticsItemContainer>
        {/*  */}<AnalyticsItemContainer>
            <AnalyticsInfo>
                <AnalyticsItemName>
                    <div className="">
                        Adidas Yeezy 350 - Black
                </div>
                    <div className="">
                        8 US
                </div>
                    <div className="" style={{
                        textShadow: "0px 10px 25px rgba(172, 253, 180, 0.5), 0px -10px 25px rgba(172, 253, 180, 0.5)"
                    }}>
                        $242
                </div>
                </AnalyticsItemName>
                <AnalyticsItemStore>
                    <div className="">YeezySupply</div>
                    <div className="">Notifees 1</div>
                    <div className="">June 19th, 2020</div>
                </AnalyticsItemStore>

            </AnalyticsInfo>
            <AnalyticsIconButtons>
                <AnalyticsIconBtnCtn>
                    <img
                        src={van}
                        className="App-logo"
                        alt="icon"
                        width="18"
                        height="18"
                    />
                </AnalyticsIconBtnCtn>

            </AnalyticsIconButtons>
        </AnalyticsItemContainer>
        {/*  */}
        <AnalyticsItemContainer>
            <AnalyticsInfo>
                <AnalyticsItemName>
                    <div className="">
                        Adidas Yeezy 350 - Black
                </div>
                    <div className="">
                        8 US
                </div>
                    <div className="" style={{
                        textShadow: "0px 10px 25px rgba(172, 253, 180, 0.5), 0px -10px 25px rgba(172, 253, 180, 0.5)"
                    }}>
                        $242
                </div>
                </AnalyticsItemName>
                <AnalyticsItemStore>
                    <div className="">YeezySupply</div>
                    <div className="">Notifees 1</div>
                    <div className="">June 19th, 2020</div>
                </AnalyticsItemStore>

            </AnalyticsInfo>
            <AnalyticsIconButtons>
                <AnalyticsIconBtnCtn>
                    <img
                        src={van}
                        className="App-logo"
                        alt="icon"
                        width="18"
                        height="18"
                    />
                </AnalyticsIconBtnCtn>

            </AnalyticsIconButtons>
        </AnalyticsItemContainer>
    </>
);
export default AnalyticItem;