import styled from "styled-components";

export const ModalWrapper = styled.div`
position: fixed;
top: 0;
left: 0;
bottom: 0;
z-index: 1050;
display: flex;
align-items: center;
`;
export const ModalBoxSetup = styled.div`
position: absolute;
left: 0;
right: 0;
width: 35%;
height: 70%;
overflow:hidden;
padding:16px;
border-radius: 15px;
margin: 20px auto;
box-sizing:border-box;
z-index:1;
background: #0C0C0C;
border: 2px solid #ACECFD;
box-shadow: 0px -5px 15px rgba(172,236,253,0.5), 0px 5px 15px rgba(172,236,253,0.5);
`;
export const ModalBg = styled.div`
width: 100vw;
height: 100vh;
z-index: 0;
background: rgba(0,0,0,0.5);
`
export const TopHeader = styled.div`
    display: flex;
    align-items: flex-start;
`

export const ActionContainer = styled.div`
    flex: 1;
    display: flex;
`

export const CloseContainer = styled.div`
    flex: 1;
`
// ${props => props.width || "32%"}
// ${color.white}