import React, { Component } from "react";
import { ModalWrapper, ModalBoxSetup, ModalBg, TopHeader, ActionContainer, CloseContainer } from "./popup.style";
import PropTypes from "prop-types";
import up from "../../assets/images/up.svg";
import down from "../../assets/images/down.svg";

export default class PopupContainer extends Component {
    constructor(props: any) {
        super(props);
    }
    render() {
        const { visible, dismiss, children, action }: any = this.props;
        return (
            <>
                {visible ? (
                    <ModalWrapper>
                        <ModalBoxSetup>
                            <TopHeader>
                                <ActionContainer>
                                    {action &&
                                        <>
                                            <img
                                                src={up}
                                                className="App-logo"
                                                alt="icon"
                                                width="40"
                                                height="40"
                                            />
                                            <img
                                                src={down}
                                                className="App-logo"
                                                alt="icon"
                                                width="40"
                                                height="40"
                                            />
                                        </>
                                    }
                                </ActionContainer>
                                <CloseContainer>
                                    <p style={{
                                        margin: 0,
                                        textAlign: 'right',
                                        color: '#ffffff', fontWeight: 700, cursor: 'pointer'
                                    }} onClick={dismiss}>X</p>
                                </CloseContainer>
                            </TopHeader>

                            {children}
                        </ModalBoxSetup>
                        <ModalBg onClick={dismiss} />
                    </ModalWrapper>
                ) : null}
            </>
        );
    }
    static propTypes = {
        visible: PropTypes.bool,
        dismiss: PropTypes.func,
        action:PropTypes.bool
    };
}