import styled from 'styled-components';


export const SelectGrid = styled.div`
  width:100%;
  display:flex;
  padding: 10px;
  align-items: center;
  justify-content: center;
`;

export const InputGrid = styled.div`
  width:100%;
  display:flex;
  align-item: center;
  padding: 20px 10px;
`;

export const IndSelect1 = styled.div`
    flex: 1;
    padding: 0px;
    padding-right: 5px;
`

export const IndSelect2 = styled.div`
    flex: 1;
    padding: 0px;
    padding-left: 5px;
`

export const MinMaxGrid = styled.div`
    width: 100%;
    padding: 30px 0;
    height: auto;
`

export const SaveBtn = styled.div`
  width: 40px;
  height: 40px;
  display:flex;
  align-items:center;
  justify-content:center;
  margin:10px;
  color:white;
  font-size:20px;
  cursor: pointer;
  // background: #0c0c0c;
  /* pkiouy7 */

  box-shadow: -3px -1px 4px rgba(66, 66, 66, 0.41), 4px 4px 9px #0D0D0D;
  border-radius: 100px;
`