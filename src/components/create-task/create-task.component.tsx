import * as React from 'react';
import { useState, useEffect } from 'react';
import styled, { css, StyledFunction } from 'styled-components';
import SelectComponent from '../../components/select/select.component';
import {
    SelectGrid,
    IndSelect1,
    IndSelect2,
    MinMaxGrid,
    SaveBtn,
} from './create-task.style';
import {
    InputContainer,
  } from '../../components/new-task/newTask.styles';
import Input from '../../components/input/input.component';
import PupupInput from '../../components/popup-input/popup-input.component';
import {
    NewTaskBtn,
  } from '../../components/new-task/newTask.styles';
import save from "../../assets/images/save-proxy.svg";
 
const select1 = [
    { key: "YeezySupply", value: "YeezySupply" }, { key: "YeezySupply", value: "YeezySupply" },
    { key: "YeezySupply", value: "YeezySupply" }, { key: "YeezySupply", value: "YeezySupply" }
]

const select2 = [
    { key: "Proxies", value: "Proxies" }, { key: "Proxies", value: "Proxies" },
    { key: "Proxies", value: "Proxies" }, { key: "Proxies", value: "Proxies" }
]

const select3 = [
    { key: "Account", value: "Account" }, { key: "Account", value: "Account" },
    { key: "Account", value: "Account" }, { key: "Account", value: "Account" }
]

const select4 = [
    { key: "Rates", value: "Rates" }, { key: "Rates", value: "Rates" },
    { key: "Rates", value: "Rates" }, { key: "Rates", value: "Rates" }
]

function CreateTask(props: any) {

    return (
        <>
            <SelectGrid>
                <IndSelect1>
                    <SelectComponent placeholder={"select option"} options={select1}></SelectComponent>
                </IndSelect1>
                <IndSelect2>
                    <SelectComponent placeholder={"select option two"} options={select2}></SelectComponent>
                </IndSelect2>
            </SelectGrid>
            <MinMaxGrid>
                <SelectGrid>
                    <IndSelect1>
                        <InputContainer>
                            <label htmlFor="monitorMin">Monitor</label>
                        </InputContainer>
                    </IndSelect1>
                    <IndSelect2>
                        <InputContainer>
                            <label htmlFor="monitorMin">Error</label>
                        </InputContainer>
                    </IndSelect2>
                </SelectGrid>
                <SelectGrid>
                    <IndSelect1>
                        <div>
                        <PupupInput
                            grow={true}
                            handleChange={() => { }}
                            type="text"
                            id="monitorMin"
                            value="test"
                            placeholder="min"
                        />
                        </div>
                    </IndSelect1>
                    <IndSelect2>
                        <div>
                        <PupupInput
                            grow={true}
                            handleChange={() => { }}
                            type="text"
                            id="monitorMinError"
                            value="test"
                            placeholder="min"
                        />
                        </div>
                    </IndSelect2>
                </SelectGrid>
                <SelectGrid>
                    <IndSelect1>
                        <div>
                        <PupupInput
                            grow={true}
                            handleChange={() => { }}
                            type="text"
                            id="monitorMax"
                            value="test"
                            placeholder="max"
                        />
                        </div>
                    </IndSelect1>
                    <IndSelect2>
                        <div>
                        <PupupInput
                            grow={true}
                            handleChange={() => { }}
                            type="text"
                            id="monitorMaxError"
                            value="test"
                            placeholder="max"
                        />
                        </div>
                    </IndSelect2>
                </SelectGrid>
            </MinMaxGrid>
            <SelectGrid>
                <IndSelect1>
                    <SelectComponent placeholder={"select option"} options={select3}></SelectComponent>
                </IndSelect1>
                <IndSelect2>
                    <SelectComponent placeholder={"select option two"} options={select4}></SelectComponent>
                </IndSelect2>
            </SelectGrid>
            <SelectGrid>
                <SaveBtn>
                    <img
                        src={save}
                        className="App-logo"
                        alt="icon"
                        width="15"
                        height="15"
                    />
                </SaveBtn>
            </SelectGrid>
        </>
    );
}

export default CreateTask;

