import React from 'react';
import {
  TaskItemContainer,
  TaskNumber,
  TaskInfo,
  TaskStatus,
  TaskIconButtons,
  TaskIconBtnCtn,
  TaskItemName,
  TaskItemStore,
  TaskDetail,
} from './taskItem.styles';
import del from '../../assets/images/delete.svg';
import play from '../../assets/images/play.svg';
import pause from '../../assets/images/pause.svg';
import copy from '../../assets/images/copy.svg';
import edit from '../../assets/images/edit.svg';

const TaskItem = ({ task, id, deleteRequest, editRequest, duplicateTask ,startTask }: any) => (
  <TaskItemContainer>
    <TaskNumber>
      { id + 1 }
    </TaskNumber>
    <TaskDetail>
      <TaskInfo>
        <TaskItemName>
          <div className="">{task?.computed?.productName}</div>
          <div className="">{task?.rate || '-'} US</div>
        </TaskItemName>
        <TaskItemStore>
          {/* {<div className="">Kith</div>} */}
          <div className="">{task?.store?.longCode}</div>
        </TaskItemStore>
      </TaskInfo>
      <TaskStatus>{task?.computed?.status}</TaskStatus>
      <TaskIconButtons>
        <TaskIconBtnCtn>
          <img src={play} className="App-logo" onClick={() => startTask(task)} alt="icon" width="18" height="18" />
        </TaskIconBtnCtn>
        <TaskIconBtnCtn>
          <img src={pause} className="App-logo" alt="icon" width="15" height="15" />
        </TaskIconBtnCtn>
        <TaskIconBtnCtn onClick={() => duplicateTask(task)}>
          <img src={copy} className="App-logo" alt="icon" width="15" height="15" />
        </TaskIconBtnCtn>
        <TaskIconBtnCtn onClick={() => editRequest(task)}>
          <img src={edit} className="App-logo" alt="icon" width="15" height="15" />
        </TaskIconBtnCtn>
        <TaskIconBtnCtn onClick={() => deleteRequest(task)}>
          <img src={del} className="App-logo" alt="icon" width="15" height="15" />
        </TaskIconBtnCtn>
      </TaskIconButtons>
    </TaskDetail>
  </TaskItemContainer>
);

export default TaskItem;
