import styled from "styled-components";

export const TaskItemContainer=styled.div`
border-radius:10px;
height:43px;
width:104.4%;
margin-bottom:6px;
padding:0px 8px;
display:flex;
justify-content:flex-start;
align-items:center;
// background: #0c0c0c;
/* pkiouy7 */

box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
`;

export const TaskNumber=styled.div`
    color: #CDF4FE;
    font-size:12px;
    width: 5%;
    width: 30px;
    height: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;
export const TaskDetail=styled.div`
    width: 95%;
    display: flex;
    justify-content: space-between;
`
export const TaskInfo=styled.div`
    width:40%;
    color: #CDF4FE;
    display:flex;
    flex-direction:column;
`;

export const TaskItemName=styled.div`
color:#CDF4FE;
font-size:12px;   
display:flex;
justify-content:space-between;
`;

export const TaskItemStore=styled.div`
color:#818181;
font-size:10.5px;   
display:flex;
justify-content:space-between;
`;

export const TaskStatus=styled.div`
color:#CDF4FE;
font-size:12px;
// background:red;
width:19%;
text-align:center;
text-shadow: 0px -10px 50px rgba(253, 172, 172, 0.5), 0px 10px 50px rgba(253, 172, 172, 0.5);
`;

export const TaskIconButtons=styled.div`
    display:flex;
    justify-content:space-between;
    align-items:center;
`;

export const TaskIconBtnCtn=styled.div`
padding-right:5px; 
display:flex;
justify-content:space-between;
align-items:center;                                                                                                                                                                                                           px;
`;
