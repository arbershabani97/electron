import styled from "styled-components";


export const InputContainer=styled.input<{grow:boolean}>`
    flex-grow:${props=>props.grow?"1":"1"};
    color:white;
    margin:10px;
    border:none;
    height: 30px;
    width: 95%;
    padding:${props=>props.grow?"0":"10px"};
    font-size:12px;
    text-align:center;  
    background: #0c0c0c;
    box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 4px black;
    border-radius: 5px;`;