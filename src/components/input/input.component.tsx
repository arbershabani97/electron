import React from 'react';
import { InputContainer } from './input.styles';

interface IInputProps {
  grow: boolean;
  handleChange: any;
  type: string;
  id: string;
  value: string;
  placeholder: string;
  name?: string;
  keyPress?: any;
}
const Input: React.SFC<IInputProps> = ({ grow, handleChange, type, id, value, placeholder, name, keyPress }) => (
  <InputContainer
    grow={grow}
    type={type}
    onChange={handleChange}
    placeholder={placeholder}
    name={name}
    id={id}
    value={value}
    onKeyDown={keyPress}
  />
);

export default Input;
