import styled from 'styled-components';

export const ShippingDataListctn = styled.div`
  width:100%;
  height:80%;
  color:#999999;
`;
export const ShippingItemLabel = styled.div`
  width:22%;
  text-align:center;
  font-size:13px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const ShippingItemCtn = styled.div`
    width: 105%;
    height:30px;
    display:flex;
    justify-content:space-between;
    align-items:center;
    margin:auto;
    margin-top:10px;
    border-radius: 5px;
    background: #0C0C0C;
    box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
`;