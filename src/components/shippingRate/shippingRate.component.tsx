import * as React from 'react';
import { useState, useEffect } from 'react';
import styled, { css, StyledFunction } from 'styled-components';
import { ShippingDataListctn, ShippingItemCtn, ShippingItemLabel } from './shipping-rate.style';

const shippingData = {
    header: ["profile", "site", "rate"],
    body: [
        ["Test profile", "kith", "shopify-UPS%20Ground-10.00"],
        ["Test profile", "kith", "shopify-UPS%20Ground-10.00"],
        ["Test profile", "kith", "shopify-UPS%20Ground-10.00"],
        ["Test profile", "kith", "shopify-UPS%20Ground-10.00"],
        ["Test profile", "kith", "shopify-UPS%20Ground-10.00"],
        ["Test profile", "kith", "shopify-UPS%20Ground-10.00"],
    ]
}
function ShippingRateList(props:any) {


    return (
        <>
           
            <ShippingDataListctn>
                <div style={{ width: '100%', background: '#0C0C0C' }}>
                    <div style={{
                        display: 'flex', justifyContent: 'space-between',
                        padding: '10px 0px', textTransform: "capitalize"
                    }}>
                        {shippingData.header.map((indHeader, i) =>
                            <ShippingItemLabel>{indHeader}</ShippingItemLabel>
                        )}
                    </div>
                </div>
                <div style={{ overflow: 'scroll', height: '50vh' }}>
                    {shippingData.body.map((indShippingData: any, i: any) =>
                        <React.Fragment key={i}>
                            <ShippingItemCtn>
                                {indShippingData.map((indData: any, j: any) =>
                                    <ShippingItemLabel>{indData}</ShippingItemLabel>
                                )}
                            </ShippingItemCtn>

                        </React.Fragment>
                    )}
                </div>
            </ShippingDataListctn>
        </>
    );
}

export default ShippingRateList;

