import React from 'react';
import { VersionNumberContainer } from './version-number.styles';

const VersionNumber = () => {
  return <VersionNumberContainer>V.0.0.1</VersionNumberContainer>;
};

export default VersionNumber;