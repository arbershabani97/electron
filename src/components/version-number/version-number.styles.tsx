import styled from "styled-components";

export const VersionNumberContainer = styled.span`
  color: #acecfd;
  margin-top: auto;
  width:40px;
  // background:blue;
  margin-bottom: 0vh;
  margin-left: 0.7;
  margin-right: 10px;
  font-size: 13px;
  font-weight: 900;
  transform: rotate(-90deg) translate(0, 50%);
  letter-spacing: 1px;
`;
