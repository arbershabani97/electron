import * as React from 'react';
import { useState, useEffect } from 'react';
import styled, { css, StyledFunction } from 'styled-components';
import SelectComponent from '../../components/select/select.component';
import {
    SelectGrid,
    IndSelect1,
    IndSelect2,
    InputGrid,
    ButtonGrid,
    Button,
    CookieList,
    IndItem
} from './generate-cookies.style';
import PupupInput from '../../components/popup-input/popup-input.component';

const cookiesData = [
    { url: 'https://www.yeezysupply.com', count: '145' },
    { url: 'https://www.adidas.com/us', count: '325' },
    { url: 'https://www.footlocker.com/us', count: '243' },
    { url: 'https://www.yeezysupply.com', count: '145' },
    { url: 'https://www.adidas.com/us', count: '325' },
    { url: 'https://www.footlocker.com/us', count: '243' },
];

const select1 = [
    { key: "YeezySupply", value: "YeezySupply" }, { key: "YeezySupply", value: "YeezySupply" },
    { key: "YeezySupply", value: "YeezySupply" }, { key: "YeezySupply", value: "YeezySupply" }
]

const select2 = [
    { key: "Proxies", value: "Proxies" }, { key: "Proxies", value: "Proxies" },
    { key: "Proxies", value: "Proxies" }, { key: "Proxies", value: "Proxies" }
]


function GenerateCookies(props: any) {

    return (
        <>
            <SelectGrid>
                <IndSelect1>
                    <SelectComponent placeholder={"select option"} options={select1}></SelectComponent>
                </IndSelect1>
                <IndSelect2>
                    <SelectComponent placeholder={"select option two"} options={select2}></SelectComponent>
                </IndSelect2>
            </SelectGrid>
            <InputGrid>
                <PupupInput grow={true} handleChange={() => { }} type="keywords" id="proxy" value="" placeholder="https://www.yeezysupply.com" />
            </InputGrid>
            <ButtonGrid>
                <Button><p style={{ color: '#999999' }}>Generate</p></Button>
            </ButtonGrid>
            <CookieList>
                {cookiesData.map((indCookieData, index) =>
                    <IndItem>
                        <div style={{ flex: 1 }}>
                            {indCookieData.url}
                        </div>
                        <div>
                            {indCookieData.count}
                        </div>
                    </IndItem>
                )}
            </CookieList>
        </>
    );
}

export default GenerateCookies;

