import styled from 'styled-components';


export const SelectGrid = styled.div`
  width:100%;
  display:flex;
  align-item: center;
`;

export const InputGrid = styled.div`
  width:100%;
  display:flex;
  align-item: center;
  padding: 15px 0px;
`;

export const IndSelect1 = styled.div`
    flex: 1;
    padding: 15px;
    padding-right: 10px;
`

export const IndSelect2 = styled.div`
    flex: 1;
    padding: 15px;
    padding-left: 5px;
`

export const ButtonGrid = styled.div`
    width:100%;
    display:flex;
    text-align: center;
    align-items: center;
    justify-content: center;
`;

export const Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width:140px;
    align-item: center;
    height: 30px;
    cursor:pointer;
    background: #0C0C0C;
    box-shadow: -1px -1px 2px rgba(60,60,60,0.25), 2px 2px 4px black;
    border-radius: 5px;
`;

export const CookieList = styled.div`
    width:100%;
    height: 34vh;
    overflow: scroll;
    margin-top: 20px;
    padding: 20px;
    color:#999999;
    background: #0C0C0C;
    box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
    border-radius: 10px;
`;

export const IndItem = styled.div`
    width:100%;
    height:30px;
    display: flex;
    padding: 0px 10px;
    align-items: center;
    background: #0C0C0C;
    box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
    border-radius: 10px;
    margin-bottom: 10px;
    font-size: 14px;
`; 
