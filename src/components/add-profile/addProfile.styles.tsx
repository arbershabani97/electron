import styled from "styled-components"

export const AddProfileContainer = styled.div`
    width: 100vw;
    height:100vh;
    background: rgba(0, 0, 0, 0.5);
    display:flex;
    justify-content:center;
    align-items:center;
`

export const AddProfileFormContainer = styled.div`
    // background-color:#0c0c0c;
    width:35%;
    height:70%;
    position: absolute;
    z-index:1;
    diaplay:flex;
    background: #0C0C0C;
    box-shadow: 0px -5px 15px rgba(172, 236, 253, 0.5), 0px 5px 15px rgba(172, 236, 253, 0.5);
    border-radius: 15px;
    display:flex;
`

export const AddProfileFormContainerHeader = styled.div`
    color:white;
    // width:30%;
    height:10%;
    display:flex;
    padding: 10px;   
`

export const InputColumnContainer = styled.div`
    display:flex;
    color: #BCBCBC;
    // justify-content:center;
    // align-items:center;
    flex-direction:column;
    // background:red;
    width:100%;
    padding: 0px 0px 0 10px;
    font-size: 14px;
    
`

export const AddProfileFormContainerTitle = styled.div`
    color: #ACECFD;
    font-size:16px;
    margin-top:-15px;
    padding-bottom:14px;
    // padding-left:40px;
    text-align:center;
    // width:60%;
    // flex-grow:0;
`

export const InputContainer = styled.div`
    width: 50%;
    display: flex;
    text-align:center;
    color: #BCBCBC;
    font-size:14px;
`

export const PaymentSaveContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-end;
    text-align:center;
    color: #BCBCBC;
    font-size:14px;
`

export const InputAddressContainer = styled.div`
    width: 70%;
    display: flex;
    justify-content: space-between;
`
export const InputAptContainer = styled.div`
    width: 30%;
    display: flex;
    justify-content: space-between;
`

export const AddProfileFormContainerIcon = styled.div`
    color:red;
    // width:60%;
`

export const AddProfileFormInputContainer = styled.div`
    width:100%;
    height:80%;
    padding:10px;
    color: #0c0c0c;
`

export const AddProfileFormInputRowContainer = styled.div`
    display:flex;
    justify-content: space-between;
    color:red;
    width:100%;
    padding: 10px 20px;
    height:80;
    color: #0c0c0c;
`

export const AddProfileFormSingleInputRowContainer = styled.div`
    display:flex;
    justify-content: center;
    color:red;
    width:100%;
    padding: 10px 20px;
    height:80;
    color: #0c0c0c;
`

export const FormBtn = styled.div`
width:80%;
//    margin-bottom:10px;
`

export const FormCtn = styled.form`
    width:100%;
    height:100%;
    padding:1000px
    display:flex;
    padding-top:20px;
    flex-direction:column;
    justify-content:sapce-around;
    align-items:center;
    box-shadow: 0px 0px 10px 0px rgba(172, 236, 253, 0.5);
    border-radius: 0px 20px 20px 0px;
    z-index: 10;
`

export const SideBar = styled.div`
display:flex;
justify-content:space-evenly;
height:100%;
align-items: center;
width:12%;
/* blueeee */

color: #ACECFD;
/* blue */

text-shadow: 0px -10px 50px rgba(172, 236, 253, 0.5), 0px 10px 50px rgba(172, 236, 253, 0.5);
transform: rotate(-90deg);
`

export const AddProfileAction = styled.div`
    display:flex;
    justify-content:center;
    color:white;
    font-size:20px;
    padding-top:25px;
`

export const AddProfileBtn = styled.div`
    width: 25px;
    height: 25px;
    display:flex;
    align-items:center;
    justify-content:center;
    margin:10px;
    color:white;
    font-size:20px;
    cursor: pointer;
    // background: #0c0c0c;
    /* pkiouy7 */
    
    box-shadow: -3px -1px 4px rgba(66, 66, 66, 0.41), 4px 4px 9px #0D0D0D;
    border-radius: 100px;
`

export const SaveBtn = styled.button`
  width: 30px;
  height: 30px;
  display:flex;
  align-items:center;
  justify-content:center;
  margin:10px;
  color:white;
  font-size:20px;
  cursor: pointer;
  background: #0c0c0c;
  /* pkiouy7 */

  box-shadow: -1px -1px 2px rgba(60,60,60,0.25), 2px 2px 4px black;
  border-radius: 5px;
`

export const CheckBox = styled.input`
    backgroundColor: black;
    width: 30px;
    height: 30px;
    border-bottom: 15px solid red;
    border-top: 15px solid red;
    &:after {
        content: "";
        border-bottom: 15px solid #0c0c0c;
        border-top: 15px solid #0c0c0c;
        display: block;
        opacity: 1;
        box-shadow: -1px -1px 2px rgba(60,60,60,0.25), 2px 2px 4px black
    }
    &:checked:after {
        border-bottom: 0px solid #0c0c0c;
        border-top: 0px solid #0c0c0c;
        display: block;
        opacity: 1;
    }
`
export const CheckBoxContainer = styled.div`
    width: 100%;
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const ModalBg = styled.div`
    width: 100vw;
    height: 100vh;
    z-index: 0;
    background: rgba(0,0,0,0.5);
`
