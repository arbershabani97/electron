import React from "react";
import _ from "lodash";
import {
  AddProfileContainer,
  AddProfileFormContainer,
  AddProfileFormContainerHeader,
  AddProfileFormContainerTitle,
  AddProfileFormContainerIcon,
  AddProfileFormInputContainer,
  AddProfileFormSingleInputRowContainer,
  AddProfileFormInputRowContainer,
  FormBtn,
  SideBar,
  FormCtn,
  InputContainer,
  InputColumnContainer,
  InputAddressContainer,
  InputAptContainer,
  AddProfileAction,
  AddProfileBtn,
  SaveBtn,
  PaymentSaveContainer,
  CheckBoxContainer,
  CheckBox,
  ModalBg,
} from './addProfile.styles';

import clock from '../../assets/images/clock.svg';
import save from '../../assets/images/save.svg';

import Input from "../input/input.component";
import { addRequest, updateRequest } from "../../redux/profile/actions"
import { IProfile } from "../../redux/profile/types"
import { connect } from 'react-redux';
import * as uuid from 'uuid';
import SelectComponent from "../select/select.component";

export interface IAddProfileProps {
  passedFunction: any;
  dismiss: any;
  profile?: any;
}

interface IAddProfileState {
  id: string;
  color: string;
  name: string;
  billingFirstName: string;
  billingLastName: string;
  billingAddress: string;
  billingAptNumber: string;
  billingState: string;
  billingCountry: string;
  billingZipcode: string;
  billingEmail: string;
  billingPhoneNumber: string;
  billingIsSameAsShipping: boolean;
  shippingFirstName: string;
  shippingLastName: string;
  shippingAddress: string;
  shippingAptNumber: string;
  shippingState: string;
  shippingCountry: string;
  shippingZipcode: string;
  shippingEmail: string;
  shippingPhoneNumber: string;
  cardHolderName: string;
  cardNumber: string;
  cvv: string;
  cardType: string;
  expiryMonth: string;
  expiryYear: string;
  email: string;
  isPaymentModal: boolean;
  isBillingModal: boolean;
  isShippingModal: boolean;
  profile: any;
}

interface PropsFromDispatch {
  addRequest: any;
  updateRequest: any
}

type AllProps = PropsFromDispatch & IAddProfileProps;

class AddProfile extends React.Component<AllProps, IAddProfileState> {
  constructor(props: any) {
    super(props);
    let profile = this.props.profile
    this.state = {
      id: _.get(profile, "id", uuid.v4()),
      color: _.get(profile, "color", "card-blue"),
      name: _.get(profile, "name", ""),
      billingFirstName: _.get(profile, "billingInfo.firstName", ""),
      billingLastName: _.get(profile, "billingInfo.lastName", ""),
      billingAddress: _.get(profile, "billingInfo.address", ""),
      billingAptNumber: _.get(profile, "billingInfo.aptNumber", ""),
      billingState: _.get(profile, "billingInfo.state", ""),
      billingCountry: _.get(profile, "billingInfo.country", ""),
      billingZipcode: _.get(profile, "billingInfo.zipcode", ""),
      billingEmail: _.get(profile, "billingInfo.email", ""),
      billingPhoneNumber: _.get(profile, "billingInfo.phoneNumber", ""),
      billingIsSameAsShipping: _.get(profile, "billingInfo.isSameAsShipping", false),
      shippingFirstName: _.get(profile, "shippingInfo.firstName", ""),
      shippingLastName: _.get(profile, "shippingInfo.lastName", ""),
      shippingAddress: _.get(profile, "shippingInfo.address", ""),
      shippingAptNumber: _.get(profile, "shippingInfo.aptNumber", ""),
      shippingState: _.get(profile, "shippingInfo.state", ""),
      shippingCountry: _.get(profile, "shippingInfo.country", ""),
      shippingZipcode: _.get(profile, "shippingInfo.zipcode", ""),
      shippingEmail: _.get(profile, "shippingInfo.email", ""),
      shippingPhoneNumber: _.get(profile, "shippingInfo.phoneNumber", ""),
      cardHolderName: _.get(profile, "paymentInfo.cardHolderName", ""),
      cardNumber: _.get(profile, "paymentInfo.cardNumber", ""),
      cvv: _.get(profile, "paymentInfo.cvv", ""),
      cardType: _.get(profile, "paymentInfo.cardType", ""),
      expiryMonth: _.get(profile, "paymentInfo.expiryMonth", ""),
      expiryYear: _.get(profile, "paymentInfo.expiryYear", ""),
      email: _.get(profile, "paymentInfo.email", ""),
      isPaymentModal: true,
      isBillingModal: false,
      isShippingModal: false,
      profile: profile
    };
  }

  handleSubmit = async (event: any) => {
    event?.preventDefault();
    const {
      id,
      name,
      cardNumber,
      color,
      billingFirstName,
      billingLastName,
      billingAddress,
      billingAptNumber,
      billingState,
      billingCountry,
      billingZipcode,
      billingPhoneNumber,
      billingEmail,
      billingIsSameAsShipping,
      shippingFirstName,
      shippingLastName,
      shippingAddress,
      shippingAptNumber,
      shippingCountry,
      shippingEmail,
      shippingState,
      shippingZipcode,
      shippingPhoneNumber,
      cardType,
      cardHolderName,
      cvv,
      expiryMonth,
      expiryYear,
      email,
    } = this.state;
    const { addRequest, updateRequest, dismiss } = this.props;

    const profile: IProfile = {
      id,
      name,
      color,
      billingInfo: {
        firstName: billingFirstName,
        lastName: billingLastName,
        address: billingAddress,
        aptNumber: billingAptNumber,
        state: billingState,
        country: billingCountry,
        zipcode: billingZipcode,
        email: billingEmail,
        phoneNumber: billingPhoneNumber,
        isSameAsShipping: billingIsSameAsShipping,
      },
      shippingInfo: {
        firstName: shippingFirstName,
        lastName: shippingLastName,
        address: shippingAddress,
        aptNumber: shippingAptNumber,
        state: shippingState,
        country: shippingCountry,
        zipcode: shippingZipcode,
        email: shippingEmail,
        phoneNumber: shippingPhoneNumber,
      },
      paymentInfo: {
        cardHolderName,
        cardNumber,
        cardType,
        cvv,
        expiryMonth,
        expiryYear,
        email
      }
    }
    if (this.state.profile) {
      updateRequest({ profile });
    } else {
      addRequest(profile);
    }

    dismiss();
  };

  handleChange = async (event: any) => {
    this.setState({ [event.target.id]: event.target.value } as Pick<IAddProfileState, keyof IAddProfileState>);
  };

  render() {
    const { passedFunction, dismiss, ...otherProps } = this.props;
    const colors = [{ key: 'card-darkblue', value: 'card-darkblue' }, { key: 'card-main', value: 'card-main' }, { key: 'card-grey', value: 'card-grey' }, { key: 'card-bluegreen', value: 'card-bluegreen' }, { key: 'card-red', value: 'card-red' }, { key: 'card-purple', value: 'card-purple' }, { key: 'card-pink', value: 'card-pink' }, { key: 'card-orange', value: 'card-orange' }, { key: 'card-blue', value: 'card-blue' }]
    const cardTypes = [{key:'Rupay',value:'Rupay'},{key:'MasterCard',value:'MasterCard'},{key:'Visa',value:'Visa'} ]
    return (
      <AddProfileContainer>
        <AddProfileFormContainer onSubmit={this.handleSubmit}>
          <SideBar>
            <div
              onClick={() =>
                this.setState({
                  isPaymentModal: false,
                  isBillingModal: false,
                  isShippingModal: true,
                })
              }
              style={{
                // marginRight: "50px",
                color: `${this.state.isShippingModal ? '#ACECFD' : 'white'}`,
                cursor: 'pointer',
              }}
              className=""
            >
              Shipping
            </div>
            <div
              onClick={() =>
                this.setState({
                  isPaymentModal: false,
                  isBillingModal: true,
                  isShippingModal: false,
                })
              }
              style={{
                marginLeft: '70px',
                color: `${this.state.isBillingModal ? '#ACECFD' : 'white'}`,
                cursor: 'pointer',
              }}
              className=""
            >
              Billing
            </div>
            <div
              onClick={() =>
                this.setState({
                  isPaymentModal: true,
                  isBillingModal: false,
                  isShippingModal: false,
                })
              }
              style={{
                marginLeft: '70px',
                color: `${this.state.isPaymentModal ? '#ACECFD' : 'white'}`,
                cursor: 'pointer',
              }}
              className=""
            >
              Payment
            </div>
          </SideBar>
          <FormCtn>
            {this.state.isShippingModal && (
              <div>
                <AddProfileFormInputRowContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingFirstName"
                      value={this.state.shippingFirstName}
                      placeholder="First Name"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingLastName"
                      value={this.state.shippingLastName}
                      placeholder="Last Name"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer>
                  <InputAddressContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="shippingAddress"
                      value={this.state.shippingAddress}
                      placeholder="Address"
                    />
                  </InputAddressContainer>
                  <InputAptContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="shippingAptNumber"
                      value={this.state.shippingAptNumber}
                      placeholder="Apt."
                    />
                  </InputAptContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingState"
                      value={this.state.shippingState}
                      placeholder="State"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingCountry"
                      value={this.state.shippingCountry}
                      placeholder="Country"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer style={{ width: '100%' }}>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingZipcode"
                      value={this.state.shippingZipcode}
                      placeholder="Zipcode"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingEmail"
                      value={this.state.shippingEmail}
                      placeholder="Email"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer style={{ width: '100%' }}>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingPhoneNumber"
                      value={this.state.shippingPhoneNumber}
                      placeholder="Phone Number"
                    />
                  </InputContainer>
                  <InputContainer style={{ "width": "44%", marginTop: '10px' }}>
                    <SelectComponent
                      placeholder={"select option"}
                      options={colors}
                      onChange={this.handleChange}
                      id="color"
                      value={this.state.color}
                      keyName={"key"}
                      valueName={"value"}
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormSingleInputRowContainer style={{ width: '100%' }}>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="name"
                      value={this.state.name}
                      placeholder="Profile Name"
                    />
                  </InputContainer>
                </AddProfileFormSingleInputRowContainer>
              </div>
            )}
            {this.state.isBillingModal && (
              <div>
                <AddProfileFormInputRowContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingFirstName"
                      value={this.state.billingFirstName}
                      placeholder="First Name"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingLastName"
                      value={this.state.billingLastName}
                      placeholder="Last Name"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer>
                  <InputAddressContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="billingAddress"
                      value={this.state.billingAddress}
                      placeholder="Address"
                    />
                  </InputAddressContainer>
                  <InputAptContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="billingAptNumber"
                      value={this.state.billingAptNumber}
                      placeholder="Apt."
                    />
                  </InputAptContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingState"
                      value={this.state.billingState}
                      placeholder="State"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingCountry"
                      value={this.state.billingCountry}
                      placeholder="country"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer style={{ width: '100%' }}>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingZipcode"
                      value={this.state.billingZipcode}
                      placeholder="Zipcode"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingEmail"
                      value={this.state.billingEmail}
                      placeholder="Email"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormSingleInputRowContainer style={{ width: '100%' }}>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="billingPhoneNumber"
                      value={this.state.billingPhoneNumber}
                      placeholder="Phone Number"
                    />
                  </InputContainer>
                </AddProfileFormSingleInputRowContainer>
                <AddProfileFormSingleInputRowContainer style={{ width: '100%' }}>
                  <CheckBoxContainer>
                    <CheckBox
                      onChange={() =>
                        this.setState({
                          billingIsSameAsShipping: !this.state.billingIsSameAsShipping,
                        })
                      }
                      checked={!!this.state.billingIsSameAsShipping}
                      type="checkbox"
                      id="same"
                    />
                    <div style={{ fontSize: '12px', color: '#818181', marginLeft: '20px' }}>Same as Shipping</div>
                  </CheckBoxContainer>
                </AddProfileFormSingleInputRowContainer>
              </div>
            )}
            {this.state.isPaymentModal && (
              <div>
                <AddProfileFormInputRowContainer>
                  <InputAddressContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="cardHolderName"
                      value={this.state.cardHolderName}
                      placeholder="Cardholder Name"
                    />
                  </InputAddressContainer>
                  <InputContainer style={{ "width": "44%", marginTop: '10px' }}>
                    <SelectComponent
                      placeholder={"select option"}
                      options={cardTypes}
                      onChange={this.handleChange}
                      id="cardType"
                      value={this.state.cardType}
                      keyName={"key"}
                      valueName={"value"}
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer>
                  <InputAddressContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="cardNumber"
                      value={this.state.cardNumber}
                      placeholder="Card Number"
                    />
                  </InputAddressContainer>
                  <InputAptContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="cvv"
                      value={this.state.cvv}
                      placeholder="CVV"
                    />
                  </InputAptContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="expiryMonth"
                      value={this.state.expiryMonth}
                      placeholder="Exp.Month"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="expiryYear"
                      value={this.state.expiryYear}
                      placeholder="Exp.Year"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormInputRowContainer style={{ width: '100%' }}>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingZipcode"
                      value={this.state.shippingZipcode}
                      placeholder="Zipcode"
                    />
                  </InputContainer>
                  <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChange}
                      type="text"
                      id="shippingEmail"
                      value={this.state.shippingEmail}
                      placeholder="Email"
                    />
                  </InputContainer>
                </AddProfileFormInputRowContainer>
                <AddProfileFormSingleInputRowContainer style={{ width: '100%', marginTop: '60px' }}>
                  <PaymentSaveContainer>
                    <SaveBtn type="submit">
                      <img src={save} className="App-logo" alt="icon" width="40" height="40" />
                    </SaveBtn>
                  </PaymentSaveContainer>
                </AddProfileFormSingleInputRowContainer>
              </div>
            )}
          </FormCtn>
        </AddProfileFormContainer>
        <ModalBg onClick={dismiss} />
      </AddProfileContainer>
    );
  }
}

const mapDispatchToProps = {
  addRequest,
  updateRequest
};

const mapStateToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(AddProfile);
