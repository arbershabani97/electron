import React from 'react';
import _ from 'lodash';
import {
  NewTaskContainer,
  NewTaskFormContainer,
  NewTaskFormContainerHeader,
  NewTaskFormContainerTitle,
  NewTaskFormContainerIcon,
  NewTaskFormInputContainer,
  NewTaskFormInputRowContainer,
  FormBtn,
  SideBar,
  FormCtn,
  InputContainer,
  InputColumnContainer,
  NewTaskAction,
  NewTaskBtn,
  ModalBg,
} from './newTask.styles';

import clock from '../../assets/images/clock.svg';
import save from '../../assets/images/save.svg';

import Input from '../input/input.component';
import SelectComponent from '../select/select.component';
import { addRequest } from '../../redux/task/actions';
import { IProfile } from '../../redux/profile/types';
import { ITask } from '../../redux/task/types';

import { connect } from 'react-redux';
import { ApplicationState } from "../../redux";
import * as uuid from "uuid";
import { fetchRequest } from "../../redux/profile/actions";
import * as storeData from './../../assets/store.json';

export interface INewTaskProps {
  passedFunction: any;
  dismiss: any;
  addRequest: any;
  proxies: any;
  taskId?: string;
  profiles: any;
  accounts: any;
  fetchProfile?: any;
}

interface INewTaskState {
  id: string;
  store: string;
  proxy: any;
  monitorInput: string;
  profile: string;
  account: any;
  size: string;
  rate: string;
  status: string;
  taskCount: number;
  isAdvance: boolean;
  shippingRate: string;
  name: string;
  errorMax: any;
  errorMin: any;
  monitorMax: any;
  monitorMin: any;
  rateMin: any;
  rateMax: any;
  productName?: any;
}

class NewTask extends React.Component<INewTaskProps, INewTaskState> {
  constructor(props: any) {
    super(props);
    const task = props.taskId ? props.getTask(props.taskId) : null;

    this.state = {
      id: _.get(task, 'id', ''),
      store: _.get(task, 'store.shortCode', ''),
      proxy: _.get(task, 'proxy', {}),
      monitorInput: _.get(task, 'monitorInput', ''),
      profile: _.get(task, 'profile.id', ''),
      account: _.get(task, 'account', {}),
      size: _.get(task, 'size', ''),
      rate: _.get(task, 'rate', ''),
      status: _.get(task, 'status', ''),
      taskCount: _.get(task, 'taskCount', 1),
      isAdvance: false,
      shippingRate: _.get(task, 'advanced.rates.shipping', ''),
      name: _.get(task, 'advanced.rates.profile', ''),
      errorMax: _.get(task, 'advanced.delays.error.max', ''),
      errorMin: _.get(task, 'advanced.delays.error.min', ''),
      monitorMax: _.get(task, 'advanced.delays.monitor.max', ''),
      monitorMin: _.get(task, 'advanced.delays.monitor.min', ''),
      rateMax: _.get(task, 'advanced.rates.max', ''),
      rateMin: _.get(task, 'advanced.rates.min', ''),
    };
  }

  createAdvancedTask() {
    //TO_DO id creation
    let task = {
      id: uuid.v4(),
      store: this.state.store,
      profile: this.state.account,
      shippingRate: this.state.shippingRate,
      name: this.state.name,
      errorMax: this.state.errorMax,
      errorMin: this.state.errorMin,
      monitorMax: this.state.monitorMax,
      monitorMin: this.state.monitorMin,
    };
    if (this.props.taskId) {
      let updatedTask = { ...task, id: this.props.taskId };
      this.props.passedFunction(updatedTask, this.props.taskId);
    } else {
      this.props.passedFunction(task);
    }
  }

  getFirst(data: any[] = []) {
    if (data?.length) {
      return data[0];
    }
    return {};
  } 

  handleSubmit() {
    const task: ITask = {
      id: uuid.v4(),
      store: this.getFirst(storeData.lookups.storeList.filter((store: any) => store.shortCode === this.state.store)),
      proxy: this.state.proxy,
      monitorInput: this.state.monitorInput,
      profile: this.getFirst(this.props.profiles.filter((profile: IProfile) => profile.id === this.state.profile)),
      account: this.state.account,
      size: this.state.size,
      rate: this.state.rate,
      status: this.state.status,
      advanced: {
        delays: {
          error: {
            min: this.state.errorMin,
            max: this.state.errorMax,
          },
          monitor: {
            min: this.state.monitorMin,
            max: this.state.monitorMax,
          },
        },
        rates: {
          min: this.state.rateMin,
          max: this.state.rateMax,
          shipping: this.state.shippingRate,
          profile: this.state.profile,
        },
      },
      computed: {
        productName: this.state.productName,
        taskCount: this.state.taskCount,
      },
    };
    if (this.props.taskId) {
      let updatedTask = { ...task, id: this.props.taskId };
      this.props.passedFunction(updatedTask, this.props.taskId);
    } else {
      this.props.passedFunction(task);
    }
  }

  // handleChangle = async (event: React.KeyboardEvent) => {
  //   //TO_DO change function
  //   console.log(event);
  // };

  handleChange = (event: any) => {
    this.setState({ [event.target.id]: event.target.value } as Pick<INewTaskState, keyof INewTaskState>);
  };

  render() {
    const { passedFunction, dismiss, profiles, ...otherProps } = this.props;

    // TO_DO get realdata and change
    const sizes = [{ key: "1", value: "1" }, { key: "2", value: "2" }]
    const rates = [{ key: "200", value: "200" }, { key: "300", value: "300" }]

    return (
      <NewTaskContainer>
        <NewTaskFormContainer onSubmit={this.handleSubmit}>
          <SideBar>
            <div
              onClick={() =>
                this.setState({
                  isAdvance: true,
                })
              }
              style={{
                marginRight: '50px',
                color: `${this.state.isAdvance ? '#ACECFD' : 'white'}`,
                cursor: 'pointer',
              }}
              className=""
            >
              Advanced
            </div>
            <div
              onClick={() =>
                this.setState({
                  isAdvance: false,
                })
              }
              style={{
                marginLeft: '70px',
                color: `${this.state.isAdvance ? 'white' : '#ACECFD'}`,
                cursor: 'pointer',
              }}
              className=""
            >
              General
            </div>
          </SideBar>
          <FormCtn>
            {!this.state.isAdvance ? (
              <div>
                <NewTaskFormContainerTitle>
                  Create New Task
                </NewTaskFormContainerTitle>
                <NewTaskFormInputRowContainer>
                  <InputContainer>
                    <label htmlFor="store">Store</label>
                  </InputContainer>
                  <InputContainer>
                    <label htmlFor="Proxies">Proxies</label>
                  </InputContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskFormInputRowContainer>
                  <InputContainer style={{ width: '45%' }}>
                    <SelectComponent
                      id="store"
                      value={this.state.store}
                      keyName={'shortCode'}
                      valueName={'longCode'}
                      onChange={this.handleChange}
                      placeholder={'select option'}
                      options={storeData.lookups.storeList}>
                    </SelectComponent>
                  </InputContainer>
                  <InputContainer style={{ width: '45%' }}>
                    {/* <label htmlFor="Proxies">Proxies</label> */}
                    <SelectComponent
                      id="proxy"
                      value={this.state.proxy}
                      onChange={this.handleChange}
                      placeholder={'select option'}
                      keyName={'groupId'}
                      valueName={'groupName'}
                      options={this.props.proxies}>
                    </SelectComponent>
                  </InputContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskFormInputRowContainer>
                  <InputColumnContainer>
                    <label style={{ textAlign: 'center' }} htmlFor="">
                      Keywords
                    </label>
                    <Input
                      grow={true}
                      handleChange={this.handleChange}
                      type="keywords"
                      id="monitorInput"
                      value={this.state.monitorInput}
                      placeholder=""
                    />
                  </InputColumnContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskFormInputRowContainer>
                  <InputContainer>
                    <label htmlFor="store">Profies</label>
                  </InputContainer>
                  <InputContainer>
                    <label htmlFor="store">Accounts</label>
                  </InputContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskFormInputRowContainer>
                  <InputContainer style={{ width: '45%' }}>
                    <SelectComponent
                      id="profile"
                      value={this.state.profile}
                      placeholder={"select option"}
                      options={this.props.profiles}
                      onChange={this.handleChange}
                      keyName={"id"}
                      valueName={"name"}
                    >
                    </SelectComponent>
                  </InputContainer>
                  <InputContainer style={{ width: '45%' }}>
                    {/* <label htmlFor="Proxies">Proxies</label> */}
                    <SelectComponent
                      placeholder={"select option"}
                      options={this.props.accounts}
                      onChange={this.handleChange}
                      id="account"
                      keyName={"email"}
                      valueName={"name"}
                      value={this.state.account}
                    ></SelectComponent>
                  </InputContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskFormInputRowContainer>
                  <InputContainer>
                    <label style={{ textAlign: 'center' }} htmlFor="">
                      Sizes
                    </label>
                  </InputContainer>
                  <InputContainer>
                    <label style={{ textAlign: 'center' }} htmlFor="">
                      Rates
                    </label>
                  </InputContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskFormInputRowContainer  >
                  <InputContainer style={{ "width": "45%" }}>
                    <Input
                      grow={true}
                      placeholder=""
                      handleChange={this.handleChange}
                      type="keywords"
                      id="size"
                      value={this.state.size}
                    />
                  </InputContainer>
                  <InputContainer style={{ width: '45%' }}>
                    {/* <label htmlFor="Proxies">Proxies</label> */}
                    <SelectComponent
                      placeholder={'select option'}
                      options={rates}
                      onChange={this.handleChange}
                      type="keywords"
                      id="rate"
                      keyName={'key'}
                      valueName={'value'}
                      value={this.state.rate}>
                    </SelectComponent>
                  </InputContainer>
                </NewTaskFormInputRowContainer>
                <NewTaskAction>
                  <NewTaskBtn>
                    <img
                      src={clock}
                      className="App-logo"
                      alt="icon"
                      width="47"
                      height="47"
                    />
                  </NewTaskBtn>
                  {!this.props.taskId ? (
                    <>
                      <NewTaskBtn
                        onClick={() => {
                          if (this.state.taskCount == 0) {
                            return;
                          }
                          this.setState({
                            taskCount: this.state.taskCount - 1,
                          });
                        }}
                      >
                        <div className="">-</div>
                      </NewTaskBtn>
                      <div style={{ paddingTop: '8px' }}>{this.state.taskCount}</div>
                      <NewTaskBtn
                        onClick={() =>
                          this.setState({
                            taskCount: this.state.taskCount + 1,
                          })
                        }
                      >
                        <div className="">+</div>
                      </NewTaskBtn>
                    </>
                  ) : (
                    <></>
                  )}
                  <NewTaskBtn>
                    <img
                      src={save}
                      className="App-logo"
                      alt="icon"
                      width="42"
                      height="42"
                      onClick={() => this.handleSubmit()}
                    />
                  </NewTaskBtn>
                </NewTaskAction>
              </div>
            ) : (
                <div>
                  <NewTaskFormContainerTitle>Delays</NewTaskFormContainerTitle>
                  <NewTaskFormInputRowContainer>
                    <InputContainer>
                      <label htmlFor="errorMin">Error</label>
                    </InputContainer>
                    <InputContainer>
                      <label htmlFor="monitorMin">Monitor</label>
                    </InputContainer>
                  </NewTaskFormInputRowContainer>
                  <NewTaskFormInputRowContainer>
                    <InputContainer>
                      {/* <label htmlFor="errorMin">Error</label> */}
                      <Input
                        grow={false}
                        handleChange={this.handleChange}
                        type="text"
                        id="errorMin"
                        value={this.state.errorMin}
                        placeholder="min"
                      />
                    </InputContainer>
                    <InputContainer>
                      <Input
                        grow={false}
                        handleChange={this.handleChange}
                        type="text"
                        id="monitorMin"
                        value={this.state.monitorMin}
                        placeholder="min"
                      />
                    </InputContainer>
                  </NewTaskFormInputRowContainer>
                  <NewTaskFormInputRowContainer>
                    <InputContainer>
                      <Input
                        grow={false}
                        handleChange={this.handleChange}
                        type="text"
                        id="errorMax"
                        value={this.state.errorMax}
                        placeholder="max"
                      />
                    </InputContainer>
                    <InputContainer>
                      <Input
                        grow={false}
                        handleChange={this.handleChange}
                        type="text"
                        id="monitorMax"
                        value={this.state.monitorMax}
                        placeholder="max"
                      />
                    </InputContainer>
                  </NewTaskFormInputRowContainer>
                  <NewTaskFormContainerTitle style={{ paddingTop: '30px' }}>Rates</NewTaskFormContainerTitle>
                  <NewTaskFormInputRowContainer>
                    <InputContainer style={{ "width": "44%" }}>
                      <SelectComponent
                        id="profile"
                        value={this.state.profile}
                        placeholder={"select option"}
                        options={this.props.profiles}
                        onChange={this.handleChange}
                        keyName={"id"}
                        valueName={"name"}
                      >
                      </SelectComponent>
                    </InputContainer>
                    <InputContainer style={{ "width": "44%" }}>
                      {/* <label htmlFor="Proxies">Proxies</label> */}
                      <SelectComponent
                        placeholder={"select option"}
                        options={this.props.accounts}
                        onChange={this.handleChange}
                        id="account"
                        value={this.state.account}
                        keyName={"email"}
                        valueName={"name"}
                      >
                      </SelectComponent>
                    </InputContainer>
                  </NewTaskFormInputRowContainer>
                  <NewTaskFormInputRowContainer>
                    <InputColumnContainer>
                      <Input
                        grow={true}
                        handleChange={this.handleChange}
                        type="shippingRate"
                        id="shippingRate"
                        value={this.state.shippingRate}
                        placeholder="Shipping Rate"
                      />
                    </InputColumnContainer>
                  </NewTaskFormInputRowContainer>
                  <NewTaskFormInputRowContainer>
                    <InputContainer>
                      <Input
                        grow={false}
                        handleChange={this.handleChange}
                        type="text"
                        id="profileName"
                        value={this.state.name}
                        placeholder="Profile Name"
                      />
                    </InputContainer>
                    <span style={{ marginTop: '-15px' }} >
                      <NewTaskBtn>
                        <img
                          src={save}
                          className="App-logo"
                          alt="icon"
                          width="42"
                          height="42"
                          onClick={() => this.createAdvancedTask()}
                        />
                      </NewTaskBtn>
                    </span>

                  {/* <InputContainer>
                    <Input
                      grow={false}
                      handleChange={this.handleChangle}
                      type="text"
                      id="rates"
                      value={this.state.store}
                      placeholder="Rates"
                    />
                  </InputContainer> */}
                </NewTaskFormInputRowContainer>
              </div>
            )}
          </FormCtn>
        </NewTaskFormContainer>
        <ModalBg onClick={dismiss} />
      </NewTaskContainer>
    );
  }
}

const mapStateToProps = ({ proxies, tasks, profiles, accounts }: ApplicationState) => ({
  proxies: proxies.proxies,
  profiles: profiles.data,
  accounts: accounts.accounts,
  getTask: (taskId: string) => tasks.data.find((task: any) => task.id === taskId)
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    addRequest: (task: any) => dispatch(addRequest(task)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewTask);
