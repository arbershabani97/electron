import React from 'react';
import {
  NewTaskContainer,
  NewTaskFormContainer,
  NewTaskFormContainerHeader,
  NewTaskFormContainerTitle,
  NewTaskFormContainerIcon,
  NewTaskFormInputContainer,
  NewTaskFormInputRowContainer,
  FormBtn,
  SideBar,
  FormCtn,
  InputContainer,
  InputColumnContainer,
  NewTaskAction,
  NewTaskBtn,
  ModalBg,
} from './addEditAccount.styles';

import clock from '../../assets/images/clock.svg';
import save from '../../assets/images/save.svg';

import Input from '../input/input.component';
import SelectComponent from '../select/select.component';
import * as storeData from '../../assets/store.json';

export interface AddEditAccountProps {
  onAction: any;
  dismiss: any;
}

interface AddEditAccountState {
  id: string;
  store: any;
  email: string;
  password: string;
  name: string;
}

// TODO: Use store list from assets
class AddEditAccount extends React.Component<AddEditAccountProps, AddEditAccountState> {
  constructor(props: any) {
    super(props);
    this.state = {
      id: '',
      store: '',
      email: '',
      password: '',
      name: '',
    };
  }

  getFirst(data: any[] = []) {
    if (data?.length) {
      return data[0];
    }
    return {};
  }

  handleChange = (event: any) => {
    this.setState({ [event.target.id]: event.target.value } as Pick<AddEditAccountState, keyof AddEditAccountState>);
  };

  preSubmitHook = (account: any) => {
    account.store = this.getFirst(storeData.lookups.storeList.filter(item => item.shortCode === account.store));
    return account;
  }

  render() {
    const { onAction, dismiss, ...otherProps } = this.props;
    const account = {
      store: this.state.store,
      email: this.state.email,
      password: this.state.password,
      name: this.state.name,
    };

    return (
      <NewTaskContainer>
        <NewTaskFormContainer>
          <SideBar>
            <div
              style={{
                marginLeft: '70px',
                color: '#ACECFD',
              }}
              className=""
            >
              Account
            </div>
          </SideBar>
          <FormCtn>
            <div>
              <NewTaskFormContainerTitle>Create New Account</NewTaskFormContainerTitle>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <label htmlFor="store">Store</label>
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <SelectComponent
                    id="store"
                    placeholder={'select option'}
                    options={storeData.lookups.storeList}
                    value={this.state.store}
                    onChange={this.handleChange}
                    className="mb-6"
                    keyName={"shortCode"}
                    valueName={"longCode"}
                  ></SelectComponent>
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <label htmlFor="email">Email</label>
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <Input
                    grow={false}
                    handleChange={this.handleChange}
                    type="email"
                    id="email"
                    value={this.state.email}
                    placeholder=""
                  />
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <label htmlFor="password">Password</label>
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <Input
                    grow={false}
                    handleChange={this.handleChange}
                    type="text"
                    id="password"
                    value={this.state.password}
                    placeholder=""
                  />
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  <label htmlFor="store">Account Name</label>
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskFormInputRowContainer>
                <InputContainer>
                  {/* <label htmlFor="store">Accounts</label> */}
                  <Input
                    grow={false}
                    handleChange={this.handleChange}
                    type="text"
                    id="name"
                    value={this.state.name}
                    placeholder=""
                  />
                </InputContainer>
              </NewTaskFormInputRowContainer>
              <NewTaskAction>
                <NewTaskBtn>
                  <img
                    src={save}
                    className="App-logo"
                    alt="icon"
                    width="84"
                    height="84"
                    onClick={() => onAction(this.preSubmitHook(account))}
                  />
                </NewTaskBtn>
              </NewTaskAction>
            </div>
          </FormCtn>
        </NewTaskFormContainer>
        <ModalBg onClick={dismiss} />
      </NewTaskContainer>
    );
  }
}

export default AddEditAccount;
