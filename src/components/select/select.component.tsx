import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

const Select = styled.select`
  width: 100%;
  height: 30px;
  background: #0c0c0c;
  -webkit-appearance: none;
  text-align-last: center;
  color: gray;
  padding-left: 5px;
  padding-right: 5px;
  box-shadow: -1px -1px 2px rgba(60, 60, 60, 0.25), 2px 2px 4px black;
  border-radius: 10px;
  font-size: 14px;
  border: none;

  option {
    color: black;
    background: white;
    display: flex;
    white-space: pre;
    min-height: 20px;
    padding: 0px 2px 1px;
  }
`;
const SelectComponent = (props: any) => {
  const { keyName, valueName } = props
  return (
    <Select style={{outline:'none'}} value={props.value} onChange={props.onChange} className={props.className} id={props.id}>
      <option value="" >
        {props.placeholder}
      </option>
      {props.options.map((option: any, i: any) => (
        <option key={option[keyName]} value={option[keyName]} style={{ backgroundColor: 'black', color: 'white' }}>
          {option[valueName]}
        </option>
      ))}
    </Select>
  );
};

export default SelectComponent;
