import React, { Component } from 'react';
import { ModalBg, InputColumnContainer, InputContainer, FormCtn, MultiEditTaskFormContainer, MultiEditTaskContainer, MultiEditTaskFormInputRowContainer, MultiEditTaskBtn, MultiEditTaskAction } from './multiEditTask.styles';
import Input from '../input/input.component';
import SelectComponent from '../select/select.component';
import save from "../../assets/images/save.svg";

interface IMultiEditState {
    keywords: string;
    size: string;
}

interface IMultiEditProps {
    dismiss: any;
    passedFunction: any;
}

class MultiEditTask extends React.Component<IMultiEditProps, IMultiEditState>  {
    constructor(props: any) {
        super(props);

        this.state = {
            keywords: '',
            size: ''
        }
    }
    handleChange = (event: any) => {
        this.setState({ [event.target.id]: event.target.value } as Pick<IMultiEditState, keyof IMultiEditState>);

    }

    multiUpdate = () => {
        this.props.passedFunction(this.state);
    }

    render() {
        const { dismiss, passedFunction } = this.props;
        const sizes = [{ key: "1", value: "1" }, { key: "2", value: "2" }]

        return (
            <MultiEditTaskContainer>
                <MultiEditTaskFormContainer>
                    <FormCtn>
                        <div>
                            <MultiEditTaskFormInputRowContainer>
                                <InputColumnContainer>
                                    <label style={{ textAlign: "center" }} htmlFor="">
                                        Keywords
                                    </label>
                                    <Input
                                        grow={true}
                                        handleChange={this.handleChange}
                                        type="keywords"
                                        id="keywords"
                                        value={this.state.keywords}
                                        placeholder=""
                                    />
                                </InputColumnContainer>
                            </MultiEditTaskFormInputRowContainer>
                            <MultiEditTaskFormInputRowContainer>
                                <InputColumnContainer style={{ "width": "45%" }}>
                                    <label style={{ textAlign: "center" }} htmlFor="">
                                        Size
                                  </label>
                                    <Input
                                        grow ={true}
                                        placeholder="size"
                                        type ="text"
                                        handleChange={this.handleChange}
                                        id="size"
                                        value={this.state.size}
                                    />
                                </InputColumnContainer>
                            </MultiEditTaskFormInputRowContainer>
                        </div>
                        <MultiEditTaskAction>
                            <MultiEditTaskBtn>
                                <img
                                    src={save}
                                    className="App-logo"
                                    alt="icon"
                                    width="42"
                                    height="42"
                                    onClick={() => this.multiUpdate()}
                                />
                            </MultiEditTaskBtn>
                        </MultiEditTaskAction>
                    </FormCtn>
                </MultiEditTaskFormContainer>
                <ModalBg onClick={dismiss} />
            </MultiEditTaskContainer>
        );
    }
}

export default MultiEditTask;