import styled from "styled-components";

export const MultiEditTaskContainer = styled.div`
    width: 100vw;
    height:100vh;
    background: rgba(0, 0, 0, 0.5);
    display:flex;
    justify-content:center;
    align-items:center;

`;

export const MultiEditTaskFormContainer = styled.form`
    // background-color:#0c0c0c;
    width:35%;
    height:35%;
    position: absolute;
    z-index: 1;
    diaplay:flex;
    background: #0C0C0C;
    box-shadow: 0px -5px 15px rgba(172, 236, 253, 0.5), 0px 5px 15px rgba(172, 236, 253, 0.5);
    border-radius: 15px;
    display:flex;

`;
export const MultiEditFormContainerHeader = styled.form`
    color:white;
    // width:30%;
    height:10%;
    display:flex;
    padding: 10px;
    
`;


export const InputColumnContainer = styled.div`
    display:flex;
    color: #BCBCBC;
    // justify-content:center;
    // align-items:center;
    flex-direction:column;
    // background:red;
    width: 100%;
    // padding: 0px 0px 0 10px;
    font-size: 14px;
    
`;

export const InputContainer = styled.div`
    display: flex;
    justify-content: center;
    width: 50%;
    text-align:center;
    color: #BCBCBC;
    font-size:14px;
    margin: 0 auto;
    margin-bottom:1em;
    // padding: 7px 0px 10px 0px;
`;

export const MultiEditTaskFormInputRowContainer = styled.form`
    display:flex;
    justify-content: space-around;
    align-items: center;
    color:red;
    width:100%;
    height:80;
    color: #0c0c0c;
`
    ;

export const FormBtn = styled.div`
width:80%;
//    margin-bottom:10px;
`
    ;

export const FormCtn = styled.div`

    width:100%;
    height:100%;
    padding:1000px
   display:flex;
   padding-top:20px;
   flex-direction:column;
   justify-content:sapce-around;
   align-items:center;
   box-shadow: 0px 0px 10px 0px rgba(172, 236, 253, 0.5);
   border-radius: 0px 20px 20px 0px;
    z-index: 1;
`;
export const SideBar = styled.div`
display:flex;
justify-content:space-evenly;
height:100%;
align-items: center;
width:12%;
/* blueeee */

color: #ACECFD;
/* blue */

text-shadow: 0px -10px 50px rgba(172, 236, 253, 0.5), 0px 10px 50px rgba(172, 236, 253, 0.5);
transform: rotate(-90deg);`;


export const MultiEditTaskAction = styled.div`
    display:flex;
    justify-content:center;
    color:white;
    font-size:20px;
    padding-top:25px;
`;

export const MultiEditTaskBtn = styled.div`
    width: 25px;
    height: 25px;
    display:flex;
    align-items:center;
    justify-content:center;
    margin:10px;
    color:white;
    font-size:20px;
    cursor: pointer;
    // background: #0c0c0c;
    /* pkiouy7 */
    
    box-shadow: -3px -1px 4px rgba(66, 66, 66, 0.41), 4px 4px 9px #0D0D0D;
    border-radius: 100px;
`;

export const ModalBg = styled.div`
    width: 100vw;
    height: 100vh;
    z-index: 0;
    background: rgba(0,0,0,0.5);
`