import styled from "styled-components";

export const ProfilesLeft = styled.div`
  display: flex;
  flex-direction: column;
  width:70px;
  justify-content: flex-end;
  align-items: flex-end;
  padding-right:0px;

  .user {
    color: #acecfd;
    transform: rotate(-90deg);
    margin-bottom: 25px;
    font-size: 16px;
    letter-spacing: 1px;
  }
`;