import * as React from 'react';
import { useState, useEffect } from 'react';
import styled, { css, StyledFunction } from 'styled-components';


interface CalanderBoolean {
    isToday: boolean,
    isSelected: boolean
}

const Frame = styled.div`
  width: 99%;
  background: #0C0C0C;
  box-shadow: -1px -1px 2px rgba(50, 50, 50, 0.38), 1px 1px 2px #000000;
  border-radius: 10px;
  margin-bottom:10px;
  padding: 15px;
`;

const Header = styled.div`
  font-size: 15px;
  font-weight: bold;
  padding: 5px 10px 5px 10px;
  display: flex;
  justify-content: space-between;
  background-color: #0C0C0C;
`;

const Button = styled.div`
  cursor: pointer;
`;

const Body = styled.div`
  width: 100%;
  font-size: 13px;
  display: flex;
  flex-wrap: wrap;
`;

const Right = styled.div`
    
    border-right:4px solid #fff;
    display: inline-block;
    height: 20px;
    width: 20px;
    border-bottom-right-radius: 40px;
    border-top-right-radius: 40px;
  `
const Left = styled.div`
    border-left: 2px solid #fff;
    display: inline-block;
    height: 20px;
    width: 20px;
    border-bottom-left-radius: 40px;
    border-top-left-radius: 40px;
  `

const Day = styled.div<CalanderBoolean>`
  width: 14.2%;
  height: 3vh;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  ${props =>
        props.isToday &&
        css`
      border: 1px solid #acecfd;
      border-radius: 50%;
    `}

  ${props =>
        props.isSelected &&
        css`
      background-color: #acecfd;
      border-radius: 50%;
      color:#0e0e0e
    `}
`;

function Calendar() {
    const DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const DAYS_LEAP = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const DAYS_OF_THE_WEEK = ['M', 'T', 'W', 'T', 'F', 'SA', 'SU'];
    const MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

    const today = new Date();
    const [date, setDate] = useState(today);
    const [day, setDay] = useState(date.getDate());
    const [month, setMonth] = useState(date.getMonth());
    const [year, setYear] = useState(date.getFullYear());
    const [startDay, setStartDay] = useState(getStartDayOfMonth(date));

    useEffect(() => {
        setDay(date.getDate());
        setMonth(date.getMonth());
        setYear(date.getFullYear());
        setStartDay(getStartDayOfMonth(date));
    }, [date]);

    function getStartDayOfMonth(date: Date) {
        return new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    }

    function isLeapYear(year: number) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    const days = isLeapYear(date.getFullYear()) ? DAYS_LEAP : DAYS;

    return (
        <Frame>
            <Header>
                <Button onClick={() => setDate(new Date(year, month - 1, day))}><Left></Left></Button>
                <div>
                    {MONTHS[month]} {year}
                </div>
                <Button onClick={() => setDate(new Date(year, month + 1, day))}><Right></Right></Button>
            </Header>
            <Body>
                {DAYS_OF_THE_WEEK.map(d => (
                    <Day key={d}
                        isToday={false}
                        isSelected={false}>

                        <strong>{d}</strong>
                    </Day>
                ))}
                {Array(days[month] + (startDay - 1))
                    .fill(null)
                    .map((_, index) => {
                        const d = index - (startDay - 2);
                        return (
                            <Day
                                key={index}
                                isToday={d === today.getDate()}
                                isSelected={d === day}
                                onClick={() => setDate(new Date(year, month, d))}
                            >
                                {d > 0 ? d : ''}
                            </Day>
                        );
                    })}
            </Body>
        </Frame>
    );
}

export default Calendar;

