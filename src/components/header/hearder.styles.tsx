import styled from "styled-components";

export const HeaderContainer=styled.div`
    display:flex;
    align-items:center;
    width:100vw;
    margin-top:-65px;
    margin-bottom:-19px;
    padding: 14px 0px;
    -webkit-user-select: none;
    -webkit-app-region: drag;
`;
export const HeaderIconsContainer=styled.div`
    width:75vw;
    display:flex;
    justify-content:flex-start;
    margin-left:-90px;
    align-items:center;
    padding-left:60px;
    // background:red;
`;

export const HeaderIconContainer=styled.div`
    margin-left:60px;
`;

export const HeaderText=styled.div`
// background-color:yellow;
    border-left: 2px solid #ACECFD;
    font-family: 'Montserrat', sans-serif;font-style: normal;
    font-weight: normal;
    font-size: 22px;
    display: flex;
    align-items: center;
    height:35px;
    margin-right:4vw;
    padding-left:17px;
    color: #ACECFD;
    box-shadow: -8px px 0px rgba(172, 236, 253, 0.5), 0px -0px 0px rgba(172, 236, 253, 0.5);
    `;
    

export const LogoContainer=styled.div`
    display:flex;
    justify-content:space-evenly;
    width:10vw;
    margin-left:-10px;
    -webkit-app-region: drag;
    cursor:pointer;
`;

export const UpdateIconContainer=styled.div`
    margin: 0px 0px;
`
;
export const UpdateContainer=styled.div`
        display: flex;
        // bakground-color:red;
        align-items:center;
`;


export const UpdateIconItemsContainer=styled.div`
    display:flex;
    // background:red;
    width:200px;
    justify-content:flex-end;
    margin-right:10px;
`
;