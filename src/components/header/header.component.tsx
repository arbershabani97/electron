import React from 'react';
import {
  HeaderContainer,
  HeaderIconsContainer,
  HeaderIconContainer,
  HeaderText,
  LogoContainer,
  UpdateContainer,
  UpdateIconContainer,
  UpdateIconItemsContainer,
} from './hearder.styles';
import Logo from '../../assets/images/logo.svg';
import Home from '../../assets/images/home_icon.svg';
import Globe from '../../assets/images/globe_icon.svg';
import Users from '../../assets/images/users_icon.svg';
import Profile from '../../assets/images/profile_icon.svg';
import Graph from '../../assets/images/graph_icon.svg';
import Setting from '../../assets/images/setting_icon.svg';
import up from '../../assets/images/up.svg';
import down from '../../assets/images/down.svg';
import reload from '../../assets/images/reload.svg';
import power from '../../assets/images/power.svg';
import minus from '../../assets/images/minus.svg';

import { Link } from 'react-router-dom';

const electron = window.require('electron');
const ipcRenderer  = electron.ipcRenderer;


const Header = () => (
  <HeaderContainer onDoubleClick={()=>{
    ipcRenderer.send('windowSizeToggle', 'toggle');
  }}>
    <LogoContainer>
      <img src={Logo} className="App-logo" alt="icon" width="82" height="82" />
    </LogoContainer>
    <HeaderIconsContainer>
      <Link to="/">
        <HeaderIconContainer>
          <img src={Home} className="App-logo" alt="icon" width="18" height="18" />
        </HeaderIconContainer>
      </Link>
      <Link to="/profiles">
        <HeaderIconContainer>
          <img src={Profile} className="App-logo" alt="icon" width="18" height="18" />
        </HeaderIconContainer>
      </Link>
      <Link to="/proxies">
        <HeaderIconContainer>
          <img src={Globe} className="App-logo" alt="icon" width="18" height="18" />
        </HeaderIconContainer>
      </Link>
      <Link to="/analytics">
        <HeaderIconContainer>
          <img src={Graph} className="App-logo" alt="icon" width="18" height="18" />
        </HeaderIconContainer>
      </Link>
      <Link to="/settings">
        <HeaderIconContainer>
          <img src={Setting} className="App-logo" alt="icon" width="18" height="18" />
        </HeaderIconContainer>
      </Link>
    </HeaderIconsContainer>
    <UpdateContainer>
      <UpdateIconItemsContainer>
        <UpdateIconContainer onClick={()=>{ipcRenderer.send('open:captcha')}}>
          <img src={reload} className="App-logo" alt="icon" width="40" height="40"  />
        </UpdateIconContainer>
        <UpdateIconContainer>
          <img src={up} className="App-logo" alt="icon" width="40" height="40" />
        </UpdateIconContainer>
        <UpdateIconContainer>
          <img src={down} className="App-logo" alt="icon" width="40" height="40" />
        </UpdateIconContainer>
      </UpdateIconItemsContainer>
      <HeaderText>Dashboard</HeaderText>
    </UpdateContainer>
    <div
      style={{
        color: 'white',
        display: 'flex',
        justifyContent: 'space-between',
        fontSize: '19px',
        fontWeight: 'bold',
        marginRight: '10px',
        marginBottom: '40px',
        width: '35px',
        cursor: 'pointer',
      }}
      className=""
    >
      <div
        style={{
          cursor: 'pointer',
          paddingTop: '8px',
        }}
        onClick={()=>{
          ipcRenderer.send('hide', 'hide');
        }}
        className=""
      >
        -
      </div>
      <img
        onClick={()=>{
          ipcRenderer.send('close', 'close');
        }}
        style={{
          cursor: 'pointer',
        }}
        src={power}
        className="App-logo"
        alt="icon"
        width="40"
        height="40"
      />{' '}
    </div>
  </HeaderContainer>
);

export default Header;
