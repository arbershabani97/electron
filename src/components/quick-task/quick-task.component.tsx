import * as React from 'react';
import { useState, useEffect } from 'react';
import styled, { css, StyledFunction } from 'styled-components';
import SelectComponent from '../../components/select/select.component';
import {
    SelectGrid,
    IndSelect1,
    IndSelect2,
} from './quick-task.style';
import PupupInput from '../../components/popup-input/popup-input.component';
 
const select1 = [
    { key: "YeezySupply", value: "YeezySupply" }, { key: "YeezySupply", value: "YeezySupply" },
    { key: "YeezySupply", value: "YeezySupply" }, { key: "YeezySupply", value: "YeezySupply" }
]

const select2 = [
    { key: "Proxies", value: "Proxies" }, { key: "Proxies", value: "Proxies" },
    { key: "Proxies", value: "Proxies" }, { key: "Proxies", value: "Proxies" }
]

function QuickTask(props: any) {

    return (
        <>
            <SelectGrid>
                <IndSelect1>
                    <SelectComponent placeholder={"select option"} options={select1}></SelectComponent>
                </IndSelect1>
                <IndSelect2>
                    <SelectComponent placeholder={"select option two"} options={select2}></SelectComponent>
                </IndSelect2>
            </SelectGrid>
             
        </>
    );
}

export default QuickTask;

