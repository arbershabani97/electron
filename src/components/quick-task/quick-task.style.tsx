import styled from 'styled-components';


export const SelectGrid = styled.div`
  width:100%;
  display:flex;
  align-item: center;
`;

export const InputGrid = styled.div`
  width:100%;
  display:flex;
  align-item: center;
  padding: 20px 10px;
`;

export const IndSelect1 = styled.div`
    flex: 1;
    padding: 0px;
    padding-right: 5px;
`

export const IndSelect2 = styled.div`
    flex: 1;
    padding: 0px;
    padding-left: 5px;
`