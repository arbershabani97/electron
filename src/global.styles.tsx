import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
	body {
		// font-family: 'Oxygen';
		font-family: 'Montserrat', sans-serif;font-style: normal;
		// font-family: Proxima Nova;

		padding: 0px 0px;
		margin: 0px 0px;
		background:#0C0C0C;
	}
	a {
		text-decoration: none;
		color:#0D0D0D;
	}
	* {
		box-sizing: border-box;
	}
	::-webkit-scrollbar {
		width: 1px;
		display: none;

	  }
	textarea:focus, input:focus{
    outline: none;
	}
	  
`;

export const Bottom = styled.div`
  position: relative;
  // height:100vh;
  background-color: #0d0d0d;
`;

export const Top = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
`;
