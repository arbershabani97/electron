import React from 'react';
import HomePage from './pages/home/home.page';
import { GlobalStyle } from './global.styles';

function App() {
  return (
    <div>
      <GlobalStyle />
      <HomePage />
    </div>
  );
}

export default App;
